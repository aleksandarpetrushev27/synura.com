---
title: Terms of use
description: This document describes the terms of use for our website.
---

# Synura Terms of Use

Last Updated: June 21, 2022

Please read this Terms of Use agreement (the “**Terms of Use**”) carefully. This
website and its subdomains (collectively, the “**Website**”) and the information
on the Website, are controlled by Synura Inc. (“**Synura**”, “**us**”, “**we**”,
or “**our**”). These Terms of Use, along with all supplemental terms that may be
presented to you for your review and acceptance (collectively, the
“**Agreement**”), govern your access to and use of the Website.

PLEASE BE AWARE THAT SECTION 8 (DISPUTE RESOLUTION) OF THE AGREEMENT BELOW
CONTAINS PROVISIONS GOVERNING HOW ANY DISPUTES BETWEEN US WILL BE RESOLVED. IN
PARTICULAR, IT CONTAINS AN ARBITRATION AGREEMENT WHICH WILL, WITH LIMITED
EXCEPTIONS, REQUIRE DISPUTES BETWEEN US TO BE SUBMITTED TO BINDING AND FINAL
ARBITRATION. UNLESS YOU OPT OUT OF THE ARBITRATION AGREEMENT: (1) YOU WILL ONLY
BE PERMITTED TO PURSUE CLAIMS AND SEEK RELIEF AGAINST US ON AN INDIVIDUAL BASIS,
NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY CLASS OR REPRESENTATIVE ACTION OR
PROCEEDING; AND (2) YOU ARE WAIVING YOUR RIGHT TO PURSUE CLAIMS AND SEEK RELIEF
IN A COURT OF LAW AND TO HAVE A JURY TRIAL.

Please be aware that Section 1.2 (Synura Communications) of the Agreement below
contains your opt-in consent to receive communications from Synura, including
via email.

Please note that the Agreement is subject to change by Synura in its sole
discretion at any time. When changes are made, Synura will make a copy of the
updated Agreement available at the Website and update the “Last Updated” date at
the top of these Terms of Use. If we make any material changes to the Agreement,
we will provide notice of such material changes on the Website and attempt to
notify you by sending an email to the email address provided during your account
registration. Any changes to the Agreement will be effective immediately for new
users of the Website and will be effective for existing users upon the earlier
of (a) thirty (30) days after the “Last Updated” date at the top of these Terms
of Use, or (b) your consent to and acceptance of the updated Agreement if Synura
provides a mechanism for your immediate acceptance in a specified manner (such
as a click-through acceptance), which Synura may require before further use of
the Website is permitted. If you do not agree to the updated Agreement, you must
stop using the Website upon the effective date of the updated Agreement.
Otherwise, your continued use of the Website after the effective date of the
updated Agreement constitutes your acceptance of the updated Agreement. Please
regularly check the Website to view the then-current Agreement. You agree that
Synura’s continued provision of the Website is adequate consideration for the
changes in the updated Agreement.

1. **USE OF THE WEBSITE**. The Website, and the information and content
   available on it, is protected by applicable intellectual property laws.
   Unless subject to a separate license between you and Synura, your right to
   use the Website is subject to the Agreement.

   1.1. **Certain Restrictions**. The rights granted to you in the Agreement are
   subject to the following restrictions: (a) you shall not license, sell, rent,
   lease, transfer, assign, reproduce, distribute, host or otherwise
   commercially exploit the Website; (b) you shall not frame or utilize framing
   techniques to enclose any trademark, logo, or other parts of the Website
   (including images, text, page layout or form); (c) you shall not use any
   metatags or other “hidden text” using Synura’s name or trademarks; (d) you
   shall not modify, translate, adapt, merge, make derivative works of,
   disassemble, decompile, reverse compile or reverse engineer any part of the
   Website except to the extent the foregoing restrictions are expressly
   prohibited by applicable law; (e) you shall not use any manual or automated
   software, devices or other processes (including but not limited to spiders,
   robots, scrapers, crawlers, avatars, data mining tools or the like) to
   “scrape” or download data from any web pages contained in the Website (except
   that we grant the operators of public search engines revocable permission to
   use spiders to copy materials from the Website for the sole purpose of and
   solely to the extent necessary for creating publicly available searchable
   indices of the materials, but not caches or archives of such materials); (f)
   except as expressly stated herein, no part of the Website may be copied,
   reproduced, distributed, republished, downloaded, displayed, posted or
   transmitted in any form or by any means; and (g) you shall not remove or
   destroy any copyright notices or other proprietary markings contained on or
   in the Website. Any future release, update or other addition to the Website
   shall be subject to the Agreement. Synura and its suppliers and service
   providers reserve all rights not granted in the Agreement.

   1.2. **Synura Communications**. By providing your email address through the
   Website, you agree to receive communications from us, including via email.
   Communications from Synura may include but are not limited to: operational
   communications concerning your use of the Website, updates concerning new and
   existing features on the Website or offered by Synura, communications
   concerning promotions run by us or our third-party partners, and news
   concerning the Synura and industry developments. IF YOU WISH TO OPT OUT OF
   PROMOTIONAL EMAILS, YOU CAN UNSUBSCRIBE FROM OUR PROMOTIONAL EMAIL LIST BY
   FOLLOWING THE UNSUBSCRIBE OPTIONS IN THE PROMOTIONAL EMAIL ITSELF.

   1.3. **Necessary Equipment and Software**. You must provide all equipment and
   software necessary to connect to the Website. You are solely responsible for
   any fees, including Internet connection or mobile fees, that you incur when
   accessing the Website.

2. **OWNERSHIP**

   2.1. **Website**. You agree that Synura and its suppliers own all rights,
   title and interest in the Website (including but not limited to, any computer
   code, themes, objects, characters, character names, stories, dialogue,
   concepts, artwork, animations, sounds, musical compositions, audiovisual
   effects, methods of operation, moral rights, documentation, and Synura
   software). You agree not to remove, alter or obscure any copyright,
   trademark, service mark or other proprietary rights notices incorporated in
   or accompanying the Website.

   2.2. **Trademarks**. Synura’s name and all related stylizations, graphics,
   logos, service marks and trade names used on or in connection with the
   Website are the trademarks of Synura and may not be used without permission
   in connection with your, or any third-party, products or services. Third
   party trademarks, service marks and trade names that may appear on or in the
   Website are the property of their respective owners.

   2.3. **Feedback**. You agree that submission of any ideas, suggestions,
   documents, and/or proposals to Synura through its suggestion, feedback, wiki,
   forum, or similar pages (“**Feedback**”) is at your own risk and that Synura
   has no obligations (including without limitation obligations of
   confidentiality) with respect to such Feedback. You represent and warrant
   that you have all rights necessary to submit the Feedback. You hereby grant
   to Synura a fully paid, royalty-free, perpetual, irrevocable, worldwide, and
   non-exclusive right and license to use, reproduce, perform, display,
   distribute, adapt, modify, re-format, create derivative works of, and
   otherwise commercially or non-commercially exploit in any manner, any and all
   Feedback, and to sublicense the foregoing rights, in connection with the
   operation and maintenance of the Website and/or Synura’s business.

3. **USER CONDUCT**. As a condition of use, you agree not to use the Website for
   any purpose that is prohibited by this Agreement or by applicable law. You
   shall not (and shall not permit any third party) either (a) take any action
   or (b) make available any data, information, or other content on or through
   the Website that: (i) infringes, misappropriates or otherwise violates any
   intellectual property right, right of publicity, right of privacy or other
   right of any person or entity; (ii) is unlawful, threatening, abusive,
   harassing, defamatory, libelous, deceptive, fraudulent, invasive of another’s
   privacy, tortious, obscene, offensive, or profane; (iii) constitutes
   unauthorized or unsolicited advertising, junk or bulk email; (iv) involves
   commercial activities and/or sales, such as contests, sweepstakes, barter,
   advertising, or pyramid schemes without Synura’s prior written consent; (v)
   impersonates any person or entity, including any employee or representative
   of Synura; (vi) interferes with or attempt to interfere with the proper
   functioning of the Website or uses the Website in any way not expressly
   permitted by the Agreement; or (vii) attempts to engage in or engage in, any
   potentially harmful acts that are directed against the Website, including but
   not limited to violating or attempting to violate any security features of
   the Website, introducing viruses, worms, or similar harmful code into the
   Website, or interfering or attempting to interfere with use of the Website by
   any other user, host or network, including by means of overloading,
   “flooding,” “spamming,” “mail bombing,” or “crashing” the Website.

4. **INDEMNIFICATION**. You agree to indemnify and hold Synura, its parents,
   subsidiaries, affiliates, officers, employees, agents, partners, suppliers,
   and licensors (each, a “**Synura Party**” and collectively, the “**Synura
   Parties**") harmless from any losses, costs, liabilities and expenses
   (including reasonable attorneys’ fees) relating to or arising out of any and
   all of the following: (a) your use of the Website in violation of the
   Agreement; (b) your violation of any rights of another party; or (c) your
   violation of any applicable laws, rules or regulations. Synura reserves the
   right, at its own cost, to assume the exclusive defense and control of any
   matter otherwise subject to indemnification by you, in which event you agree
   to fully cooperate with Synura in asserting any available defenses. This
   provision does not require you to indemnify any of the Synura Parties for any
   unconscionable commercial practice by such party or for such party’s fraud,
   deception, false promise, misrepresentation or concealment, or suppression or
   omission of any material fact in connection with the Website provided
   hereunder. You agree that the provisions in this section will survive any
   termination of the Agreement and/or your access to the Website.

5. **DISCLAIMER OF WARRANTIES AND CONDITIONS**

   5.1. **As Is**. YOU EXPRESSLY UNDERSTAND AND AGREE THAT TO THE EXTENT
   PERMITTED BY APPLICABLE LAW, YOUR USE OF THE WEBSITE IS AT YOUR SOLE RISK,
   AND THE WEBSITE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITH ALL
   FAULTS. SYNURA EXPRESSLY DISCLAIMS ALL WARRANTIES, REPRESENTATIONS, AND
   CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS
   FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARISING FROM USE OF THE
   WEBSITE.

   5.1.a. SYNURA MAKES NO WARRANTY, REPRESENTATION OR CONDITION THAT: (1) THE
   WEBSITE WILL MEET YOUR REQUIREMENTS; (2) YOUR USE OF THE WEBSITE WILL BE
   UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE; OR (3) THE RESULTS THAT MAY BE
   OBTAINED FROM USE OF THE WEBSITE WILL BE ACCURATE OR RELIABLE.

   5.1.b. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM
   SYNURA OR THROUGH THE WEBSITE WILL CREATE ANY WARRANTY NOT EXPRESSLY MADE
   HEREIN.

   5.1.c. FROM TIME TO TIME, SYNURA MAY OFFER NEW “BETA” FEATURES OR TOOLS WITH
   WHICH ITS USERS MAY EXPERIMENT. SUCH FEATURES OR TOOLS ARE OFFERED SOLELY FOR
   EXPERIMENTAL PURPOSES AND WITHOUT ANY WARRANTY OF ANY KIND, AND MAY BE
   MODIFIED OR DISCONTINUED AT SYNURA’S SOLE DISCRETION. THE PROVISIONS OF THIS
   SECTION APPLY WITH FULL FORCE TO SUCH FEATURES OR TOOLS.

   5.2. **No Liability for Conduct of Third Parties**. YOU ACKNOWLEDGE AND AGREE
   THAT SYNURA PARTIES ARE NOT LIABLE, AND YOU AGREE NOT TO SEEK TO HOLD SYNURA
   PARTIES LIABLE, FOR THE CONDUCT OF THIRD PARTIES, INCLUDING OPERATORS OF
   EXTERNAL SITES AND OTHER USERS OF THE WEBSITE, AND THAT THE RISK OF INJURY
   FROM SUCH THIRD PARTIES RESTS ENTIRELY WITH YOU. YOU RELEASE SYNURA FROM ALL
   LIABILITY FOR YOU HAVING ACQUIRED OR NOT ACQUIRED CONTENT THROUGH THE
   WEBSITE. WE MAKE NO REPRESENTATIONS CONCERNING ANY CONTENT CONTAINED IN OR
   ACCESSED THROUGH THE WEBSITE, AND WE WILL NOT BE RESPONSIBLE OR LIABLE FOR
   THE ACCURACY, COPYRIGHT COMPLIANCE, OR LEGALITY OF MATERIAL OR CONTENT
   CONTAINED IN OR ACCESSED THROUGH THE WEBSITE.

6. **LIMITATION OF LIABILITY**

   6.1. **Disclaimer of Certain Damages**. YOU UNDERSTAND AND AGREE THAT, TO THE
   FULLEST EXTENT PROVIDED BY LAW, IN NO EVENT SHALL SYNURA PARTIES BE LIABLE
   FOR ANY LOSS OF PROFITS, REVENUE OR DATA, INDIRECT, INCIDENTAL, SPECIAL, OR
   CONSEQUENTIAL DAMAGES, OR DAMAGES OR COSTS DUE TO LOSS OF PRODUCTION OR USE,
   BUSINESS INTERRUPTION, OR PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, IN
   EACH CASE WHETHER OR NOT SYNURA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
   DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THE AGREEMENT OR ANY
   COMMUNICATIONS, INTERACTIONS OR MEETINGS WITH OTHER USERS OF THE WEBSITE, ON
   ANY THEORY OF LIABILITY. THE FOREGOING LIMITATION OF LIABILITY SHALL NOT
   APPLY TO LIABILITY OF A SYNURA PARTY FOR (i) DEATH OR PERSONAL INJURY CAUSED
   BY A SYNURA PARTY’S NEGLIGENCE; OR FOR (ii) ANY INJURY CAUSED BY A SYNURA
   PARTY’S FRAUD OR FRAUDULENT MISREPRESENTATION.

   6.2. **Cap on Liability**. TO THE FULLEST EXTENT PROVIDED BY LAW, SYNURA
   PARTIES WILL NOT BE LIABLE TO YOU FOR MORE THAN THE TOTAL AMOUNT PAID TO
   SYNURA BY YOU DURING THE THREE (3)-MONTH PERIOD PRIOR TO THE ACT, OMISSION OR
   OCCURRENCE GIVING RISE TO SUCH LIABILITY;<sup> </sup>OR THE REMEDY OR PENALTY
   IMPOSED BY THE STATUTE UNDER WHICH SUCH CLAIM ARISES. THE FOREGOING CAP ON
   LIABILITY SHALL NOT APPLY TO LIABILITY OF A SYNURA PARTY FOR (i) DEATH OR
   PERSONAL INJURY CAUSED BY A SYNURA PARTY’S NEGLIGENCE; OR FOR (ii) ANY INJURY
   CAUSED BY A SYNURA PARTY’S FRAUD OR FRAUDULENT MISREPRESENTATION.

   6.3. **Exclusion of Damages**. CERTAIN JURISDICTIONS DO NOT ALLOW THE
   EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME
   OR ALL OF THE ABOVE EXCLUSIONS OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU
   MIGHT HAVE ADDITIONAL RIGHTS.

   6.4. **Basis of the Bargain**. THE LIMITATIONS OF DAMAGES SET FORTH ABOVE ARE
   FUNDAMENTAL ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN SYNURA AND YOU.

7. **TERM AND TERMINATION**. Subject to this Section, the Agreement will remain
   in full force and effect while you use the Website. We may suspend or
   terminate your rights to use the Website at any time for any reason at our
   sole discretion, including for any use of the Website in violation of the
   Agreement. Upon termination of your rights under the Agreement, your right to
   access and use the Website will terminate immediately. Synura will not have
   any liability whatsoever to you for any termination of your rights under the
   Agreement.

8. **DISPUTE RESOLUTION**. **Please read the following arbitration agreement in
   this section (“Arbitration Agreement”) carefully. It requires U.S. users to
   arbitrate disputes with Synura and limits the manner in which you can seek
   relief from us.**

   8.1. **Applicability of Arbitration Agreement**. You agree that any dispute,
   claim, or request for relief relating in any way to your access or use of the
   Website, to any products sold or distributed through the Website, or to any
   aspect of your relationship with Synura, will be resolved by binding
   arbitration, rather than in court, except that (a) you may assert claims or
   seek relief in small claims court if your claims qualify, and (b) you or
   Synura may seek equitable relief in court for infringement or other misuse of
   intellectual property rights. **This Arbitration Agreement shall apply,
   without limitation, to all disputes or claims and requests for relief that
   arose or were asserted before the effective date of this Agreement or any
   prior version of this Agreement.**

   8.2. **Arbitration Rules and Forum**. The Federal Arbitration Act governs the
   interpretation and enforcement of this Arbitration Agreement. To begin an
   arbitration proceeding, you must send a letter requesting arbitration and
   describing your dispute or claim or request for relief to our registered
   agent: Resident Agents Inc., 8 The Green, Ste R, City of Dover, County of
   Kent, 19901. The arbitration will be conducted by JAMS, an established
   alternative dispute resolution provider. Disputes involving claims,
   counterclaims, or request for relief under $250,000, not inclusive of
   attorneys’ fees and interest, shall be subject to JAMS’s most current version
   of the Streamlined Arbitration Rules and procedures available at
   [http://www.jamsadr.com/rules-streamlined-arbitration/](http://www.jamsadr.com/rules-streamlined-arbitration/);
   all other disputes shall be subject to JAMS’s most current version of the
   Comprehensive Arbitration Rules and Procedures, available at
   [http://www.jamsadr.com/rules-comprehensive-arbitration/](http://www.jamsadr.com/rules-comprehensive-arbitration/).
   JAMS’s rules are also available at www.jamsadr.com or by calling JAMS at
   800-352-5267. If JAMS is not available to arbitrate, the parties will select
   an alternative arbitral forum. If the arbitrator finds that you cannot afford
   to pay JAMS’s filing, administrative, hearing and/or other fees and cannot
   obtain a waiver from JAMS, Synura will pay them for you. In addition, Synura
   will reimburse all such JAMS’s filing, administrative, hearing and/or other
   fees for disputes, claims, or requests for relief totaling less than $10,000
   unless the arbitrator determines the claims are frivolous.

   You may choose to have the arbitration conducted by telephone, based on
   written submissions, or in person in the country where you live or at another
   mutually agreed location. Any judgment on the award rendered by the
   arbitrator may be entered in any court of competent jurisdiction.

   8.3. **Authority of Arbitrator**. The arbitrator shall have exclusive
   authority to (a) determine the scope and enforceability of this Arbitration
   Agreement and (b) resolve any dispute related to the interpretation,
   applicability, enforceability or formation of this Arbitration Agreement
   including, but not limited to, any assertion that all or any part of this
   Arbitration Agreement is void or voidable. The arbitration will decide the
   rights and liabilities, if any, of you and Synura. The arbitration proceeding
   will not be consolidated with any other matters or joined with any other
   cases or parties. The arbitrator shall have the authority to grant motions
   dispositive of all or part of any claim. The arbitrator shall have the
   authority to award monetary damages and to grant any non-monetary remedy or
   relief available to an individual under applicable law, the arbitral forum’s
   rules, and the Agreement (including the Arbitration Agreement). The
   arbitrator shall issue a written award and statement of decision describing
   the essential findings and conclusions on which the award is based, including
   the calculation of any damages awarded. The arbitrator has the same authority
   to award relief on an individual basis that a judge in a court of law would
   have. The award of the arbitrator is final and binding upon you and us.

   8.4. **Waiver of Jury Trial**. YOU AND SYNURA HEREBY WAIVE ANY CONSTITUTIONAL
   AND STATUTORY RIGHTS TO SUE IN COURT AND HAVE A TRIAL IN FRONT OF A JUDGE OR
   A JURY. You and Synura are instead electing that all disputes, claims, or
   requests for relief shall be resolved by arbitration under this Arbitration
   Agreement, except as specified in Section 8.1 (Applicability of Arbitration
   Agreement) above. An arbitrator can award on an individual basis the same
   damages and relief as a court and must follow this Agreement as a court
   would. However, there is no judge or jury in arbitration, and court review of
   an arbitration award is subject to very limited review.

   8.5. **Waiver of Class or Other Non-Individualized Relief**. ALL DISPUTES,
   CLAIMS, AND REQUESTS FOR RELIEF WITHIN THE SCOPE OF THIS ARBITRATION
   AGREEMENT MUST BE ARBITRATED ON AN INDIVIDUAL BASIS AND NOT ON A CLASS OR
   COLLECTIVE BASIS, ONLY INDIVIDUAL RELIEF IS AVAILABLE, AND CLAIMS OF MORE
   THAN ONE CUSTOMER OR USER CANNOT BE ARBITRATED OR CONSOLIDATED WITH THOSE OF
   ANY OTHER CUSTOMER OR USER. If a decision is issued stating that applicable
   law precludes enforcement of any of this section’s limitations as to a given
   dispute, claim, or request for relief, then such aspect must be severed from
   the arbitration and brought into the State or Federal Courts located in Santa
   Clara County, California. All other disputes, claims, or requests for relief
   shall be arbitrated.

   8.6. **30-Day Right to Opt Out**. You have the right to opt out of the
   provisions of this Arbitration Agreement by sending written notice of your
   decision to opt out to: [contact@synura.com](mailto:contact@synura.com),
   within thirty (30) days after first becoming subject to this Arbitration
   Agreement. Your notice must include your name and address, your Synura
   username (if any), the email address you used to set up your Synura account
   (if you have one), and an unequivocal statement that you want to opt out of
   this Arbitration Agreement. If you opt out of this Arbitration Agreement, all
   other parts of this Agreement will continue to apply to you. Opting out of
   this Arbitration Agreement has no effect on any other arbitration agreements
   that you may currently have, or may enter in the future, with us.

   8.7. **Severability**. Except as provided in Section 8.5 (Waiver of Class or
   Other Non-Individualized Relief), if any part or parts of this Arbitration
   Agreement are found under the law to be invalid or unenforceable, then such
   specific part or parts shall be of no force and effect and shall be severed
   and the remainder of the Arbitration Agreement shall continue in full force
   and effect.

   8.8. **Survival of Agreement**. This Arbitration Agreement will survive the
   termination of your relationship with Synura.

   8.9. **Modification**. Notwithstanding any provision in this Agreement to the
   contrary, we agree that if Synura makes any future material change to this
   Arbitration Agreement, you may reject that change within thirty (30) days of
   such change becoming effective by writing Synura at the following address:
   [contact@synura.com](mailto:contact@synura.com).

9. **THIRD-PARTY WEBSITES AND APPLICATIONS**. The Website may contain links to
   third-party websites (“**Third-Party Websites**”) and applications
   (“**Third-Party Applications**”). When you click on a link to a Third-Party
   Website or Third-Party Application, we will not warn you that you have left
   the Website and are subject to the terms and conditions (including privacy
   policies) of another website or destination. Such Third-Party Websites and
   Third-Party Applications are not under the control of Synura. Synura is not
   responsible for any Third-Party Websites or Third-Party Applications. Synura
   provides these Third-Party Websites and Third-Party Applications only as a
   convenience and does not review, approve, monitor, endorse, warrant, or make
   any representations with respect to Third-Party Websites or Third-Party
   Applications, or any product or service provided in connection therewith. You
   use all links in Third-Party Websites and Third-Party Applications at your
   own risk. When you leave our Website, the Agreement and our policies no
   longer govern. You should review applicable terms and policies, including
   privacy and data gathering practices, of any Third-Party Websites or
   Third-Party Applications, and make whatever investigation you feel necessary
   or appropriate before proceeding with any transaction with any third party.

10. **GENERAL PROVISIONS**

    10.1. **Governing Law**. Any dispute, claim or request for relief relating
    in any way to your use of the services will be governed and interpreted by
    and under the laws of the state of California, consistent with the Federal
    Arbitration Act, without giving effect to any principles that provide for
    the application of the law of any other jurisdiction. The United Nations
    Convention on Contracts for the International Sale of Goods is expressly
    excluded from this Agreement.

    10.2. **Exclusive Venue**. To the extent the parties are permitted under
    this Agreement to initiate litigation in a court, both you and Synura agree
    that all claims and disputes arising out of or relating to the Agreement
    will be litigated exclusively in the state or federal courts located in
    Santa Clara County, California.

    10.3. **Electronic Communications**. The communications between you and
    Synura may take place via electronic means, whether you visit the Website or
    send Synura emails, or whether Synura posts notices on the Website or
    communicates with you via email. For contractual purposes, you (a) consent
    to receive communications from Synura in an electronic form; and (b) agree
    that all terms and conditions, agreements, notices, disclosures, and other
    communications that Synura provides to you electronically satisfy any legal
    requirement that such communications would satisfy if it were to be in
    writing. The foregoing does not affect your statutory rights, including but
    not limited to the Electronic Signatures in Global and National Commerce Act
    at 15 U.S.C. §7001 et seq.

    10.4. **Assignment**. The Agreement, and your rights and obligations
    hereunder, may not be assigned, subcontracted, delegated or otherwise
    transferred by you without Synura’s prior written consent, and any attempted
    assignment, subcontract, delegation, or transfer in violation of the
    foregoing will be null and void.

    10.5. **Force Majeure**. Synura shall not be liable for any delay or failure
    to perform resulting from causes outside its reasonable control, including,
    but not limited to, acts of God, pandemics, war, terrorism, riots, embargos,
    acts of civil or military authorities, fire, floods, accidents, strikes or
    shortages of transportation facilities, fuel, energy, labor or materials.

    10.6. **Questions, Complaints, Claims**. If you have any questions,
    complaints or claims with respect to the Website, please contact us at:
    [contact@synura.com](mailto:contact@synura.com). We will do our best to
    address your concerns. If you feel that your concerns have been addressed
    incompletely, we invite you to let us know for further investigation.

    10.7. **Choice of Language**. It is the express wish of the parties that the
    Agreement and all related documents have been drawn up in English.

    10.8. **Notice**. Where Synura requires that you provide an email address,
    you are responsible for providing Synura with your most current email
    address. In the event that the last email address you provided to Synura is
    not valid, or for any reason is not capable of delivering to you any notices
    required/ permitted by the Agreement, Synura’s dispatch of the email
    containing such notice will nonetheless constitute effective notice. You may
    give notice to Synura at the following address: Resident Agents Inc., 8 The
    Green, Ste R, City of Dover, County of Kent, 19901. Such notice shall be
    deemed given when received by Synura by letter delivered by nationally
    recognized overnight delivery service or first class postage prepaid mail at
    the above address.

    10.9. **Waiver**. Any waiver or failure to enforce any provision of the
    Agreement on one occasion will not be deemed a waiver of any other provision
    or of such provision on any other occasion.

    10.10. **Severability**. If any portion of the Agreement is held invalid or
    unenforceable, that portion shall be construed in a manner to reflect, as
    nearly as possible, the original intention of the parties, and the remaining
    portions shall remain in full force and effect.

    10.11. **Export Control**. The Website may be subject to U.S. export control
    laws and may be subject to export or import regulations in other countries.
    You agree not to export, reexport, or transfer, directly or indirectly, any
    U.S. technical data acquired from Synura, or any products utilizing such
    data, in violation of the United States export laws or regulations.

    10.12. **Consumer Complaints**. In accordance with California Civil Code
    §1789.3, you may report complaints to the Complaint Assistance Unit of the
    Division of Consumer Services of the California Department of Consumer
    Affairs by contacting them in writing at 1625 North Market Blvd., Suite N
    112, Sacramento, CA 95834, or by telephone at (800) 952-5210.

    10.13. **Entire Agreement**. The Agreement is the final, complete and
    exclusive agreement of the parties with respect to the subject matter hereof
    and supersedes and merges all prior discussions between the parties with
    respect to such subject matter.
