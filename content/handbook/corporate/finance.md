---
title: Finance
description:
  Our financial management procedures and policies, including how we track
  assets, spend and track money, deal with invoices, and so on.
---

# {{% param "title" %}}

{{% param "description" %}}

If you are a freelancer you may be working as a vendor or contractor. If unsure,
you can check with your contact at Synura to confirm.

## Getting paid as a vendor

1. All invoices are required to submit to accounting@synura.com.
2. Please DO NOT submit invoices and/or payment instructions via GitLab/Notion,
   etc.
3. Do not add them directly to bill.com or other systems.
4. Vendors are required to submit their invoices within 7 days after the
   services are rendered or the goods are delivered. Timely submission of your
   invoices will enable Accounting to record the expenses in the right financial
   period.

### Important tax form requirements

1. For **US-based vendors**, vendors are required to provide a completed
   [W-9 Form](https://www.irs.gov/pub/irs-pdf/fw9.pdf) to accounting@synura.com
   before or at the time when they submit their 1st invoice.
2. **For Non-US vendors**, vendors are required to provide a completed
   [W-8BEN-E Form](https://www.irs.gov/pub/irs-pdf/fw8bene.pdf) before or at the
   time they submit their 1st invoice to accounting@synura.com. Here are the
   [instructions](https://www.irs.gov/instructions/iw8bene) for Form W-8BEN-E
   Form.
3. Important: Accounting cannot process a payment without these Tax Forms

## Getting paid as a contractor

If you are a monthly wage contractor your payment will be automatically
scheduled at the end of the month, similar to a full-time direct employee, and
there's nothing special you need to do. If you are an hourly/daily wage
contractor you will need to submit your days/hours worked into
[our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
(internal only) each month. You can check to see your wage type (hourly, daily,
or monthly) in the compensation section of your
[non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
(internal only) profile if you are unsure.

The basic timeline for invoicing and payments for monthly/daily contractors is
as follows:

1. The contractor submits days or hours worked via Invoice in
   [our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only) on the last day of the month. If you would prefer to be paid
   bi-monthly you can submit an additional invoice on the 15th of each month as
   well.
1. CEO approves on the next business day.
1. Accounting submits payment on the same day as approval.
1. Payment arrives to the contractor about 5 business days later.

For new hires who joined on or before the 22nd of a given month, Accounting will
try to include the new hire in the monthly payment schedule. If a new hire
joined after the 22nd of a given month, Accounting may need to run an off-cycle
payment for the new hire. Accounting will notify the new hire when the payment
will arrive in the situation of an off-cycle payment.

If you have any questions or concerns about the status of your payment, please
contact accounting@synura.com.

## Getting paid as an employee

1. Payroll for US-based full-time employees is managed via
   [our US payroll system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.o8wzj9p4hse2)
   (internal only).
2. Synura runs a semi-monthly payroll schedule.
3. US Employees will complete the onboarding process through
   [our US payroll system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.o8wzj9p4hse2)
   (internal only) and set up direct deposits for their payroll payments.

## Internal procedures

### Fixed assets tracking

1. Fixed assets such as office furniture, computer equipment, and laptops are
   purchased for long-term use. They usually have a useful life of more than one
   year. Synura tracks its fixed assets with a unit price of over USD 500 via
   [this document](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0)
   (internal only).
2. The purchaser of the assets is required to open this file to fill out the
   information (as indicated in the file), such as the date of purchase, the
   purchaser's name, the value of the asset, etc.
3. Any team members who received the company-purchased laptops or computer
   equipment are required to open this file and fill out the asset description,
   model #, and serial #.
4. Please make sure the information of the Fixed Asset Tracking file is
   completed _within 7 business days_ of the purchase date when the asset is
   initially purchased.
5. If a transition happens from existing computer equipment to another team
   member, the recipient of the computer equipment is required to acknowledge
   the receipt of the computer equipment _within 7 business days_. Please open
   the Fixed Asset Tracking file and fill out the required information (as
   indicated in the file), such as the description of the asset transition, date
   of transition, old owner, new owner, etc.

#### Returning assets

See
[the offboarding guide on company assets](../../people/offboarding/#company-assets-and-laptop)
for how to return fixed assets at the end of employment.

#### Purchasing company assets that are no longer used for business

If an employee is interested in buying a company asset that is no longer used
for business, the CEO will review case by case, with consideration to whether an
asset can be used again, value to the company, and define a fair market value
for sale. CEO should notify Accounting immediately if a decision is made to
allow an employee to purchase the company's assets, so Accounting can make sure
this is recorded properly.

### New contractor setup

Please refer to the Expense Reports and Non-US payroll sections below for more
details.

Be sure also to complete the other non-financial
[onboarding steps](../../people/onboarding/#steps-after-an-offer-is-accepted).

Freelancers are functionally equivalent to contractors, so should follow the
same process for getting set up to be paid. This includes submitting a
[new contractor form](https://forms.gle/S9EXafcXmnhnARMX9).

#### Non-US payroll

Synura has
[a non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
(internal only) to process payments for **non-US team members**.

When a team member completes the onboarding process, the direct manager of the
new hire should notify Accounting immediately via sending an email to
accounting@synura.com with the information below:

- Full name of the new hire
- Official start date
- Annual or monthly compensation in USD$

With the new hire's information, Accounting will help to pre-generate the
monthly invoices for the next 12 months on behalf of the new hire, if they are
paid via a monthly rate. If there are any exceptions on this, please notify
Accounting by sending an email to accounting@synura.com.

1. For contracted **non-US team members**, a completed W-8BEN Form is collected
   during the
   [non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only) onboarding process.
2. Payments related to the contracted services are usually processed in
   [our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only) monthly.
3. Expense reimbursement reports from the **non-US team members** are required
   to submit in
   [our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only).

### Invoice processing

Invoice receiving, approval, and payment are only relevant for US contractors
and vendors. All non-US-based contractors should use
[our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
(internal only).

#### Invoice receiving

Invoice receiving is handled by the accounting team.

1. Process invoices received in accounting@synura.com in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) and route the invoices for approval in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) according to the Invoice Approval Hierarchy.
2. The AP accountant will reply to accounting@synura.com and the vendor to
   acknowledge that the invoice has been processed in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only). The turnaround time is usually 1-2 business days from the
   day the invoice is submitted to Accounting and the acknowledgment.
3. AP Accountant needs to make sure to use the contractor's contact email to
   send out the Bill.com e-invite (do not use the contractor's Synura email).
   This will help us to make sure the applicable contractors can receive their
   tax forms even if they are no longer working with Synura
4. For US vendors, upload a copy of the W-9 in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) under the Document section
5. For non-US vendors, upload a copy of the
   [W-8BEN Form](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf) in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) under the Document section

#### Invoice approvals

1. All invoices that require Accounting to make a payment are required to be
   processed in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only).
2. Approvals of invoices are completed in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only).
3. Accounting will route the invoices according to the following approval
   hierarchy
4. In general, invoices up to $5000 can either be approved by CTO or CEO.
5. Invoices over $5000 require CEO approval
6. **Project Related Invoice Approval**. For Project-related invoices, ONLY the
   Head of Engineering team approves the bill in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only). The CTO and the CEO can approve the payment in
   [our banking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.sbqarsgy7ued)
   (internal only) for wires.
7. The Head of Engineering and Accounting can review and validate the spending
   according to the Project budget periodically. However, all Project-related
   bills will need the Head of Engineering to approve in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) to enable proper internal control.

#### Invoice payments

##### Billing system payments

1. Once an invoice is approved, payment is usually processed based on Net 30
   payment terms unless otherwise specified in an agreement or invoice
2. [Our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) e-payment is recommended to pay all vendors
3. To establish e-payment in
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only), Accounting will send an e-invite to an email that is
   provided by the vendor. The vendor accepts the invite and provides their bank
   information within
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only). The payment will be scheduled to deposit into the vendor's
   account according to the payment terms. E-payment will usually arrive in the
   recipient's account within 2-4 business days.

**Exception:** For some non-US vendors who may have difficulties setting up an
e-payment in
[our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
(internal only), a wire payment will be processed.

##### Wire payments

All wires from
[our banking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.sbqarsgy7ued)
(internal only) are initiated by Accounting and approved by Synura authorized
signer(s) per the authorization that is set up with the bank.

Wire requestor's responsibilities:

1. Make sure the vendor provides all the required information as outlined below
   before requesting a wire payment.
2. For a new vendor’s 1st payment, please send an email to accounting@synura.com
   with invoices, wire instructions, Form W-8BEN-E (if non-US vendors), or Form
   W-9 (US vendors). For the 2nd payment and onward, sending an invoice only is
   okay unless the vendor requests any changes, such as bank changes.
3. Make sure the vendor provides a formal invoice. (A quotation and/or a Pro
   Forma Invoice cannot be accepted to issue a payment). Exceptions should be
   discussed with the CTO or CEO and Accounting.
4. For non-US vendors, make sure the invoices indicate the proper currency Wire
   payment is used to pay any vendors who cannot get paid via
   [our billing system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.aip1u1xb9fx1)
   (internal only) or provide funds to **non-US team members** for business
   expenses. To request a wire payment, please send an email request to
   accounting@synura.com along with:
5. The invoice to be paid
6. Vendor's wire instructions - see below-requested information about wire
   instructions
7. If a foreign currency wire payment is required, please indicate which
   currency
8. If a wire payment needs to arrive by a certain date, please be very specific
   about it so Accounting can plan accordingly
9. A brief description of the Purpose of Payment (per bank requirement for
   international wires)
10. **Bank Fees** If any bank fees that Synura should be covering to ensure the
    recipient can receive the full payment, please notify Accounting about how
    much bank fees to add to the wire total as the fees can vary vendor by
    vendor. For example, some Zambia vendors don't need Synura to include any
    bank transfer charges, some of them do. Given each vendor's situation is
    different, Accounting requests vendors to provide how much bank fee to
    include and in what currency with the 1st payment request. For future
    recurring payments, Accounting can include the same amount of bank fees
    without asking again unless new changes are provided by the vendor and the
    amount of change is justified by Accounting.

Typically, a wire instruction needs to include:

1. Recipient's name
2. Recipient's address
3. Recipient's bank name
4. Recipient's bank address
5. Recipient's bank account
6. Recipient's bank ABA Routing Numbers (if domestic wire within the United
   States)
7. Recipient's IBAN, BIC, or SWIFT Code (if international bank outside of United
   States)

Sometimes the bank may require to provide additional information, Accounting
will reach out to the wire requestor if such a request arises.

1. Wire payment is usually quicker, but for international wire payments, it
   still takes a few business days to clear. We suggest you submit the wire
   request at least 7 business days before the payment due date to allow
   accounting to get the wire initiated and approved.
2. If the wire payment is urgent, please flag "**URGENT Payment Request**" in
   the subject line when you send the wire request to accounting@synura.com.
   Please don't submit a payment request via a Gitlab ticket as it can easily be
   buried in the thread.
3. If the wire payment is recurring, please send the invoice that needs to be
   paid to Accounting and indicates this is a recurring payment with no wire
   instruction change. Please be specific if the wire needs to arrive by a
   certain date so Accounting can plan the wire accordingly.

Once a wire payment is initiated and approved, the controller will send a proof
of payment to the wire requestor on the following business day or when such a
document becomes available in
[our banking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.sbqarsgy7ued)
(internal only). When the controller is not available, such as on vacation,
accounting@synura.com will provide such proof of payment.
