---
title: Terms of service
description: This document describes the terms of use for our application.
---

# Synura Terms of Service

Last Updated: July 15, 2022

Please read this Terms of Service agreement (the “**Terms of Service**”)
carefully. The services and resources available or enabled via
[https://www.synura.com/](https://www.synura.com/) (the “**Website**”) and the
software made available by Synura, including but not limited to any Synura APIs
and/or SDKs, as well as any associated documentation (the “**Software**”) (each
a “**Service**” and collectively, the “**Services**”), are controlled by Synura
Inc. (“**Synura**”, “**us**”, “**we**”, or “**our**”). These Terms of Service,
along with all supplemental terms that may be presented to you for your review
and acceptance (collectively, the “**Agreement**”), govern your access to and
use of the Services. By clicking on the “I Accept” button, completing the
registration process, downloading the Software, or otherwise accessing or using
any of the Services, you represent that (1) you have read, understand, and agree
to be bound by the Agreement, (2) you are of legal age to form a binding
contract with Synura, and (3) you have the authority to enter into the Agreement
personally or on behalf of the legal entity identified during the account
registration process, and to bind that legal entity to the Agreement. The term
“**you**” refers to the individual or such legal entity, as applicable. **If
you, or if applicable, such legal entity, do not agree to be bound by the
Agreement, you, and if applicable, such legal entity, may not access or use any
of the Services**.

IF YOU SUBSCRIBE TO THE SERVICES FOR A TERM, THE AGREEMENT WILL BE AUTOMATICALLY
RENEWED FOR ADDITIONAL PERIODS OF THE SAME DURATION AS THE INITIAL TERM AT
SYNURA’S THEN-CURRENT FEE FOR SUCH SERVICES UNLESS YOU DECLINE TO RENEW YOUR
SUBSCRIPTION IN ACCORDANCE WITH SECTION 7.6 (AUTOMATIC RENEWAL) BELOW.

PLEASE BE AWARE THAT SECTION 15 (DISPUTE RESOLUTION) OF THE AGREEMENT BELOW
CONTAINS PROVISIONS GOVERNING HOW ANY DISPUTES BETWEEN US WILL BE RESOLVED. IN
PARTICULAR, IT CONTAINS AN ARBITRATION AGREEMENT WHICH WILL, WITH LIMITED
EXCEPTIONS, REQUIRE DISPUTES BETWEEN US TO BE SUBMITTED TO BINDING AND FINAL
ARBITRATION. UNLESS YOU OPT OUT OF THE ARBITRATION AGREEMENT: (1) YOU WILL ONLY
BE PERMITTED TO PURSUE CLAIMS AND SEEK RELIEF AGAINST US ON AN INDIVIDUAL BASIS,
NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY CLASS OR REPRESENTATIVE ACTION OR
PROCEEDING; AND (2) YOU ARE WAIVING YOUR RIGHT TO PURSUE CLAIMS AND SEEK RELIEF
IN A COURT OF LAW AND TO HAVE A JURY TRIAL.

Please be aware that Section 1.4 (Synura Communications) of the Agreement below
contains your opt-in consent to receive communications from Synura, including,
as applicable, via email and push notification.

Please note that the Agreement is subject to change by Synura in its sole
discretion at any time. When changes are made, Synura will make a copy of the
updated Agreement available at the Website and update the “Last Updated” date at
the top of these Terms of Service. If we make any material changes to the
Agreement, we will provide notice of such material changes on the Website and
attempt to notify you by sending an email to the email address provided during
your account registration. Any changes to the Agreement will be effective
immediately for new users of the Services and will be effective for existing
Registered Users (defined below) upon the earlier of (a) thirty (30) days after
the “Last Updated” date at the top of these Terms of Service, or (b) your
consent to and acceptance of the updated Agreement if Synura provides a
mechanism for your immediate acceptance in a specified manner (such as a
click-through acceptance), which Synura may require before further use of the
Services is permitted. If you do not agree to the updated Agreement, you must
stop using all Services upon the effective date of the updated Agreement.
Otherwise, your continued use of any of the Services after the effective date of
the updated Agreement constitutes your acceptance of the updated Agreement.
Please regularly check the Website to view the then-current Agreement. You agree
that Synura’s continued provision of the Services is adequate consideration for
the changes in the updated Agreement.

1. **USE OF THE SERVICES**. The Services, and the information and content
   available on them, are protected by applicable intellectual property laws.
   Unless subject to a separate license between you and Synura, your right to
   use any and all Services is subject to the Agreement.

   1.1. **Synura Software**. Use of the Software is governed by the Agreement.
   Subject to your compliance with the Agreement, Synura grants you a
   non-assignable, non-transferable, non-sublicensable, revocable, non-exclusive
   license to use the Software in object code form only for the sole purpose of
   enabling you to use the Services in the manner permitted by the Agreement.
   Some Software may be offered under open source licenses that we will make
   available to you upon your request. There may be provisions in the open
   source licenses that expressly override some of these terms.

   1.2. **Limited Source Available License**. Subject to your compliance with
   this Agreement, Synura grants you a limited right to access and review the
   source code form of the Software (“**Source Code**”) solely for your internal
   review, testing, and evaluation purposes. You may not: (a) distribute,
   sublicense, sell, publish, or otherwise make the Source Code available to any
   third party for any purpose; or (b) use the Source Code for or in any
   production or commercial purpose, including but not limited to developing,
   marketing, or selling any products similar or related to the Software.
   Notwithstanding the foregoing, you may modify and publish patches to the
   Source Code for the sole purpose of making contributions back to Synura (such
   modifications referred to herein as “**Contributions**”). If you choose to
   make any Contributions to the Source Code, you agree that such Contributions
   are the sole property of Synura and you hereby assign to Synura your entire
   right, title, and interest in any such Contributions and any intellectual
   property rights embodied therein. You further agree to execute any
   assignments, oaths, declarations and other documents as may be requested by
   Synura to effect the foregoing. For the avoidance of doubt, this Section 1.2
   (Limited Source Available License) applies regardless of whether you have
   signed up for a paid subscription term to the Services.

   1.3. **Updates**. You understand that the Services are evolving. As a result,
   Synura may require you to accept updates to the Services that you have
   installed on your computer. You acknowledge and agree that Synura may update
   the Services with or without notifying you. You may need to update
   third-party software from time to time in order to use the Services.

   1.4. **Certain Restrictions**. The rights granted to you in the Agreement are
   subject to the following restrictions: (a) you shall not license, sell, rent,
   lease, transfer, assign, reproduce, distribute, host or otherwise
   commercially exploit any of the Services; (b) you shall not frame or utilize
   framing techniques to enclose any trademark, logo, or other parts of the
   Services (including images, text, page layout or form); (c) you shall not use
   any metatags or other “hidden text” using Synura’s name or trademarks; (d)
   you shall not modify, translate, adapt, merge, make derivative works of,
   disassemble, decompile, reverse compile or reverse engineer any part of the
   Services except as otherwise expressly permitted by Section 1.2 and to the
   extent the foregoing restrictions are expressly prohibited by applicable law;
   (e) you shall not use any manual or automated software, devices or other
   processes (including but not limited to spiders, robots, scrapers, crawlers,
   avatars, data mining tools or the like) to “scrape” or download data from any
   web pages contained in the Services (except that we grant the operators of
   public search engines revocable permission to use spiders to copy materials
   from the Website for the sole purpose of and solely to the extent necessary
   for creating publicly available searchable indices of the materials, but not
   caches or archives of such materials); (f) except as expressly stated herein,
   no part of the Services may be copied, reproduced, distributed, republished,
   downloaded, displayed, posted or transmitted in any form or by any means; and
   (g) you shall not remove or destroy any copyright notices or other
   proprietary markings contained on or in the Services. Any future release,
   update or other addition to the Services shall be subject to the Agreement.
   Synura and its suppliers and service providers reserve all rights not granted
   in the Agreement.

   1.5. **Synura Communications**. By entering into the Agreement or using the
   Services, you agree to receive communications from us, including via email or
   push notification through the Services. Communications from Synura may
   include but are not limited to: operational communications concerning your
   Account or the use of the Services, updates concerning new and existing
   features on the Services, communications concerning promotions run by us or
   our third-party partners, and news concerning the Synura and industry
   developments. IF YOU WISH TO OPT OUT OF PROMOTIONAL EMAILS, YOU CAN
   UNSUBSCRIBE FROM OUR PROMOTIONAL EMAIL LIST BY FOLLOWING THE UNSUBSCRIBE
   OPTIONS IN THE PROMOTIONAL EMAIL ITSELF.

2. **REGISTRATION**

   2.1. **Registering Your Account**. In order to access certain features of the
   Services you may be required to become a Registered User. For purposes of the
   Agreement, a “**Registered User**” is a user who has registered an account
   with Synura through the Services (“**Account**”) or who has a valid account
   on a third-party service such as Google or Slack (“**Third-Party Service**”)
   through which the user has connected to the Services (each such account, a
   “**Third-Party Account**”).

   2.2. **Access Through a Third-Party Service**. If you access the Services
   through a Third-Party Service as part of the functionality of the Services,
   you may link your Account with Third-Party Accounts by allowing Synura to
   access your Third-Party Account, as is permitted under the applicable terms
   and conditions that govern your Third-Party Account. You represent that you
   are entitled to grant Synura access to your Third-Party Account (including,
   but not limited to, for use for the purposes described herein) without breach
   by you of any of the terms and conditions that govern your Third-Party
   Account and without obligating Synura to pay any fees or making Synura
   subject to any usage limitations imposed by such Third-Party Service. By
   granting Synura access to any Third-Party Accounts, you understand that
   Synura may access, make available and store (if applicable) any information,
   data, text, software, music, sound, photographs, graphics, video, messages,
   tags and other materials accessible through the Services that you have
   provided to and stored in your Third-Party Account (“**Third-Party Service
   Content**”) so that it is available on and through the Services via your
   Account. Unless otherwise specified in the Agreement, all Third-Party Service
   Content shall be considered to be Your Content (as defined in Section 3.1
   (Types of Content)) for all purposes of the Agreement. Depending on the
   Third-Party Accounts you choose and subject to the privacy settings that you
   have set in such Third-Party Accounts, personally identifiable information
   that you post to your Third-Party Accounts may be available on and through
   your Account on the Services. Please note that if a Third-Party Account or
   associated service becomes unavailable, or Synura’s access to such
   Third-Party Account is terminated by the Third-Party Service, then
   Third-Party Service Content will no longer be available on and through the
   Services. If you select settings within the Services to link with a
   Third-Party Account, you grant Synura permission to transfer, store, share,
   make available, and publish your Content to your Third-Party Accounts(s) as
   directed by you within the Services. Please note that if a Third-Party
   Account or associated service becomes unavailable, or if Synura’s access to
   such Third-Party Account is terminated by the applicable third-party service
   provider, then Synura may not publish your Content to such Third-Party
   Account. You have the ability to disable the connection between your Account
   and your Third-Party Accounts at any time by accessing the “Settings” section
   of the Services. PLEASE NOTE THAT YOUR RELATIONSHIP WITH THE THIRD-PARTY
   SERVICE PROVIDERS ASSOCIATED WITH YOUR THIRD-PARTY SERVICE ACCOUNTS IS
   GOVERNED SOLELY BY YOUR AGREEMENT(S) WITH SUCH THIRD-PARTY SERVICE PROVIDERS,
   AND SYNURA DISCLAIMS ANY LIABILITY FOR PERSONALLY IDENTIFIABLE INFORMATION
   THAT MAY BE PROVIDED TO IT BY SUCH THIRD-PARTY SERVICE PROVIDERS IN VIOLATION
   OF THE PRIVACY SETTINGS THAT YOU HAVE SET IN SUCH THIRD-PARTY SERVICE
   ACCOUNTS. Synura makes no effort to review any Third-Party Service Content
   for any purpose, including but not limited to, for accuracy, legality or
   noninfringement, and Synura is not responsible for any Third-Party Service
   Content.

   2.3. **Registration Data**. In registering an Account, you agree to (a)
   provide true, accurate, current and complete information about yourself as
   prompted by the registration form (the “**Registration Data**”); and (b)
   maintain and promptly update the Registration Data to keep it true, accurate,
   current and complete. You represent that you are (i) at least eighteen (18)
   years old; (ii) of legal age to form a binding contract; and (iii) not a
   person barred from using the Services under the laws of the United States,
   your place of residence or any other applicable jurisdiction. You are
   responsible for all activities that occur under your Account. You agree that
   you shall monitor your Account to restrict use by any other persons,
   including minors, and you will accept full responsibility for any such
   unauthorized use. You may not share your Account login or password with
   anyone, and you agree to (y) notify Synura immediately of any unauthorized
   use of your password or any other breach of security; and (z) exit from your
   Account at the end of each session. If you provide any information that is
   untrue, inaccurate, not current or incomplete, or Synura has reasonable
   grounds to suspect that any information you provide is untrue, inaccurate,
   not current or incomplete, Synura has the right to suspend or terminate your
   Account and refuse any and all current or future use of the Services (or any
   portion thereof). You agree not to create an Account using a false identity
   or information, or on behalf of someone other than yourself. You agree that
   you shall not have more than one Account at any given time. Synura reserves
   the right to remove or reclaim any usernames at any time and for any reason,
   including but not limited to, claims by a third party that a username
   violates the third party’s rights. You agree not to create an Account or use
   the Services if you have been previously removed by Synura, or if you have
   been previously banned from any of the Services.

   2.4. **Your Account**. Notwithstanding anything to the contrary herein, you
   acknowledge and agree that you shall have no ownership or other property
   interest in your Account, and you further acknowledge and agree that all
   rights in and to your Account are and shall forever be owned by and inure to
   the benefit of Synura.

   2.5. **Necessary Equipment and Software**. You must provide all equipment and
   software necessary to connect to the Services. You are solely responsible for
   any fees, including Internet connection or mobile fees, that you incur when
   accessing the Services.

3. **RESPONSIBILITY FOR CONTENT**

   3.1. **Types of Content**. For purposes of this Agreement, the term
   “**Content**” includes, without limitation, information, videos, audio files,
   data, text, photographs, written posts and comments, software, scripts,
   graphics, and interactive features generated, provided, or otherwise made
   accessible on or through the Services. You acknowledge that all Content is
   the sole responsibility of the party from whom such Content originated. This
   means that you, and not Synura, are entirely responsible for all Content that
   you upload, post, email, transmit or otherwise make available through the
   Services (“**Your Content**”), and that you and other Registered Users of the
   Services, and not Synura, are similarly responsible for all Content that you
   and they make available through the Services (“**User Content**”).

   3.2. **No Obligation to Pre-Screen Content**. You acknowledge that Synura has
   no obligation to pre-screen User Content, although Synura reserves the right
   in its sole discretion to pre-screen, refuse or remove any User Content. By
   entering into the Agreement, you hereby provide your irrevocable consent to
   Synura’s monitoring of Your Content. You acknowledge and agree that you have
   no expectation of privacy concerning the transmission of Your Content,
   including without limitation chat, text, or voice communications. In the
   event that Synura pre-screens, refuses or removes any of Your Content, you
   acknowledge that Synura will do so for Synura’s benefit, not yours. Without
   limiting the foregoing, Synura shall have the right to remove any of Your
   Content that violates the Agreement or is otherwise objectionable.

   3.3. **Storage**. Synura has no responsibility or liability for the accuracy
   of any User Content, including Your Content; or the security, privacy, or
   transmission of other communications originating with or involving use of the
   Services. Certain Services may enable you to specify the level at which such
   Services restrict access to Your Content. You are solely responsible for
   choosing the appropriate level of access to Your Content. If you do not so
   choose, the Services may default to the most permissive setting. You agree
   that Synura retains the right to create reasonable limits on Synura’s use and
   storage of User Content, including Your Content, such as limits on file size,
   storage space, processing capacity, and similar limits as determined by
   Synura in its sole discretion.

4. **OWNERSHIP**

   4.1. **Services**. Except with respect to Your Content and other User
   Content, you agree that Synura and its suppliers own all rights, title and
   interest in the Services (including but not limited to, any computer code,
   themes, objects, characters, character names, stories, dialogue, concepts,
   artwork, animations, sounds, musical compositions, audiovisual effects,
   methods of operation, moral rights, documentation, and Synura software). You
   agree not to remove, alter or obscure any copyright, trademark, service mark
   or other proprietary rights notices incorporated in or accompanying any
   Services.

   4.2. **Trademarks**. Synura’s name and all related stylizations, graphics,
   logos, service marks and trade names used on or in connection with any
   Services are the trademarks of Synura and may not be used without permission
   in connection with your, or any third-party, products or services. Third
   party trademarks, service marks and trade names that may appear on or in the
   Services are the property of their respective owners.

   4.3. **Your Content**. Synura does not claim ownership of Your Content.
   However, when you post or publish Your Content on or in any Services, you
   represent and warrant that you have all rights to grant such licenses to us
   without infringement or violation of any third-party rights, including
   without limitation, any privacy rights, publicity rights, copyrights,
   trademarks, contract rights, or any other intellectual property or
   proprietary rights. Without limiting the foregoing, you may not post a video,
   photograph, or any personally identifying information of another person
   without that person’s permission.

   4.4. **License to Your Content**. Subject to any applicable Account settings
   that you select, you grant Synura a fully paid, royalty-free, worldwide,
   non-exclusive right (including any moral rights) and license to access, use,
   import, edit, modify, truncate, aggregate, adapt, reproduce, distribute,
   display, publish, disclose, transmit, prepare derivative works of, store,
   cache, and perform Your Content (in whole or in part) for the purposes of
   operating and providing the Services to you and to other Registered Users as
   identified by you as part of your organization or as otherwise selected by
   you through the Services (each, a “**Team Member**”). Please remember that
   your Team Members may search for, see, use, modify and reproduce any of Your
   Content that you elect to share with such Team Members, each as elected by
   you through the Services. When you elect to share Your Content to other
   Registered Users, including your Team Members, you understand that each such
   Registered User has the ability to view, access, and display Your Content,
   including after termination of your Account or use of the Services. In
   addition, when you share Your Content to other Registered Users, you may
   choose to grant the applicable Registered User any or all of the following
   additional rights: download Your Content, publish a link to Your Content,
   and/or invite third parties to view, access, and display Your Content. You
   agree that you, not Synura, are responsible for all of Your Content.

   4.5. **Username**. Notwithstanding anything contained herein to the contrary,
   by submitting Your Content to any forums, comments, or any other area on the
   Services, you hereby expressly permit Synura to identify you by your username
   (which may be a pseudonym) as the contributor of Your Content.

   4.6. **Feedback**. You agree that submission of any ideas, suggestions,
   documents, and/or proposals to Synura through its suggestion, feedback, wiki,
   forum, or similar pages (“**Feedback**”) is at your own risk and that Synura
   has no obligations (including without limitation obligations of
   confidentiality) with respect to such Feedback. You represent and warrant
   that you have all rights necessary to submit the Feedback. You hereby grant
   to Synura a fully paid, royalty-free, perpetual, irrevocable, worldwide, and
   non-exclusive right and license to use, reproduce, perform, display,
   distribute, adapt, modify, re-format, create derivative works of, and
   otherwise commercially or non-commercially exploit in any manner, any and all
   Feedback, and to sublicense the foregoing rights, in connection with the
   operation and maintenance of the Services and/or Synura’s business.

5. **USER CONDUCT**. As a condition of use, you agree not to use any of the
   Services for any purpose that is prohibited by this Agreement or by
   applicable law. You shall not (and shall not permit any third party) either
   (a) take any action or (b) make available any Content on or through the
   Services that: (i) infringes, misappropriates or otherwise violates any
   intellectual property right, right of publicity, right of privacy or other
   right of any person or entity; (ii) is unlawful, threatening, abusive,
   harassing, defamatory, libelous, deceptive, fraudulent, invasive of another’s
   privacy, tortious, obscene, offensive, or profane; (iii) constitutes
   unauthorized or unsolicited advertising, junk or bulk email; (iv) involves
   commercial activities and/or sales, such as contests, sweepstakes, barter,
   advertising, or pyramid schemes without Synura’s prior written consent; (v)
   impersonates any person or entity, including any employee or representative
   of Synura; (vi) interferes with or attempt to interfere with the proper
   functioning of the Services or uses the Services in any way not expressly
   permitted by the Agreement; or (vii) attempts to engage in or engage in, any
   potentially harmful acts that are directed against the Services, including
   but not limited to violating or attempting to violate any security features
   of the Services, introducing viruses, worms, or similar harmful code into the
   Services, or interfering or attempting to interfere with use of the Services
   by any other user, host or network, including by means of overloading,
   “flooding,” “spamming,” “mail bombing,” or “crashing” the Services.

6. **INTERACTIONS WITH OTHER USERS**

   6.1. **User Responsibility**. You are solely responsible for your
   interactions with other Registered Users and any other parties with whom you
   interact; provided, however, that Synura reserves the right, but has no
   obligation, to intercede in such disputes. You agree that Synura will not be
   responsible for any liability incurred as the result of such interactions.

   6.2. **Content Provided by Other Users**. The Services may contain User
   Content provided by other Registered Users. Synura is not responsible for and
   does not control User Content. Synura has no obligation to review or monitor,
   and does not approve, endorse or make any representations or warranties with
   respect to, User Content. You use all User Content and interact with other
   Registered Users at your own risk.

7. **FEES AND PURCHASE TERMS**

   7.1. **Service Subscription Fees**. If you sign up for a paid subscription
   term to the Services, you will be responsible for payment of the applicable
   fees for any Services (each, a “**Service Subscription Fee**”) at the time
   you create your Account and select your subscription type and payment package
   (each, a "**Service Commencement Date**"). Except as set forth in the
   Agreement, all fees for the Services are non-refundable. No contract will
   exist between you and Synura for the Services until Synura accepts your order
   by a confirmatory email or other appropriate means of communication.

   7.2. **Payment**. You agree to pay all fees or charges to your Account in
   accordance with the fees, charges and billing terms in effect at the time a
   fee or charge is due and payable in accordance with the Services. Synura
   reserves the right at any time to change its prices and billing methods,
   either immediately upon posting on the Services or by email delivery to you.
   If you sign up for a paid subscription term to the Services, you must provide
   Synura or its Payment Processor (as defined in Section 7.3) with a valid
   credit card (Visa, MasterCard, or any other issuer accepted by us) or any
   other payment provider as accepted by Synura in its sole discretion (each, a
   “**Payment Provider**”). Your Payment Provider agreement governs your use of
   the designated credit card or other Payment Provider account, and you must
   refer to that agreement, not this Agreement, to determine your rights and
   liabilities. By providing Synura or its Payment Processor with your credit
   card number or other Payment Provider account and associated payment
   information, you agree that Synura is authorized to immediately invoice your
   Account for all fees and charges as they become due and payable and that no
   additional notice or consent is required. You agree to immediately notify
   Synura or its Payment Processor of any change in your billing address or the
   credit card or other Payment Provider account used for payment hereunder.

   7.3. **Third-Party Payment Processor**. Synura uses third-party payment
   processors, which may include Stripe, Inc. and its affiliates, as the
   third-party service provider for payment services (e.g., card acceptance,
   merchant settlement, and related services) (a “**Payment Processor**”). By
   registering for a paid subscription through the Services, you agree to be
   bound by Stripe’s Privacy Policy (currently accessible at
   [https://stripe.com/us/privacy](https://stripe.com/us/privacy)) and its Terms
   of Service (currently accessible at
   [https://stripe.com/us/terms](https://stripe.com/us/terms)) and hereby
   consent and authorize Synura and Stripe to share any information and payment
   instructions you provide with one or more Payment Processor(s) to the minimum
   extent required to complete your transactions.

   7.4. **Taxes**. The payments required under Section 7.1 (Service Subscription
   Fees) of this Agreement do not include any Sales Tax that may be due in
   connection with the services provided under the Agreement. If Synura
   determines it has a legal obligation to collect a Sales Tax from you in
   connection with the Agreement, Synura may collect such Sales Tax in addition
   to the payments required under Section 7.1 (Service Subscription Fees) of the
   Agreement. If any Services or payments for any Services under the Agreement
   are subject to any Sales Tax in any jurisdiction and you have not remitted
   the applicable Sales Tax to Synura, you will be responsible for the payment
   of such Sales Tax and any related penalties or interest to the relevant tax
   authority, and you will indemnify Synura for any liability or expense Synura
   may incur in connection with such Sales Taxes. Upon Synura’s request, you
   will provide it with official receipts issued by the appropriate taxing
   authority, or other such evidence that you have paid all applicable taxes.
   For purposes of this section, “**Sales Tax**” shall mean any sales or use tax
   and any other tax measured by sales proceeds that is the functional
   equivalent of a sales tax where the applicable taxing jurisdiction does not
   otherwise impose a sales or use tax.

   7.5. **Withholding Taxes**. You agree to make all payments of fees to Synura
   free and clear of, and without reduction for, any withholding taxes. Any such
   taxes imposed on payments of fees to Synura will be your sole responsibility,
   and you will provide Synura with official receipts issued by the appropriate
   taxing authority, or such other evidence as we may reasonably request, to
   establish that such taxes have been paid.

   7.6. **Automatic Renewal**. Your subscription will continue indefinitely
   until terminated in accordance with the Agreement. **After your initial
   subscription period, and again after any subsequent subscription period, your
   subscription will automatically commence on the first day following the end
   of such period (each a “Renewal Commencement Date”) and continue for an
   additional equivalent period, at Synura’s then-current price for such
   subscription. You agree that your Account will be subject to this automatic
   renewal feature unless you cancel your subscription at least thirty (30) days
   prior to the Renewal Commencement Date (or in the event that you receive a
   notice from Synura that your subscription will be automatically renewed, you
   will have thirty (30) days from the date of the Synura notice), by logging
   into and going to the “Change/Cancel Membership” page of your “Account
   Settings” page.** If you do not wish your subscription to renew
   automatically, or if you want to change or terminate your subscription,
   please contact Synura at [contact@synura.com](mailto:contact@synura.com) or
   log in and go to the “Change/Cancel Membership” page on your “Account
   Settings” page. If you cancel your subscription, you may use your
   subscription until the end of your then-current subscription term; your
   subscription will not be renewed after your then-current term expires.
   However, you will not be eligible for a prorated refund of any portion of the
   subscription fee paid for the then-current subscription period. By
   subscribing for a paid subscription, you authorize Synura to charge your
   Payment Provider now, and again at the beginning of any subsequent
   subscription period. Upon renewal of your subscription, if Synura does not
   receive payment from your Payment Provider, (a) you agree to pay all amounts
   due on your Account upon demand and/or (b) you agree that Synura may either
   terminate or suspend your subscription and continue to attempt to charge your
   Payment Provider until payment is received (upon receipt of payment, your
   Account will be activated and for purposes of automatic renewal, your new
   subscription commitment period will begin as of the day payment was
   received).

   7.7. **Free Subscriptions, Trials and Other Promotions**. If you register for
   a free tier of the Services, you will not be charged unless you either
   upgrade your Account to a paid subscription level, or attempt to access
   features of the Services which are only available for paid subscription
   types. You will be prompted to provide a Payment Provider within the Services
   if you attempt to access such features. Any free trial or other promotion
   that provides paid subscription-level access to the Services must be used
   within the specified time of the trial. At the end of the trial period, your
   use of that Service will expire and any further use of the Service is
   prohibited unless you pay the applicable subscription fee. If you are
   inadvertently charged for a subscription, please contact Synura to have the
   charges reversed.

8. **INDEMNIFICATION**. You agree to indemnify and hold Synura, its parents,
   subsidiaries, affiliates, officers, employees, agents, partners, suppliers,
   and licensors (each, a “**Synura Party**” and collectively, the “**Synura
   Parties**”) harmless from any losses, costs, liabilities and expenses
   (including reasonable attorneys’ fees) relating to or arising out of any and
   all of the following: (a) Your Content; (b) your use of any Service in
   violation of the Agreement; (c) your violation of any rights of another
   party, including any Registered Users; or (d) your violation of any
   applicable laws, rules or regulations. Synura reserves the right, at its own
   cost, to assume the exclusive defense and control of any matter otherwise
   subject to indemnification by you, in which event you agree to fully
   cooperate with Synura in asserting any available defenses. This provision
   does not require you to indemnify any of the Synura Parties for any
   unconscionable commercial practice by such party or for such party’s fraud,
   deception, false promise, misrepresentation or concealment, or suppression or
   omission of any material fact in connection with any Services provided
   hereunder. You agree that the provisions in this section will survive any
   termination of your Account, the Agreement and/or your access to the
   Services.

9. **DISCLAIMER OF WARRANTIES AND CONDITIONS**

   9.1. **As Is**. YOU EXPRESSLY UNDERSTAND AND AGREE THAT TO THE EXTENT
   PERMITTED BY APPLICABLE LAW, YOUR USE OF THE SERVICES IS AT YOUR SOLE RISK,
   AND THE SERVICES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITH
   ALL FAULTS. SYNURA EXPRESSLY DISCLAIMS ALL WARRANTIES, REPRESENTATIONS, AND
   CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS
   FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARISING FROM USE OF THE
   SERVICES.

   9.1.a. SYNURA MAKES NO WARRANTY, REPRESENTATION OR CONDITION THAT: (1) THE
   SERVICES WILL MEET YOUR REQUIREMENTS; (2) YOUR USE OF THE SERVICES WILL BE
   UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE; OR (3) THE RESULTS THAT MAY BE
   OBTAINED FROM USE OF THE SERVICES WILL BE ACCURATE OR RELIABLE.

   9.1.b. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM
   SYNURA OR THROUGH THE SERVICES WILL CREATE ANY WARRANTY NOT EXPRESSLY MADE
   HEREIN.

   9.1.c. FROM TIME TO TIME, SYNURA MAY OFFER NEW “BETA” FEATURES OR TOOLS WITH
   WHICH ITS USERS MAY EXPERIMENT. SUCH FEATURES OR TOOLS ARE OFFERED SOLELY FOR
   EXPERIMENTAL PURPOSES AND WITHOUT ANY WARRANTY OF ANY KIND, AND MAY BE
   MODIFIED OR DISCONTINUED AT SYNURA’S SOLE DISCRETION. THE PROVISIONS OF THIS
   SECTION APPLY WITH FULL FORCE TO SUCH FEATURES OR TOOLS.

   9.2 **No Liability for Conduct of Third Parties**. YOU ACKNOWLEDGE AND AGREE
   THAT SYNURA PARTIES ARE NOT LIABLE, AND YOU AGREE NOT TO SEEK TO HOLD SYNURA
   PARTIES LIABLE, FOR THE CONDUCT OF THIRD PARTIES, INCLUDING OPERATORS OF
   EXTERNAL SITES AND OTHER USERS OF THE SERVICES, AND THAT THE RISK OF INJURY
   FROM SUCH THIRD PARTIES RESTS ENTIRELY WITH YOU. YOU RELEASE SYNURA FROM ALL
   LIABILITY FOR YOU HAVING ACQUIRED OR NOT ACQUIRED CONTENT THROUGH THE
   SERVICES. WE MAKE NO REPRESENTATIONS CONCERNING ANY CONTENT CONTAINED IN OR
   ACCESSED THROUGH THE SERVICES, AND WE WILL NOT BE RESPONSIBLE OR LIABLE FOR
   THE ACCURACY, COPYRIGHT COMPLIANCE, OR LEGALITY OF MATERIAL OR CONTENT
   CONTAINED IN OR ACCESSED THROUGH THE SERVICES.

   9.3. **Third-Party Materials**. As a part of the Services, you may have
   access to materials that are hosted by another party. You agree that it is
   impossible for Synura to monitor such materials and that you access these
   materials at your own risk.

10. **LIMITATION OF LIABILITY**

    10.1. **Disclaimer of Certain Damages**. YOU UNDERSTAND AND AGREE THAT, TO
    THE FULLEST EXTENT PROVIDED BY LAW, IN NO EVENT SHALL SYNURA PARTIES BE
    LIABLE FOR ANY LOSS OF PROFITS, REVENUE OR DATA, INDIRECT, INCIDENTAL,
    SPECIAL, OR CONSEQUENTIAL DAMAGES, OR DAMAGES OR COSTS DUE TO LOSS OF
    PRODUCTION OR USE, BUSINESS INTERRUPTION, OR PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES, IN EACH CASE WHETHER OR NOT SYNURA HAS BEEN ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THE
    AGREEMENT OR ANY COMMUNICATIONS, INTERACTIONS OR MEETINGS WITH OTHER USERS
    OF THE SERVICES, ON ANY THEORY OF LIABILITY. THE FOREGOING LIMITATION OF
    LIABILITY SHALL NOT APPLY TO LIABILITY OF A SYNURA PARTY FOR (i) DEATH OR
    PERSONAL INJURY CAUSED BY A SYNURA PARTY’S NEGLIGENCE; OR FOR (ii) ANY
    INJURY CAUSED BY A SYNURA PARTY’S FRAUD OR FRAUDULENT MISREPRESENTATION.

    10.2. **Cap on Liability**. TO THE FULLEST EXTENT PROVIDED BY LAW, SYNURA
    PARTIES WILL NOT BE LIABLE TO YOU FOR MORE THAN THE TOTAL AMOUNT PAID TO
    SYNURA BY YOU DURING THE THREE (3)-MONTH PERIOD PRIOR TO THE ACT, OMISSION
    OR OCCURRENCE GIVING RISE TO SUCH LIABILITY;<sup> </sup>OR THE REMEDY OR
    PENALTY IMPOSED BY THE STATUTE UNDER WHICH SUCH CLAIM ARISES. THE FOREGOING
    CAP ON LIABILITY SHALL NOT APPLY TO LIABILITY OF A SYNURA PARTY FOR (i)
    DEATH OR PERSONAL INJURY CAUSED BY A SYNURA PARTY’S NEGLIGENCE; OR FOR (ii)
    ANY INJURY CAUSED BY A SYNURA PARTY’S FRAUD OR FRAUDULENT MISREPRESENTATION.

    10.3. **User Content**. EXCEPT FOR SYNURA’S OBLIGATIONS TO PROTECT YOUR
    PERSONAL DATA AS SET FORTH IN THE SYNURA’S PRIVACY POLICY, SYNURA ASSUMES NO
    RESPONSIBILITY FOR THE TIMELINESS, DELETION, MIS-DELIVERY OR FAILURE TO
    STORE ANY CONTENT (INCLUDING, BUT NOT LIMITED TO, YOUR CONTENT AND USER
    CONTENT), USER COMMUNICATIONS OR PERSONALIZATION SETTINGS.

    10.4. **Exclusion of Damages**. CERTAIN JURISDICTIONS DO NOT ALLOW THE
    EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME
    OR ALL OF THE ABOVE EXCLUSIONS OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU
    MIGHT HAVE ADDITIONAL RIGHTS.

    10.5. **Basis of the Bargain**. THE LIMITATIONS OF DAMAGES SET FORTH ABOVE
    ARE FUNDAMENTAL ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN SYNURA AND YOU.

11. **RESPECTING COPYRIGHT**

    11.1. **Procedure for Making Claims of Copyright Infringement**. Synura
    respects the intellectual property rights of others and follows the
    requirements set forth in the Digital Millennium Copyright Act (“**DMCA**”)
    and other applicable laws. It is Synura’s policy to terminate membership
    privileges of any Registered User who repeatedly infringes copyright upon
    prompt notification to Synura by the copyright owner or the copyright
    owner’s legal agent. Without limiting the foregoing, if you believe that
    your work has been copied and posted on the Services in a way that
    constitutes copyright infringement, please provide our Copyright Agent with
    the following information: (a) an electronic or physical signature of the
    person authorized to act on behalf of the owner of the copyright interest;
    (b) a description of the copyrighted work that you claim has been infringed;
    (c) a description of the location on the Services of the material that you
    claim is infringing, such as a URL; (d) your address, telephone number and
    email address; (e) a written statement by you that you have a good faith
    belief that the disputed use is not authorized by the copyright owner, its
    agent or the law; and (f) a statement by you, made under penalty of perjury,
    that the above information in your notice is accurate and that you are the
    copyright owner or authorized to act on the copyright owner’s behalf.
    Contact information for Synura’s Copyright Agent for notice of claims of
    copyright infringement is as follows:

    ```text
    Jason Yavorska
    548 Market St
    PMB 25791
    San Francisco, CA 94104
    Email: jason@synura.com
    ```

    11.2. **Synura’s Actions Upon Receipt of Notifications of Claimed
    Infringement**. If Synura receives a complete notification from a rights
    holder or someone acting on their behalf claiming that content on a channel
    infringes the rights holder’s copyright, Synura generally notifies the
    applicable Registered User. At the same time, we will generally remove or
    disable access to recorded or other content containing the claimed
    infringing material. We may suspend an Account if the claimed infringement
    is continuing at the time we receive the rights holder’s notification.

    11.3. **Synura’s Actions Upon Receipt of Counter-Notifications**. If Synura
    receives a complete counter-notification from the applicable Registered
    User, Synura generally notifies the claimant who submitted the original
    notification. At the same time, we may restore recorded content containing
    the claimed infringing material. Disputed notifications generally will not
    qualify for a strike under our Repeat Infringer Policy (discussed in more
    detail below), unless and until the dispute has been resolved by the
    applicable Registered User and the rights holder. Please note that Synura
    may not be able to reinstate certain material or access to it due to factors
    such as the passage of time or the format in which the material was
    broadcast or stored.

    11.4. **Synura’s Actions Upon Receipt of Retractions**. If Synura receives a
    retraction from a rights holder or someone acting on their behalf, we may
    restore recorded content containing the claimed infringing material and the
    disputed notification will not qualify for a strike under our Repeat
    Infringer Policy (as provided by Section 11.7 (Repeat Infringer Policy)).
    Please note that Synura may not be able to reinstate certain material or
    access to it due to factors such as the passage of time or the format in
    which the material was broadcast or stored.

    11.5. **How to Submit a Counter-Notification**. If you are a Registered User
    and a notification of claimed infringement has been submitted against Your
    Content, we encourage you to review the details we have provided and
    consider your options. If you believe that the notification, and any action
    Synura has taken as a result, was sent due to a mistake (for example, you
    believe your actions qualify as fair use under U.S. law) or
    misidentification, then you may send us a counter-notification. To send a
    counter-notification, please provide all of the following information: (a)
    URL(s) where the material that was the subject of the notification of
    claimed infringement appeared before it was identified, removed, or access
    to it was disabled; (b) name of the claimant who submitted the notification;
    (c) your Synura username, if any; (d) your full legal name; (e) your email
    address; (f) your full postal address; (g) your phone number; and (h) a
    statement by you, made under penalty of perjury, that the above information
    in your notice is accurate and that you have a good faith belief that the
    material identified in the notification was identified, removed, and/or
    disabled as a result of mistake or misidentification. You also have the
    option to explain why you believe there was a mistake or misidentification.
    Counter-notifications that include all of the information above should be
    sent to Synura’s Copyright Agent as identified in Section 11.1 (Procedure
    for Making Claims of Copyright Infringement). Keep in mind that failure to
    provide this information could result in Synura being unable to take action
    in response to your counter-notification.

    11.6. **How to Submit a Retraction**. If you are a Registered User who has
    received a notification of claimed infringement, you can seek a retraction
    by reaching out to the rights holder or agent who sent the notification
    directly. Synura will honor retractions of notifications from the party that
    originally submitted the notifications. If you are a rights holder or agent
    who submitted a notification that you wish to retract, please contact
    Synura’s Copyright Agent as identified in Section 11.1 (Procedure for Making
    Claims of Copyright Infringement) with the following information: (a) the
    date of your original notification; (b) the copyrighted work(s) allegedly
    infringed; (c) the URL(s) where the allegedly infringing material could be
    found; and (d) a signature.

    11.7. **Repeat Infringer Policy**. Synura will terminate a Registered User’s
    access to the Services if such Registered User is determined by Synura to be
    a “repeat infringer” of copyrighted works on the Services. Under our policy,
    a Registered User will be considered a repeat infringer if they accrue three
    copyright strikes. Furthermore, we may in appropriate cases and at our sole
    discretion, limit access to the Services and/or terminate the Accounts of
    any Registered User who blatantly and egregiously infringes the intellectual
    property rights of others, whether or not repeat infringement has occurred.
    Registered Users generally earn a strike when Synura receives a complete
    notification of infringement and does not receive a complete
    counter-notification regarding the alleged infringement or a retraction of
    the notification. Strikes are not permanent, but rather are associated with
    an Account for enough time for Synura to determine whether the Registered
    User is engaging in repeated infringement such that termination is necessary
    under this policy. When determining whether Registered Users are repeat
    infringers under this policy, we take into consideration complete
    notifications of claimed infringement from rights holders, complete
    counter-notifications from Registered Users, retractions from rights
    holders, and other relevant factors and circumstances. Synura may also ask
    the complainant and/or Registered User for more information where we think
    it’s necessary to fairly apply our Repeat Infringer Policy. If a relevant
    court rules that a Registered User is an “infringer” or “repeat infringer”
    on Synura, we will take that ruling as conclusive under our Repeat Infringer
    Policy. To provide judicial determinations showing that a Registered User is
    an infringer, or a repeat infringer, on the Services, please forward it to
    our Synura’s Copyright Agent as identified in Section 11.1 (Procedure for
    Making Claims of Copyright Infringement) with “court ruling regarding
    infringer/repeat infringer” in the subject line. Synura personnel will
    review the submission and may contact the complainant and/or Registered User
    to verify the court ruling and understand its scope.

12. **MONITORING AND ENFORCEMENT**. Synura reserves the right to: (a) remove or
    refuse to post any of Your Content for any or no reason in our sole
    discretion; (b) take any action with respect to any of your Content that we
    deem necessary or appropriate in our sole discretion, including if we
    believe that such Content violates the Agreement, infringes any intellectual
    property right or other right of any person or entity, threatens the
    personal safety of users of the Services or the public, or could create
    liability for Synura; (c) take appropriate legal action, including without
    limitation, referral to law enforcement, for any illegal or unauthorized use
    of the Services; and/or (d) terminate or suspend your access to all or part
    of the Services for any or no reason, including without limitation, any
    violation of this Agreement.

    If Synura becomes aware of any possible violations by you of the Agreement,
    Synura reserves the right to investigate such violations. If, as a result of
    the investigation, Synura believes that criminal activity has occurred,
    Synura reserves the right to refer the matter to, and to cooperate with, any
    and all applicable legal authorities. Synura is entitled, except to the
    extent prohibited by applicable law, to disclose any information or
    materials on or in the Services, including Your Content, in Synura’s
    possession in connection with your use of the Services, to (i) comply with
    applicable laws, legal process or governmental request; (ii) enforce the
    Agreement, (iii) respond to any claims that Your Content violates the rights
    of third parties, (iv) respond to your requests for customer service, or (v)
    protect the rights, property or personal safety of Synura, its Registered
    Users or the public, and all enforcement or other government officials, as
    Synura in its sole discretion believes to be necessary or appropriate.<sup>
    </sup>

13. **TERM AND TERMINATION**

    13.1. **Term**. The Agreement commences on the date when you accept them (as
    described in the preamble above) and remain in full force and effect while
    you use the Services, unless terminated earlier in accordance with the
    Agreement.

    13.2. **Prior Use**. Notwithstanding the foregoing, you hereby acknowledge
    and agree that the Agreement commenced on the earlier to occur of (a) the
    date you first used the Services or (b) the date you accepted the Agreement,
    and will remain in full force and effect while you use any Services, unless
    earlier terminated in accordance with the Agreement.

    13.3. **Termination of Services by Synura**. You will have thirty (30) days
    from the Service Commencement Date, or any Renewal Commencement Date, for
    any Services hereunder, to cancel such Service, in which case Synura will
    refund your Service Subscription Fee, if already paid pursuant to Section
    7.1 (Service Subscription Fees) or 7.2 (Payment), for the applicable
    Service. Except as set forth above, the Service Subscription Fee for any
    Service shall be non-refundable. If timely payment cannot be charged to your
    Payment Provider for any reason, if you have materially breached any
    provision of the Agreement, or if Synura is required to do so by law (e.g.,
    where the provision of the Services is, or becomes, unlawful), Synura has
    the right to, immediately and without notice, suspend or terminate any
    Services provided to you. You agree that all terminations for cause shall be
    made in Synura’s sole discretion and that Synura shall not be liable to you
    or any third party for any termination of your Account.

    13.4. **Termination of Services by You**. If you want to terminate the
    Services provided by Synura, you may do so by (a) notifying Synura at any
    time and (b) closing your Account for all of the Services that you use. Your
    notice should be sent, in writing, to Synura’s address set forth below. THE
    SERVICES WILL CONTINUE AT THE END OF EACH SUBSCRIPTION PERIOD UNLESS YOU
    CANCEL YOUR SUBSCRIPTION IN ACCORDANCE WITH THE PROCEDURE SET FORTH IN
    SECTION 7.6 (AUTOMATIC RENEWAL).

    13.5. **Effect of Termination**. Termination of any Service includes removal
    of access to such Service and barring of further use of the Service.
    Termination of all Services also includes deletion of your password and all
    related information, files and Content associated with or inside your
    Account (or any part thereof), including Your Content. Upon termination of
    any Service, your right to use such Service will automatically terminate
    immediately. You understand that any termination of Services may involve
    deletion of Your Content associated therewith from our live databases.
    Synura will not have any liability whatsoever to you for any suspension or
    termination, including for deletion of Your Content. All provisions of the
    Agreement which by their nature should survive, shall survive termination of
    Services, including without limitation, ownership provisions, warranty
    disclaimers, and limitation of liability.

    13.6. **No Subsequent Registration**. If your registration(s) with, or
    ability to access, the Services or any other Synura community, is
    discontinued by Synura due to your violation of any portion of the Agreement
    or for conduct otherwise inappropriate for the community, then you agree
    that you shall not attempt to re-register with or access the Services or any
    Synura community through use of a different member name or otherwise, and
    you acknowledge that you will not be entitled to receive a refund for fees
    related to those Services to which your access has been terminated. In the
    event that you violate the immediately preceding sentence, Synura reserves
    the right, in its sole discretion, to immediately take any or all of the
    actions set forth herein without any notice or warning to you.

14. **INTERNATIONAL USERS**. The Services can be accessed from countries around
    the world and may contain references to Services and Content that are not
    available in your country. These references do not imply that Synura intends
    to announce such Services or Content in your country. The Services are
    controlled and offered by Synura from its facilities in the United States of
    America. Synura makes no representations that the Services are appropriate
    or available for use in other locations. Those who access or use the
    Services from other countries do so at their own volition and are
    responsible for compliance with local law.

15. **DISPUTE RESOLUTION**. **Please read the following arbitration agreement in
    this section (“Arbitration Agreement”) carefully. It requires U.S. users to
    arbitrate disputes with Synura and limits the manner in which you can seek
    relief from us.**

    15.1 **Applicability of Arbitration Agreement**. You agree that any dispute,
    claim, or request for relief relating in any way to your access or use of
    the Services, to any products sold or distributed through the Services, or
    to any aspect of your relationship with Synura, will be resolved by binding
    arbitration, rather than in court, except that (a) you may assert claims or
    seek relief in small claims court if your claims qualify, and (b) you or
    Synura may seek equitable relief in court for infringement or other misuse
    of intellectual property rights. **This Arbitration Agreement shall apply,
    without limitation, to all disputes or claims and requests for relief that
    arose or were asserted before the effective date of this Agreement or any
    prior version of this Agreement.**

    15.2. **Arbitration Rules and Forum**. The Federal Arbitration Act governs
    the interpretation and enforcement of this Arbitration Agreement. To begin
    an arbitration proceeding, you must send a letter requesting arbitration and
    describing your dispute or claim or request for relief to our registered
    agent: Resident Agents Inc., 8 The Green, Ste R, City of Dover, County of
    Kent, 19901. The arbitration will be conducted by JAMS, an established
    alternative dispute resolution provider. Disputes involving claims,
    counterclaims, or request for relief under $250,000, not inclusive of
    attorneys’ fees and interest, shall be subject to JAMS’s most current
    version of the Streamlined Arbitration Rules and procedures available at
    [http://www.jamsadr.com/rules-streamlined-arbitration/](http://www.jamsadr.com/rules-streamlined-arbitration/);
    all other disputes shall be subject to JAMS’s most current version of the
    Comprehensive Arbitration Rules and Procedures, available at
    [http://www.jamsadr.com/rules-comprehensive-arbitration/](http://www.jamsadr.com/rules-comprehensive-arbitration/).
    JAMS’s rules are also available at www.jamsadr.com or by calling JAMS at
    800-352-5267. If JAMS is not available to arbitrate, the parties will select
    an alternative arbitral forum. If the arbitrator finds that you cannot
    afford to pay JAMS’s filing, administrative, hearing and/or other fees and
    cannot obtain a waiver from JAMS, Synura will pay them for you. In addition,
    Synura will reimburse all such JAMS’s filing, administrative, hearing and/or
    other fees for disputes, claims, or requests for relief totaling less than
    $10,000 unless the arbitrator determines the claims are frivolous. You may
    choose to have the arbitration conducted by telephone, based on written
    submissions, or in person in the country where you live or at another
    mutually agreed location. Any judgment on the award rendered by the
    arbitrator may be entered in any court of competent jurisdiction.

    15.3. **Authority of Arbitrator**. The arbitrator shall have exclusive
    authority to (a) determine the scope and enforceability of this Arbitration
    Agreement and (b) resolve any dispute related to the interpretation,
    applicability, enforceability or formation of this Arbitration Agreement
    including, but not limited to, any assertion that all or any part of this
    Arbitration Agreement is void or voidable. The arbitration will decide the
    rights and liabilities, if any, of you and Synura. The arbitration
    proceeding will not be consolidated with any other matters or joined with
    any other cases or parties. The arbitrator shall have the authority to grant
    motions dispositive of all or part of any claim. The arbitrator shall have
    the authority to award monetary damages and to grant any non-monetary remedy
    or relief available to an individual under applicable law, the arbitral
    forum’s rules, and the Agreement (including the Arbitration Agreement). The
    arbitrator shall issue a written award and statement of decision describing
    the essential findings and conclusions on which the award is based,
    including the calculation of any damages awarded. The arbitrator has the
    same authority to award relief on an individual basis that a judge in a
    court of law would have. The award of the arbitrator is final and binding
    upon you and us.

    15.4. **Waiver of Jury Trial**. YOU AND SYNURA HEREBY WAIVE ANY
    CONSTITUTIONAL AND STATUTORY RIGHTS TO SUE IN COURT AND HAVE A TRIAL IN
    FRONT OF A JUDGE OR A JURY. You and Synura are instead electing that all
    disputes, claims, or requests for relief shall be resolved by arbitration
    under this Arbitration Agreement, except as specified in Section 15.1
    (Applicability of Arbitration Agreement) above. An arbitrator can award on
    an individual basis the same damages and relief as a court and must follow
    this Agreement as a court would. However, there is no judge or jury in
    arbitration, and court review of an arbitration award is subject to very
    limited review.

    15.5. **Waiver of Class or Other Non-Individualized Relief**. ALL DISPUTES,
    CLAIMS, AND REQUESTS FOR RELIEF WITHIN THE SCOPE OF THIS ARBITRATION
    AGREEMENT MUST BE ARBITRATED ON AN INDIVIDUAL BASIS AND NOT ON A CLASS OR
    COLLECTIVE BASIS, ONLY INDIVIDUAL RELIEF IS AVAILABLE, AND CLAIMS OF MORE
    THAN ONE CUSTOMER OR USER CANNOT BE ARBITRATED OR CONSOLIDATED WITH THOSE OF
    ANY OTHER CUSTOMER OR USER. If a decision is issued stating that applicable
    law precludes enforcement of any of this section’s limitations as to a given
    dispute, claim, or request for relief, then such aspect must be severed from
    the arbitration and brought into the State or Federal Courts located in
    Santa Clara County, California. All other disputes, claims, or requests for
    relief shall be arbitrated.

    15.6. **30-Day Right to Opt Out**. You have the right to opt out of the
    provisions of this Arbitration Agreement by sending written notice of your
    decision to opt out to: [contact@synura.com](mailto:contact@synura.com),
    within thirty (30) days after first becoming subject to this Arbitration
    Agreement. Your notice must include your name and address, your Synura
    username (if any), the email address you used to set up your Synura account
    (if you have one), and an unequivocal statement that you want to opt out of
    this Arbitration Agreement. If you opt out of this Arbitration Agreement,
    all other parts of this Agreement will continue to apply to you. Opting out
    of this Arbitration Agreement has no effect on any other arbitration
    agreements that you may currently have, or may enter in the future, with us.

    15.7. **Severability**. Except as provided in Section 15.5 (Waiver of Class
    or Other Non-Individualized Relief), if any part or parts of this
    Arbitration Agreement are found under the law to be invalid or
    unenforceable, then such specific part or parts shall be of no force and
    effect and shall be severed and the remainder of the Arbitration Agreement
    shall continue in full force and effect.

    15.8. **Survival of Agreement**. This Arbitration Agreement will survive the
    termination of your relationship with Synura.

    15.9. **Modification**. Notwithstanding any provision in this Agreement to
    the contrary, we agree that if Synura makes any future material change to
    this Arbitration Agreement, you may reject that change within thirty (30)
    days of such change becoming effective by writing Synura at the following
    address: [contact@synura.com](mailto:contact@synura.com).

16. **THIRD-PARTY WEBSITES AND APPLICATIONS**. The Services may contain links to
    third-party websites (“**Third-Party Websites**”) and applications
    (“**Third-Party Applications**”). When you click on a link to a Third-Party
    Website or Third-Party Application, we will not warn you that you have left
    the Services and are subject to the terms and conditions (including privacy
    policies) of another website or destination. Such Third-Party Websites and
    Third-Party Applications are not under the control of Synura. Synura is not
    responsible for any Third-Party Websites or Third-Party Applications. Synura
    provides these Third-Party Websites and Third-Party Applications only as a
    convenience and does not review, approve, monitor, endorse, warrant, or make
    any representations with respect to Third-Party Websites or Third-Party
    Applications, or any product or service provided in connection therewith.
    You use all links in Third-Party Websites and Third-Party Applications at
    your own risk. When you leave our Website, the Agreement and our policies no
    longer govern. You should review applicable terms and policies, including
    privacy and data gathering practices, of any Third-Party Websites or
    Third-Party Applications, and make whatever investigation you feel necessary
    or appropriate before proceeding with any transaction with any third party.

17. **GENERAL PROVISIONS**

    17.1. **Governing Law**. Any dispute, claim or request for relief relating
    in any way to your use of the services will be governed and interpreted by
    and under the laws of the state of California, consistent with the Federal
    Arbitration Act, without giving effect to any principles that provide for
    the application of the law of any other jurisdiction. The United Nations
    Convention on Contracts for the International Sale of Goods is expressly
    excluded from this Agreement.

    17.2. **Exclusive Venue**. To the extent the parties are permitted under
    this Agreement to initiate litigation in a court, both you and Synura agree
    that all claims and disputes arising out of or relating to the Agreement
    will be litigated exclusively in the state or federal courts located in
    Santa Clara County, California.

    17.3. **Electronic Communications**. The communications between you and
    Synura may take place via electronic means, whether you visit the Services
    or send Synura emails, or whether Synura posts notices on the Services or
    communicates with you via email. For contractual purposes, you (a) consent
    to receive communications from Synura in an electronic form; and (b) agree
    that all terms and conditions, agreements, notices, disclosures, and other
    communications that Synura provides to you electronically satisfy any legal
    requirement that such communications would satisfy if it were to be in
    writing. The foregoing does not affect your statutory rights, including but
    not limited to the Electronic Signatures in Global and National Commerce Act
    at 15 U.S.C. §7001 et seq.

    17.4. **Assignment**. The Agreement, and your rights and obligations
    hereunder, may not be assigned, subcontracted, delegated or otherwise
    transferred by you without Synura’s prior written consent, and any attempted
    assignment, subcontract, delegation, or transfer in violation of the
    foregoing will be null and void.

    17.5. **Force Majeure**. Synura shall not be liable for any delay or failure
    to perform resulting from causes outside its reasonable control, including,
    but not limited to, acts of God, pandemics, war, terrorism, riots, embargos,
    acts of civil or military authorities, fire, floods, accidents, strikes or
    shortages of transportation facilities, fuel, energy, labor or materials.

    17.6. **Questions, Complaints, Claims**. If you have any questions,
    complaints or claims with respect to the Services, please contact us at:
    [contact@synura.com](mailto:contact@synura.com). We will do our best to
    address your concerns. If you feel that your concerns have been addressed
    incompletely, we invite you to let us know for further investigation.

    17.7. **Choice of Language**. It is the express wish of the parties that the
    Agreement and all related documents have been drawn up in English.

    17.8. **Notice**. Where Synura requires that you provide an email address,
    you are responsible for providing Synura with your most current email
    address. In the event that the last email address you provided to Synura is
    not valid, or for any reason is not capable of delivering to you any notices
    required/ permitted by the Agreement, Synura’s dispatch of the email
    containing such notice will nonetheless constitute effective notice. You may
    give notice to Synura at the following address: Resident Agents Inc., 8 The
    Green, Ste R, City of Dover, County of Kent, 19901. Such notice shall be
    deemed given when received by Synura by letter delivered by nationally
    recognized overnight delivery service or first class postage prepaid mail at
    the above address.

    17.9. **Waiver**. Any waiver or failure to enforce any provision of the
    Agreement on one occasion will not be deemed a waiver of any other provision
    or of such provision on any other occasion.

    17.10. **Severability**. If any portion of the Agreement is held invalid or
    unenforceable, that portion shall be construed in a manner to reflect, as
    nearly as possible, the original intention of the parties, and the remaining
    portions shall remain in full force and effect.

    17.11. **Export Control**. You may not use, export, import, or transfer any
    Services except as authorized by U.S. law, the laws of the jurisdiction in
    which you obtained the Services, and any other applicable laws. In
    particular, but without limitation, the Services may not be exported or
    re-exported (a) into any United States embargoed countries, or (b) to anyone
    on the U.S. Treasury Department’s list of Specially Designated Nationals or
    the U.S. Department of Commerce’s Denied Person’s List or Entity List. By
    using the Services, you represent and warrant that (i) you are not located
    in a country that is subject to a U.S. Government embargo, or that has been
    designated by the U.S. Government as a “terrorist supporting” country and
    (ii) you are not listed on any U.S. Government list of prohibited or
    restricted parties. You also will not use the Services for any purpose
    prohibited by U.S. law, including the development, design, manufacture or
    production of missiles, nuclear, chemical or biological weapons. You
    acknowledge and agree that products, services or technology provided by
    Synura are subject to the export control laws and regulations of the United
    States. You shall comply with these laws and regulations and shall not,
    without prior U.S. government authorization, export, re-export, or transfer
    Synura products, services or technology, either directly or indirectly, to
    any country in violation of such laws and regulations.

    17.12. **Consumer Complaints**. In accordance with California Civil Code
    §1789.3, you may report complaints to the Complaint Assistance Unit of the
    Division of Consumer Services of the California Department of Consumer
    Affairs by contacting them in writing at 1625 North Market Blvd., Suite N
    112, Sacramento, CA 95834, or by telephone at (800) 952-5210.

    17.13. **Entire Agreement**. The Agreement is the final, complete and
    exclusive agreement of the parties with respect to the subject matter hereof
    and supersedes and merges all prior discussions between the parties with
    respect to such subject matter.

18. **INTERNATIONAL PROVISIONS**. The following provisions shall apply only if
    you are located in the countries listed below.

    18.1. **United Kingdom**. A third party who is not a party to the Agreement
    has no right under the Contracts (Rights of Third Parties) Act 1999 to
    enforce any provision of the Agreement, but this does not affect any right
    or remedy of such third party which exists or is available apart from that
    Act.

    18.2. **Germany**. Notwithstanding anything to the contrary in Section 10
    (Limitation of Liability), Synura is also not liable for acts of simple
    negligence (unless they cause injuries to or death of any person), except
    when they are caused by a breach of any substantial contractual obligations
    (vertragswesentliche Pflichten).
