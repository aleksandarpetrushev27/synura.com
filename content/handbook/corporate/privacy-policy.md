---
title: Privacy policy
description:
  This policy describes the privacy practices of Synura, Inc. and what you can
  expect we do with your data.
---

# {{% param "title" %}}

Effective as of June 3, 2022.

This Privacy Policy describes how Synura, Inc. ("**Synura**," "**we**", “**us**”
or "**our**") handles personal information that we collect through our digital
properties that link to this Privacy Policy, including our website
(collectively, the “**Website**”), as well as through social media, our
marketing activities, our live events and other activities described in this
Privacy Policy.

Synura’s products provide collaborative, cloud-based workflows, and our products
and the Website are intended for our business customers. This Privacy Policy
does not apply to information that we process on behalf of our business
customers while providing the Synura product or services to them. Our use of
this information is restricted by our agreements with those business customers.
If you have concerns regarding personal information that we process on behalf of
a business, please direct your concerns to that business.

Our websites, products and services are designed for businesses and their
representatives. We do not offer products or services for use by individuals for
their personal, family or household purposes. Accordingly, we treat all personal
information we collect as pertaining to individuals in their capacities as
business representatives and not their individual capacities.

## Personal information we collect

**Information you provide to us.** Personal information you may provide to us
through the Website or otherwise includes:

- **Contact data**, such as your first and last name, salutation, email address,
  billing and mailing addresses, professional title and company name, and phone
  number.
- **Profile data**, such as the username or email address and password that you
  may set to establish an online account on the Website, a profile photograph,
  and any other information that you add to your account profile.
- **Communications** that we exchange with you, including when you contact us
  through the Website, social media, or otherwise.
- **Transactional data**, such as information relating to or needed to complete
  your orders on or through the Website, including order numbers and transaction
  history.
- **Marketing data**, such as your preferences for receiving our marketing
  communications and details about your engagement with them.
- **User-generated content**, such as profile pictures, photos, images, music,
  videos, comments, questions, messages, works of authorship, and other content
  or information that you generate, transmit, or otherwise make available on the
  Website, as well as associated metadata. Metadata includes information on how,
  when, where and by whom a piece of content was collected and how that content
  has been formatted or edited. Metadata also includes information that users
  can add or can have added to their content, such as keywords, geographical or
  location information, and other similar data.
- **Other data** not specifically listed here, which we will use as described in
  this Privacy Policy or as otherwise disclosed at the time of collection.

**Automatic data collection.** We, our service providers, and our business
partners may automatically log information about you, your computer or mobile
device, and your interaction over time with the Website, our communications and
other online services, such as:

- **Device data**, such as your computer’s or mobile device’s operating system
  type and version, manufacturer and model, browser type, screen resolution, RAM
  and disk size, CPU usage, device type (e.g., phone, tablet), IP address,
  unique identifiers (including identifiers used for advertising purposes),
  language settings, mobile device carrier, radio/network information (e.g.,
  Wi-Fi, LTE, 3G), and general location information such as city, state or
  geographic area.
- **Online activity data**, such as pages or screens you viewed, how long you
  spent on a page or screen, the website you visited before browsing to the
  Website, navigation paths between pages or screens, information about your
  activity on a page or screen, access times and duration of access, and whether
  you have opened our emails or clicked links within them.

**Cookies and similar technologies.** Some of the automatic collection described
above is facilitated by the following technologies:

- **Cookies**, which are small text files that websites store on user devices
  and that allow web servers to record users’ web browsing activities and
  remember their submissions, preferences, and login status as they navigate a
  site. Cookies used on our sites include both “session cookies” that are
  deleted when a session ends, “persistent cookies” that remain longer, “first
  party” cookies that we place and “third party” cookies that our third-party
  business partners and service providers place.
- **Local storage technologies**, like HTML5, that provide cookie-equivalent
  functionality but can store larger amounts of data on your device outside of
  your browser in connection with specific applications.
- **Web beacons**, also known as pixel tags or clear GIFs, which are used to
  demonstrate that a webpage or email was accessed or opened, or that certain
  content was viewed or clicked.

## How we use your personal information

We may use your personal information for the following purposes or as otherwise
described at the time of collection:

**Website delivery.** We may use your personal information to:

- provide, operate and improve the Website and our business;
- establish and maintain your user profile on the Website;
- enable security features of the Website, such as by sending you security codes
  via email or SMS, and remembering devices from which you have previously
  logged in;
- communicate with you about the Website, including by sending announcements,
  updates, security alerts, and support and administrative messages;
- communicate with you about events or contests in which you participate;
- understand your needs and interests, and personalize your experience with the
  Website and our communications; and
- provide support for the Website, and respond to your requests, questions and
  feedback.

**Research and development.** We may use your personal information for research
and development purposes, including to analyze and improve the Website and our
business.

**Marketing.** We and our service providers may collect and use your personal
information for marketing and advertising purposes. We may send you direct
marketing communications. You may opt-out of our marketing communications as
described in the Opt-out of marketing section below.

**Compliance and protection.** We may use your personal information to:

- comply with applicable laws, lawful requests, and legal process, such as to
  respond to subpoenas or requests from government authorities;
- protect our, your or others’ rights, privacy, safety or property (including by
  making and defending legal claims);
- audit our internal processes for compliance with legal and contractual
  requirements or our internal policies;
- enforce the terms and conditions that govern the Website; and
- prevent, identify, investigate and deter fraudulent, harmful, unauthorized,
  unethical or illegal activity, including cyberattacks and identity theft.

**With your consent.** In some cases, we may specifically ask for your consent
to collect, use or share your personal information, such as when required by
law.

**To create anonymous, aggregated or de-identified data.** We may create
anonymous, aggregated or de-identified data from your personal information and
other individuals whose personal information we collect. We make personal
information into anonymous, aggregated or de-identified data by removing
information that makes the data identifiable to you. We may use this anonymous,
aggregated or de-identified data and share it with third parties for our lawful
business purposes, including to analyze and improve the Website and promote our
business.

**Cookies and similar technologies.** In addition to the other uses included in
this section, we may use the Cookies and similar technologies described above
for the following purposes:

- **Technical operation**. To allow the technical operation of the Website.
- **Functionality**. To enhance the performance and functionality of our
  services.
- **Analytics**. To help us understand user activity on the Website, including
  which pages are most and least visited and how visitors move around the
  Website, as well as user interactions with our emails. For example, we use
  Google Analytics for this purpose. You can learn more about Google Analytics
  and how to prevent the use of Google Analytics relating to your use of our
  sites here:
  [https://tools.google.com/dlpage/gaoptout?hl=en](https://tools.google.com/dlpage/gaoptout?hl=en).

## How we share your personal information

We may share your personal information with the following parties and as
otherwise described in this Privacy Policy or at the time of collection.

**Affiliates.** Our corporate parent, subsidiaries, and affiliates, for purposes
consistent with this Privacy Policy.

**Service providers.** Third parties that provide services on our behalf or help
us operate the Website or our business (such as hosting, information technology,
customer support, email delivery, marketing, consumer research and website
analytics).

**Third parties designated by you.** We may share your personal data with third
parties where you have instructed us or provided your consent to do so.

**Professional advisors.** Professional advisors, such as lawyers, auditors,
bankers and insurers, where necessary in the course of the professional services
that they render to us.

**Authorities and others.** Law enforcement, government authorities, and private
parties, as we believe in good faith to be necessary or appropriate for the
compliance and protection purposes described above.

**Business transferees.**  Acquirers and other relevant participants in business
transactions (or negotiations of or due diligence for such transactions)
involving a corporate divestiture, merger, consolidation, acquisition,
reorganization, sale or other disposition of all or any portion of the business
or assets of, or equity interests in, Synura or our affiliates (including, in
connection with a bankruptcy or similar proceedings).

**Other users you invite.** Your profile and other user-generated content
(except for messages) may be visible to other users you invited. For example,
other users of the Website may have access to your information if you chose to
make your profile or other personal information available to them through an
invite. This information can be seen, collected and used, including being
cached, copied, screen captured or stored elsewhere, and we are not responsible
for any such use of this information.

## Your choices

In this section, we describe the rights and choices available to all users.

**Access or update your information.** If you have registered for an account
with us through the Website, you may review and update certain account
information by logging into the account and visiting the settings page.

**Opt-out of marketing communications.** You may opt-out of marketing-related
emails by following the opt-out or unsubscribe instructions at the bottom of the
email, or by [contacting us](#how-to-contact-us). Please note that if you choose
to opt-out of marketing-related emails, you may continue to receive
service-related and other non-marketing emails.

**Do Not Track.** Some Internet browsers may be configured to send “Do Not
Track” signals to the online services that you visit. We currently do not
respond to “Do Not Track” or similar signals. To find out more about “Do Not
Track,” please visit [http://www.allaboutdnt.com](http://www.allaboutdnt.com).

**Declining to provide information.** We need to collect personal information to
provide certain services. If you do not provide the information we identify as
required or mandatory, we may not be able to provide those services.

## Other sites and services

The Website may contain links to websites, mobile applications, and other online
services operated by third parties. In addition, our content may be integrated
into web pages or other online services that are not associated with us. These
links and integrations are not an endorsement of, or representation that we are
affiliated with, any third party. We do not control websites, mobile
applications or online services operated by third parties, and we are not
responsible for their actions. We encourage you to read the privacy policies of
the other websites, mobile applications and online services you use.

## Security

We employ a number of technical, organizational and physical safeguards designed
to protect the personal information we collect. However, security risk is
inherent in all internet and information technologies and we cannot guarantee
the security of your personal information.

## International data transfer

We are headquartered in the United States and may use service providers that
operate in other countries. Your personal information may be transferred to the
United States or other locations where privacy laws may not be as protective as
those in your state, province, or country.

## Children

The Website is not intended for use by anyone under 13 years of age. If you are
a parent or guardian of a child from whom you believe we have collected personal
information in a manner prohibited by law, please
[contact us](#how-to-contact-us). If we learn that we have collected personal
information through the Website from a child without the consent of the child’s
parent or guardian as required by law, we will comply with applicable legal
requirements to delete the information.

## Changes to this Privacy Policy

We reserve the right to modify this Privacy Policy at any time. If we make
material changes to this Privacy Policy, we will notify you by updating the date
of this Privacy Policy and posting it on the Website or other appropriate means.
Any modifications to this Privacy Policy will be effective upon our posting the
modified version (or as otherwise indicated at the time of posting). In all
cases, your use of the Website after the effective date of any modified Privacy
Policy indicates your acceptance of the modified Privacy Policy.

## How to contact us

- **Email**: contact@synura.com
- **Mail**: Synura, Inc., 548 Market St. PMB 25791, San Francisco, California
  94104-5401
