---
title: Investors & fundraising
description:
  This page describes how we work with our investors, as well as links to
  information that our investors might find useful.
---

# {{% param "title" %}}

{{% param "description" %}}

## Company direction

Our public mission and business overview can be found on our
[direction page](../../general/direction/).

### Pitch deck

We maintain a version of the company direction summarized into a
[pitch deck](https://docs.google.com/presentation/d/12MYJDI0cCc8Avh_i5ERLWwghbsMwNhjpTJeolOcbrTY/edit#slide=id.p)
(internal only).

## Monthly investor update

Each month we send an update to our investors, as soon as the last iteration of
the month is complete. The list of recipients is kept
[in Mailchimp](https://us12.admin.mailchimp.com/lists/members?id=1081460#p:1-s:25-sa:last_update_time-so:false)
and it includes both active as well as potential investors. Potential investors
are added at any time, but any who choose not to join our next round will be
removed at that point.

The investor update contains a financial health summary, special thanks and
asks, highlights and lowlights, and expectations for the next month. It also
contains metrics summarizing the
[last two completed iterations](../../general/product_development/#iteration-reviews)

## Biweekly team and investor Call

We have recurring biweekly calls for the entire team to meet with our investors
and discuss different objectives for the business.

Here are the "norms" for the call:

- Before the call, please add a discussion topic or update to the meeting
  [agenda](https://docs.google.com/document/d/11_bxolgHm-lpp1XQhSd0GaEI1Alvkk-KcZcS4hElcGE/edit#).
  Prefix the question with your name.
- In the call, we will go through them in order, giving each person the chance
  to voice their updates.
- Note that this call is sometimes joined by GitLab's "CEO Shadows" -- please
  see more about this program here:
  [https://about.gitlab.com/handbook/ceo/shadow/](https://about.gitlab.com/handbook/ceo/shadow/)
- The call is also sometimes streamed on Twitch, as long as there is nothing
  confidential in the agenda.

## Product demo

The latest demo (and other relevant videos) can be found on our
[YouTube channel](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg/videos).
