---
title: Benefits overview
description:
  This page contains general information about our benefits, including a summary
  and links to other pages with more details.
---

# {{% param "title" %}}

{{% param "description" %}} Note that some benefits may only apply to full-time
salaried employees - if you have any questions, you can always ask your manager.

See also our separate page with information on
[holidays & PTO](../holidays_pto/).

## Benefits for everyone

All full-time team members receive the following:

- You will be provided with a
  [company-owned laptop](../../people/onboarding/#purchasing-a-company-issued-device)
  and can also expense home office basics like headphones, lighting, monitor,
  desk, and other accessories. Please be responsible and purchase standard
  equipment needed for the kind of job you're doing (i.e., if you don't need
  professional lighting rigging or a top-class XDR monitor, for example, please
  don't purchase it.) If in doubt you can check with your manager.
- One of our best benefits, which applies to everyone, is that we are an
  [all-remote async company](../../general/communication/#communication-norms).

## Contract employees

For employees who choose to work with us on a contract basis, we are limited in
the number of benefits we can provide to avoid stepping over the line into
artificial full-time employment.

That said, full-time contract team members receive:

- Non-U.S. contractors receive equity in the company.
- Non-U.S. contractors can choose to be paid in USD or local currency.

Contractors can be set up and start working in a few days.

Why would you choose temporary contract employment? Ultimately, this is a
personal decision for you to make based on your own needs. Synura can support
direct hires anywhere in the world.

Be sure to check that your country is
[eligible for direct hire](https://help.pilot.co/en/articles/3672752-hire-list-of-supported-countries)
before choosing this option. Note that we are still able to hire you as a
contractor in
[most other countries](https://help.pilot.co/en/articles/3672748-pay-list-of-supported-countries)
(essentially any non-sanctioned country).

## Benefits for direct employees

In addition to the above benefits for everyone, direct (non-contract) employees
also receive the following.

- Twenty paid time off days per year to be used in any way you like (if you live
  in a country that requires additional days by statute, those are also
  honored.)
- Synura encourages team members to invest in their personal growth. Teammates
  may spend up to $600 per year on other courses or professional certifications,
  so long as they are discussed with their manager in advance, they have the
  time available while fulfilling their work responsibilities, and it is
  relevant to their responsibilities.
- Company-paid
  [end-of-year celebration](../holidays_pto/#end-of-year-celebration).
- Access to coworking space, if desired.
- U.S.-based team members additionally receive:
  - 100% coverage for team members, and 66% for dependents of a Health Plan
    Premium's monthly costs.
  - You can arrange reimbursement for your home internet bill.
- Non-US team members may receive additional benefits, including health care,
  but this is on a per-country basis and set up by our international employment
  provider [Pilot.co](https://pilot.co). You can ask your (potential) hiring
  manager what the specific benefits would be in your case.

Note that full-time employees take about 3-4 weeks to get set up before the
first day.
