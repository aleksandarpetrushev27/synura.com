---
title: Holidays & PTO
description:
  This page describes how holidays and PTO work, as well as details on how to
  properly submit and track your time off.
---

# {{% param "title" %}}

{{% param "description" %}} Note that some benefits may only apply to full-time
salaried employees - if you have any questions, you can always ask your manager.

## Taking time off

When you take any time off, you should follow this process:

- Let your manager and team know as soon as possible. (e.g., post a message on
  your team's Slack channel)
- Find someone to cover anything that needs covering while you're out. (i.e.,
  meetings, planned tasks, unfinished business, important Slack/email threads,
  anything where someone might be depending on you)
- Mark an all-day "out of office" event in Google Calendar for the day(s) you're
  taking off.

This process is the same for any days you take off, regardless of whether it's a
holiday or you just need a break.

## Holidays

At Synura, we have team members with various employment classifications in many
different countries around the world. We emulate GitLab’s “don’t ask, must tell”
policy and encourage people to take the time off. It's up to each teammate to
choose what days will work best for them, but we generally encourage you to take
your national holidays, in addition to some long weekends and vacations.

References that may be helpful:

- [GitLab's Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/)
- [GitLab's Guidance on Mental Health](https://about.gitlab.com/company/culture/all-remote/mental-health/)

Either way, it's up to you to ensure that your responsibilities are covered, and
your team knows that you're out of the office.

### End-of-year celebration

The end of the year is a natural time for celebration and togetherness. For many
companies, an "end-of-year party" is a time to be together with coworkers and to
share the experience with significant others. We also want these qualities, but
as an all-remote company, take a different approach.

We ask that each team member have a special celebratory dinner with their family
or close friends, courtesy of Synura, and then share pictures and stories from
the experience with us in the #celebrations Slack channel. We hope this will
help each person enjoy the festive spirit with their loved ones, and for us all
to get to know one another better through the stories.

### How to take time off

You can take your time off whenever you want. However, please be responsible for
when you take it and how you communicate it. Here are a few things to consider:

1. Check with your manager to try to avoid being out around a critical milestone
   or at the same time that others on your team will be out
2. Try to provide advance notice -- a general guideline would be to share twice
   as many days in advance as you'd like to take off. e.g. two days' notice is
   for one day off; two weeks in advance would be better if you want to take a
   full week.
   - Note that this does not apply to emergencies, and it is not a requirement.
     Sometimes fun things come up last minute that you want to do, but this
     guideline will help prevent leaving others in the lurch when you have some
     flexibility in scheduling.
3. Team members who are with us through an agency should also follow that
   agency's procedures in addition to ours.
4. Being part of a global remote team means you need to be highly organized and
   a considerate team player. Each team has busy times so it is always a good
   idea to check with them to ensure there is adequate coverage in place.
5. Engineers should ensure the following are in place before going on leave:
   - All tickets assigned to you are completed, verified, and closed
     - If there are pending tickets, discuss them with an engineer who can take
       them up while you are gone
   - There are no merge requests assigned to you that are not merged
   - If you are taking a longer leave, communicate with the team a few days
     before your leave

## Absenteeism

Our time off policy is flexible but requires proactive communication. Any team
member who takes 3 consecutive days without prior notification will be
considered to have voluntarily resigned.

If someone is absent, the manager will make effort to contact the team member or
someone listed as their emergency contact. Please share emergency contact info
with people-ops, so that we know who to call in case of an unexpected absence.

Managers can find information on who to call in our
[emergency contact info sheet](https://docs.google.com/spreadsheets/d/1I-pZDyoABuVSWIwVWBTszoBYAXNyS3jaE2SdxlTgNlQ/edit#gid=0).

## Communicating your time off

Once you've decided to take time off and your manager is aware, it should be
communicated to the rest of the team.

1. Mark the time as Out of office on your calendar.
   1. Select the Automatically decline new and existing meetings option so that
      your meeting status will show "declined"
2. You should add the time off using PTO by Roots to Slack. Please use the
   following syntax which uses the
   [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date notation in order to
   avoid confusion: OOO from YYYY-MM-DD to YYYY-MM-DD, please contact XXX for
   assistance.
3. If you won't check email at all, add an out-of-office response in your email
   settings with the start and end date of your time off so that people reaching
   out to you on email will be notified
4. It may be useful to share your planned time off as a **FYI** on your team's
   Slack channel(s), especially if you lead a team.
5. Make sure your manager and team are aware of when you will be out.

## PTO by Roots

[PTO by Roots](https://www.tryroots.io/pto) allows us to coordinate time off
seamlessly with intuitive Slack commands.

### Slack commands

- /pto-roots ooo Create an OOO event.
- /pto-roots me View your OOO dashboard to edit, add or remove OOO events.
- /pto-roots whosout See upcoming OOO for everyone in the channel where this
  command is used.
- /pto-roots @username Check if a particular person is OOO and if they are, see
  which of your co-workers are covering for them.
- /pto-roots feedback This is your direct line to support. At any time, use this
  command to report bugs or share your thoughts on how the product can be
  improved or what’s working well.
- /pto-roots help A top-level look at PTO by Roots items that you may need help
  with, and how to receive help.
- /pto-roots settings This is where you modify your profile and calendar
  settings. This is also where you opt-in and out-put for reminders, including
  monthly messages prompting you to consider what PTO you may take this month.

## Parental leave

Synura offers anyone (regardless of gender) who has been at Synura for six
months up to 16 weeks of 100% paid time off during the first year of parenthood.
This includes anyone who becomes a parent through childbirth or adoption. The
paid time off is per birth or adoption event and may be used only within the
first 12 months of the event.

We encourage parents to take the time they need. Synura team members will be
encouraged to decide for themselves the appropriate amount of time to take and
how to take it.

### New parent leave

Synura gives new parents six weeks of paid leave. After six weeks, if you don't
feel ready to return yet, we'll set up a quick call to discuss and work together
to come up with a plan to help you return to work gradually or when you're
ready.

### How to initiate Your parental leave

Some teams require more time to put a plan of action in place so we recommend
communicating your plan to your manager at least 3 months before your leave
starts. In the meantime, familiarize yourself with the steps below.

To initiate your parental leave, submit your time off by selecting the Parental
Leave category in PTO by Roots at least 30 days before your leave starts. We
understand that parental leave dates may change. You can edit your PTO by Roots
at a later time if you need to adjust the dates of your parental leave. You must
submit a tentative date at least 30 days in advance. Your manager will get
notified after you submit your leave and will send you an e-mail within 48 hours
confirming that they've been notified of your Parental Leave dates.

Please note, even though we have a "no ask, must tell" Paid Time Off Policy,
your Team Head must be aware of your leave at least 30 days before your leave
starts.

### Planning Your parental leave dates

Your 16 weeks of parental leave starts on the first day that you take off. This
day can be in advance of the day that the baby arrives. You don't have to take
your parental leave for one continuous period, we encourage you to plan and
arrange your Parental Leave in a way that suits you and your family's needs. You
may split your Parental Leave dates as you see fit, so long as it is within the
12 months of the birth or adoption event.

If you don't meet the initial requirements for paid leave, Synura payroll
coverage begins once you meet the requirements. Until then, you will not receive
pay. If for example, you are someone who qualifies after 6 months at Synura and
then goes on leave at the start of your 120th day at Synura, you would not
receive payment from Synura for the first 60 days. You would receive payment
from Synura for up to 60 additional days taken within a year from the birth
event.

You can change the dates of your parental leave via PTO by Roots. Your Team Head
will receive a notification every time you edit your Parental Leave dates. Make
sure your leave is under the Parental Leave category, otherwise your Team Head
won't get a notification.

Please note, if you are planning to change or extend your Parental Leave by
using a different type of leave such as PTO, unpaid leave, or any local
statutory leave, please send an e-mail to your Team Head.

### Administration of parental leave requests

For Your Team Head:

- PTO by Roots will notify Your Team Head of any upcoming parental leave.
- Notify the team member that the parental leave request was received by sending
  a confirmation e-mail.
- PTO by Roots will automatically update the status.
- Check if the team member has confirmed their return via email. If not, send
  the Return to Work e-mail to the team member and manager.
- Do a weekly audit of parental leave return dates. If a team member hasn't sent
  an e-mail within 3 days of returning, send them the return to work e-mail,
  copying the manager in.
