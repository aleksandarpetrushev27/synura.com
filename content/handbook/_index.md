---
title: Handbook
description:
  Welcome to the public handbook for Synura, which includes information on the
  company, how we work, benefits, and more.
---

# {{% param "title" %}}

{{% param "description" %}}

Our handbook is public for a few reasons. The first is that it gives our
customers insights into how we work together, and allows anyone to give us
feedback (and even propose changes) that help us make a better product. The
second is that it gives a chance for potential team members to learn a lot about
what their day to day will look like, helping them make a better informed
decision about joining us.

The content here is organized into a few sections:

## General

- [About us](general/about/)
- [Direction](general/direction/)
- [Competitive landscape](general/competitive_landscape/)
- [Values](general/values/)
- [Product development](general/product_development/)
- [Communication](general/communication/)
- [Marketing & brand](general/marketing/)
- [Sales](general/sales/)
- [Meetings](general/meetings/)
- [Tools](general/tools/)

## Corporate

- [Contact information](corporate/contact/)
- [Finance](corporate/finance/)
- [Investors & fundraising](corporate/investors/)
- [Legal & general business](corporate/legal/)
- [Privacy policy](corporate/privacy-policy/)
- [Terms of use (for website)](corporate/terms-of-use/)
- [Terms of service (for app)](corporate/terms-of-service-app/)

## People

- [Offboarding](people/offboarding/)
- [Onboarding](people/onboarding/)
- [PeopleOps](people/peopleops/)
- [Performance metrics & evaluation](people/performance/)
- [Recruiting](people/recruiting/)
- [Travel](people/travel/)

## Benefits

- [Benefits overview](benefits/overview/)
- [Holidays & PTO](benefits/holidays_pto/)

## Readmes

- [Jason Yavorska](readmes/jason.yavorska/)
