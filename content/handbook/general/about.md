---
title: "About Us"
description:
  "Learn about the team building Synura and some of the reasons we're passionate
  about helping teams with video."
layout: about
---

# About us

At Synura we're passionate about video. We are building a collaborative video
planning and editing app that helps streamline your creative workflow and offers
automatic, AI-powered edits.

Check out our [home page](/),
[company YouTube channel](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg),
and [handbook](/handbook/) (which includes our
[values](/handbook/general/values/) and
[company direction](/handbook/general/direction/)) for more info. Details on our
funding and investors can be found on our
[Crunchbase profile](https://www.crunchbase.com/organization/synura).

## Get in touch

We're always happy to talk to our users. If you have any questions, or feedback,
or would just like to chat, you can reach us any time at
[contact@synura.com](mailto:contact@synura.com).
