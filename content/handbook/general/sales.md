---
title: Sales
description:
  This page describes our sales processes and how we work with our users to sell
  our paid products.
---

# {{% param "title" %}}

{{% param "description" %}}

The [marketing and brand](../marketing/) page in the handbook also contains
important information relating to sales, in particular around how we use
marketing for lead generation.

## Strategy

We are an early-stage startup, and so 90% of what we're doing when we're talking
to customers is telling our story and building relationships that will
eventually convert into sales; in other words, **sales evangelism**.

Our sales process needs to be able to take an incomplete product, form a
narrative around it, and then listen to the (potential) customer. This feedback
is then brought back to the product for another iteration, and we want to keep
this learning loop as tight as possible so we
[do things that don't scale](http://paulgraham.com/ds.html) like having a
founder-led sales process.

Once this flow is well-defined and working routinely, that's the moment where we
will scale the sales team.

## Sales principles

There are a few key insights that will help us navigate early sales. These are
presented in no particular order but are adapted from mindset changes in the
book [Founding Sales](https://www.foundingsales.com/) and other sources.

### Embrace plenty, not scarcity

- Don't spend too much time on accounts that aren't a perfect fit.
- What's scarce in sales in time, so it's important to be efficient there over
  any other concern.

### Put activity over all else

- More time on the phone. More demos. More proposals are being sent. More emails
  are being sent. More dials. More keystrokes.
- Jump first and prepare midair. Template all communication, drive activity, and
  output will follow.

### Be direct & get down to business

- Ask "Do you have the problem I'm trying to solve? Are you in agreement that it
  needs a solution? Are you prepared to spend money to solve it?"
- Asking for a sale is not optional, and it will quickly become second nature.

### Build many shallow relationships

- Have dozens of net-new interactions a week, and maintain a pipeline anywhere
  from a few dozen to more than a hundred ongoing conversations.
- Record keeping and CRM excellence are paramount.

### Assume the sale is inevitable and it might be

- Think "This is going to happen. It makes sense for you. This solution is the
  future, and it will make you more successful going forward".
- If the deal doesn't immediately close, that's ok - you can come back to it
  later (so keep good records).

### Expect to win, but be unfazed by rejection

- A 20-30% win rate is solid.
- The key to those kinds of numbers is to maintain the tempo and confidence
  required for sales success.
- Always record the reason for a win or loss so you can learn from it.

### Record everything, but efficiently

- There's no way you can retain all the information with everything going on,
  not just day to day, but month to month and quarter to quarter.
- Use CRM to keep track of everything, adopting a mindset of persistently
  pursuing and checking off key pieces of information.

### Make yourself at home in a glass house

- The creation of transparent sales data is critical for the success of the
  company, not just for go-to-market but for product development.
- Transparency also creates a culture of accountability and shared learning that
  drives a positive feedback loop, resulting in action orientation rather than
  loss aversion.

## Pricing

Our pricing strategy is fairly unique among our competitors in that a lot of our
product is free, and we charge based on storage, processing, and transfer
consumption. This simplifies our pricing for our users and also ensures we have
a highly valuable free tier.

If our product is great, then people will use it, and the price they pay scales
directly with the value they get. This is an important way we ensure that our
incentives are always aligned with our customers on providing value.

## Customer outreach

One of the most important things we're doing at this point for our company is
developing contacts with customers. This is important for several reasons:

- We need to validate our ideas with real people
- We need to test the effectiveness of our messaging (i.e., what resonates with
  people?)
- We need to develop relationships with potential future customers

Jason is the only person doing this process today. He targets outreach to ~20
people per day, using Sales Navigator to search for creators and video marketers
who would be interesting to talk to.

Metrics and templates are tracked in our
[outreach guide](https://docs.google.com/document/d/1RCj-fgRKra_C3bw-LE4fsMe3J1Y0dMFF-G_uJl0lRbM/edit)
(internal only) for now. Metrics are all manually tracked for now, but in the
future, we can look to automate this. Because of this, it is important to
remember to update the metrics whenever you have a customer conversation.
Additionally, new contacts should be saved to one of our LinkedIn Sales
Navigator lists.

The article
[Founder's Guide to Cold Outreach](https://www.firstbase.io/blog/the-founders-guide-to-cold-outreach)
has some good general tips for reaching out.

## Customer types

Our user personas are documented on our [direction page](../direction/), and our
[marketing page](../marketing/#marketing-audiences) also has information on how
we segment our audiences. Unlike other B2B SaaS products, we sell to two primary
and quite distinct audiences:

- Marketing teams who produce videos at companies for their consumers
- Independent content creators who make videos for their audiences

It's important to remember at any given moment which audience you're selling to
because the approaches for reaching them are quite different. That said, the
primary means of outreach to independent creators will be through marketing,
while marketing teams will have a more traditional sales-led flow.

## References

- The book [Founding Sales](https://www.foundingsales.com/) is a great resource
  for learning to sell at a small startup, especially if your background is not
  in sales.
