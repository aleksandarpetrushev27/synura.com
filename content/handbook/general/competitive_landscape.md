---
title: Competitive landscape
description:
  This page provides an overview of adjacent and competitive offerings in our
  space.
---

# {{% param "title" %}}

{{% param "description" %}}

The competitive landscape is one component that feeds into our overall planning,
but the [strategy and direction](../direction/) page is where we define how we
fit into this space. We are not trying to compete with every company here, just
understand the world of video products and our place in it.

This page is organized into primary capabilities (project management, reviews,
asset management, recording, editing, and distribution) but some tools bridge
multiple categories.

## Video project management

Part of producing a video is just managing the creative workflow around the
process. This includes steps like coming up with, reviewing, and approving the
concepts, scripts, storyboards, motion graphics/other assets, and everything
else you need as you go through all the phases of the production process.

It could even include billing and time tracking, though most of the products
today do not.

- [Milanote](https://milanote.com/) and
  [Collato](https://collato.com/collaboration) are specifically designed for
  creative workflows.
- [Monday](https://monday.com),
  [Wrike](https://www.wrike.com/teams/creative-project-management/),
  [Trello](https://trello.com/), and [Asana](https://asana.com/) are general
  project management tools that are light enough to not get in the way of
  creative flows but aren't video native. Some teams use
  [JIRA](https://www.atlassian.com/software/jira) for video planning, but this
  is mainly in the enterprise space and the user opinion of the tool is often
  that it is not very effective at this kind of work.
- [Notion](https://notion.so) is not specifically a creative app, but it is
  flexible and some people use it. [Slack](https://www.slack.com) is sometimes
  used similarly to coordinate everything in a low-touch way.
- [Google Drive](https://drive.google.com) is the least structured of all of
  these options, but some people are using folders and Google Drive, Docs, and
  Sheets to track everything.

A common complaint amongst all these options is that nothing works well with
video and integrates a production process. You can sort of make it work, some
better than others, but nothing is solving all the problems well.

### Reviews

It's common, especially for production agencies (who take requirements and
produce content for you), to need a review process where steps of the production
flow are reviewed and approved.

[Frame.io](https://frame.io/) is an innovative leader in this space that was
recently acquired by Adobe. This
[article](https://www.techtarget.com/searchcontentmanagement/news/252505638/Adobe-buys-Frameio-cloud-video-review-platform-for-1275B)
contains a strategic overview of why Adobe bought Frame.io. They are very
upmarket, with competitive advantages in C2C (cloud connectivity for expensive
cameras) and other features for professional film crews.

Alternatives to Frame.io include:

- [Streamwork](https://www.getstreamwork.com/) is video collaboration but
  designed for marketing teams.
- [Ad-lib.io](https://www.ad-lib.io/) is meant to bring brands and creative
  media teams together in a collaborative app and also includes marketing
  analytics to help determine which creative assets work best.
- [Wipster](https://www.wipster.io/) supports all kinds of documents, including
  video, and is probably the closest direct competitor
- [Vimeo Review](https://vimeo.com/features/video-collaboration)
- [Dropbox Replay](https://www.dropbox.com/replay)
- [Assemble](https://www.assemble.tv/)
- [Krock](https://krock.io/) focuses on video and animation)
- [Trackfront](https://trackfront.com/) has collaboration plus budgets, quotes,
  etc.
- [Kollaborate](https://www.kollaborate.tv/)
- [Kitsu](https://www.cg-wire.com/en/kitsu.html) has progress tracking as the
  basic idiom.
- [Filestage](https://filestage.io/)

Most of these products, including Frame.io, focus on a "we film and produce for
you" type of agency workflow, where collaboration is limited to defining
requirements and then signing off on the result, rather than both parties
working together closely for the duration of the project. In those cases, a
[production/project management tool](#productionproject-management) is typically
used instead.

Some agencies have also already built their own intake/collaboration apps, such
as [Lemonlight](https://www.lemonlight.com/how-it-works/) and
[Shootsta](https://shootsta.com/the-shootsta-platform/).

## Editing and processing

Preparing recorded videos for publishing is an important step in the process.

- [PlayPlay](https://playplay.com/) has made a simple editor that is focused on
  marketing teams.
- There are open-source video editors like
  [Olive](https://www.olivevideoeditor.org/) and
  [Kdenlive](https://kdenlive.org/en/).
- In the proprietary space, the standard professional editing app is
  [Adobe Premiere](https://www.adobe.com/products/premiere.html), but there are
  also companies like [Descript](https://www.descript.com/) doing innovative
  things. Descript uses transcription to identify moments in the video and
  allows for editing/removing "umms" by editing the transcription text.
- [DaVinci Resolve](https://www.blackmagicdesign.com/nl/products/davinciresolve/)
  is targeted toward professional film production.
- [InVideo](https://invideo.io/) and [WeVideo](https://www.wevideo.com/) aim at
  making the editing experience simpler and also include a large library of
  effects, transitions, etc.
- Projects like [FFmpeg](https://ffmpeg.org/),
  [FILM](https://film-net.github.io/), and [OpenCV](https://opencv.org/) (and
  many more) have advanced capabilities for post-processing and performing
  contextual actions, but are not easy for non-technical people to set up and
  use.

### AI-powered

- Some products focus on AI-powered editing automation.
  [Kamua](https://kamua.com/) does automated AI-powered video editing; they were
  bought by Jellysmack, and did a lot of the most painful manual editing flows.
  [Wondershare](https://www.wondershare.com/) is similar, and also provides
  effects.
- [RunwayML](https://runwayml.com) is a video editor that focuses on bringing AI
  editing to the forefront of the editing experience. Their
  [longer-term vision](https://www.youtube.com/watch?v=mYjfIc9xw90) includes
  natural language editing capabilities.
- [Fylm.ai](https://fylm.ai/) focuses on AI-powered color grading.

### Captions

1. As far as captions, Instagram & YouTube have pretty good captioning built-in,
   but it isn't perfect.
1. Some are also using [Otter.ai](https://otter.ai) as an AI-powered option.
   Another one is [Whisper](https://github.com/openai/whisper), which is an
   open-source project for English speech recognition from OpenAI.
1. [AssemblyAI Audio Intelligence](https://www.assemblyai.com/products/audio-intelligence)
   provides a suite of audio analysis features including sentiment analysis,
   entity detection, separation of content into chapters, and auto-moderation.
   It can also translate, automatically remove PII, detect the topic, emotion,
   and more.

## Artifact creation

During production, you're also producing or searching for various artifacts.
Storyboards, mood boards, scripts, stock footage, music, and so on.

### Scriptwriting

The vast majority of people we have spoken to who are using something for script
writing are using Google Docs.

### Storyboarding

You can use Figma or Miro as a general tool for structuring visual information,
but [Milanote](https://milanote.com/inspiration/filmmakers) brands itself as a
creative visual tool and specifically talks about storyboarding in its product
marketing.

### Motion graphics, stock footage, and music

For creating motion graphics, most people use Adobe After Effects. There are
many public resources for finding royalty-free or paid ones:

- [Envato Elements](https://elements.envato.com/stock-video/motion-graphics)
- [Videvo](https://www.videvo.net/free-motion-graphics/)
- [Videezy](https://www.videezy.com/free-video/motion-graphic)

There are similar sites for finding stock footage:

- [Pexels](https://www.pexels.com)
- [Shutterstock](https://www.shutterstock.com/)

And for music, there are also large libraries:

New in this space is AI generated elements, such as
[Beatoven.ai](https://www.beatoven.ai/) which can generate music per prompts.

These search engines for paid and free elements are tending to merge/index each
other's contents. For example, Shutterstock now has not just photos but videos,
music, and vectors.

## Asset management (CMS & DAM)

[Video CMS tools](https://www.g2.com/categories/video-cms) allow businesses to
organize, share, modify, and distribute videos internally and/or externally.

- [Panopto](https://www.panopto.com/features/video-cms/) is a good example of a
  Video CMS.

[Digital Asset Management](https://www.g2.com/categories/digital-asset-management)
(or DAM, sometimes also referred to as Media Asset Management or MAM) is another
category of asset management tool focused on working with all kinds of digital
media, sometimes including video. Typically, DAM tools are used by brand,
developer relations, and sales/marketing teams to ensure consistent brand
identity, and provide indexes of current assets, version control, and similar
workflows.

- [Canto](https://www.canto.com/) is an example of a Video DAM system.

Note that usage of the terms
[CMS (Content Management System) and DAM (Digital Asset Management) are not well defined](https://www.canto.com/blog/dam-vs-cms/),
and different companies may use the terms inconsistently. In general, both of
these categories contain products that are focused on building a library of
assets and making them easy to discover and use.

## Recording

All videos at some point require getting content from the camera into a file,
and there are several options for doing so:

- Adobe offers the free
  [Premiere Rush](https://www.adobe.com/products/premiere-rush.html) app, which
  also includes editing features. On the Mac, there is also
  [iMovie](https://www.apple.com/imovie/).
- [Riverside.fm](https://riverside.fm/) provides cloud-connected recording from
  their mobile app and will do the editing and transcription for you.
- On mobile, there are products like
  [ProCamera](https://www.procamera-app.com/en/procamera-video-mode/) that
  enable more powerful recording options.
- [Reincubate Camo](https://reincubate.com/camo/) allows for using your phone as
  a webcam, to increase quality. Apple released a
  [similar feature](https://9to5mac.com/2022/08/01/iphone-as-mac-webcam-continuity-camera/)
  allowing you to use your iPhone as a webcam with iOS 16 and macOS Ventura.
- [vMix](https://vmix.com) is the professional standard for producing streaming
  content outside of gaming; typically OBS (see below) will be used for gaming.
- Tools like Loom, Zoom, and Teams are used often by enterprises for recording
  meetings. As far as enterprise demos, a lot of people make videos where they
  sit at their desks and talk. Loom probably makes this look the nicest of the
  available options, but you could imagine AI-powered editing making this look a
  lot nicer and generating visual interest, removing silences, and helping show
  relevant content at the right time.

There are several companies focused only on gaming, which provide not just video
management capabilities but audience management as well (for example, merch
stores, chat plugins, etc.) Some examples of companies in this space are:

- [Open Broadcaster Software®️ (OBS)](https://obsproject.com) is the
  open-source, free software leader in this space.
- [Lightstream](https://golightstream.com/) (and their
  [API.stream](http://api.stream/) sub-product) has raised 11.4M in this space.
- [Streamlabs](https://streamlabs.com/) is owned by Logitech. They had some
  recent
  [controversy](https://dotesports.com/news/pokimane-hasan-obs-elgato-among-those-who-call-out-streamlabs-for-apparent-copy-of-lightstreams-product).
- [Twitch Studio](https://www.twitch.tv/broadcast/studio) was created as an
  easier-to-use alternative to OBS for streamers.

Gaming tends to have big differences from all other kinds of video production
because it's more about "going live" and recording the gameplay. It's much more
ad-hoc than other kinds of video production.

[Apps4rent](https://www.apps4rent.com/obs-open-broadcaster-software-streaming-hosting/)
offers hosted OBS instances in the cloud that you control locally but handle
file backups and processing on their end. It’s a very simple, no-frills offering
but one that already provides value for some users. They describe their
advantage over solutions like Lightstream as "Lightstream doesn't use much CPU
space but fails to provide as many features as OBS. If you want to enjoy the
tons of features offered by OBS studio without compromising the lightness and
portability that Lightstream offers, you can do so by subscribing to dedicated
OBS hosting plans offered by Apps4Rent."

## Hosting and distribution

The classic social media publishing targets are YouTube, Facebook, and
Instagram, but other products also specialize in hosting.
[Vimeo](https://vimeo.com) specializes in enterprise/corporate hosting, and
[Wistia](https://wistia.com) specializes in hosting marketing-related videos
with special analytics and call-to-action capabilities.

As far as streaming, YouTube offers streaming, and Twitch specializes in gaming.
There are also professional streaming platforms like
[Wowza](https://www.wowza.com/) which are used for large events; additionally,
internet CDN providers like Akamai can get involved in ensuring reliability and
throughput for very large global events.
