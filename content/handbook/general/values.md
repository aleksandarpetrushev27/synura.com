---
title: Values
description:
  A shared set of values is important for how a company conducts itself, and
  we're no different. Learn about the values that set us apart here.
---

# {{% param "title" %}}

{{% param "description" %}}

## Iteration

{{< rawhtml >}}

<div class="value-emoji uk-align-right">🐜</div>
{{< /rawhtml >}}

By keeping our “build & learn” loop tight, we can consistently deliver results
(customer value) and adapt to how our users respond to our improvements. Through
building iteratively we avoid making things more complex than they need to be.
We also create the opportunity for people to give feedback before things are
done, which is an important way we make sure everyone's diverse viewpoints are
included. Small steps forward, consistently made, is how we grow.

Our emoji animal for this value is the ant, where through the small, coordinated
efforts of many contributors, they are able to achieve outsized results.

- [GitLab Values (Iteration)](https://about.gitlab.com/handbook/values/#iteration)

## Transparency

{{< rawhtml >}} <span class="value-emoji uk-align-right">🐕</span>
{{< /rawhtml >}}

We respect and listen to each other’s and our users' viewpoints, and have “short
toes” for feedback. We truly believe that every perspective brought to a
conversation is a valuable addition. Clear written communication is the
cornerstone of how we give updates since this allows for deeper
[async](../communication/#communication-norms) thought and reflection. We are
also transparent publicly - feedback from our users on our iterations is the
best way to be sure we’re on the right track. One of the ways we live this value
is by sharing videos of us working on Twitch and
[YouTube](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg). Check our
[team page](/handbook/general/about/) for links to individual streaming
channels.

Our emoji animal for this value is the dog because they are open, humble, and
sincere.

- [GitLab Values (Transparency)](https://about.gitlab.com/handbook/values/#transparency)
- [Guidance on Feedback | GitLab](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/)

## Curiosity

{{< rawhtml >}}
<span class="value-emoji uk-align-right">🦦</span>{{< /rawhtml >}}

None of us can do it alone: we won’t be successful without everyone's (including
our users') contributions, so we must be good at observing and listening to each
other. We seek to include differing opinions from as many different kinds of
people as we can because their points of view enrich the ones we're already
familiar with. Doing so requires trust and mutual respect, assumption of
positive intent, and it means engaging with each other with sincere curiosity
and a desire to understand and help. Being connected in this way helps us have
the important, fun, and even sometimes difficult conversations that are needed
to build something that will help people.

Our emoji animal for this value is the otter, which is known for its curious,
friendly, and playful nature.

## Resilience

{{< rawhtml >}}<p class="value-emoji uk-align-right">🦡</p> {{< /rawhtml >}}

Building a startup is a journey of learning and trying things, not all of which
will be successful - it is an environment with a lot of uncertainty and change,
and is more a marathon than a sprint. We embrace this uncertainty by being “not
merely determined, but flexible as well”, and excelling at “adapting our plans
on the fly.” (see Relentlessly Resourceful link below.) We also adapt to
challenges by having a diverse team - individually, different people are strong
in different contexts and situations, but together we cover for each other and
make us all stronger.

Our emoji animal for this value is the badger since they are known for being
tough and determined, and not backing down from a challenge.

- [Relentlessly Resourceful](http://www.paulgraham.com/relres.html)
