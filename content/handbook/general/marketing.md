---
title: Marketing & brand
description:
  This page includes information about our brand and marketing guidelines,
  logos, other brand assets, and resources for teams working on campaigns.
---

# {{% param "title" %}}

{{% param "description" %}}

## Name

The name of our company is 'Synura' with the S capitalized.

## Design

Synapse, our
[application design system](https://www.figma.com/file/WOYoQTFGlyO0Q3f0H3CMoc/Synapse),
is public and contains fonts, elements, colors, icons, and much more.

### Logo

Our logos in various formats (icon, stacked, side-by-side) can be found in
[the brand folder of our site repo](https://gitlab.com/synura/synura.com/-/tree/main/static/images/brand/).
Various
[video-related icons](https://gitlab.com/synura/synura.com/-/tree/main/static/images/hero)
can be found in our website repo (they are currently used for the hero
animation).

The name of the company, when displayed next to the logo, is as it appears at
the top of this site. The font is Poppins, the weight is 500, and the letter
spacing is 0.28rem.

### Company letterhead

Our company Letterhead is TBD and will be on our Google Drive once we work with
a designer to create it.

## Marketing audiences

Our core user personas are documented on our [direction page](../direction/).
For each, we will provide a description, the style of marketing they prefer,
some examples of content that appeals to the audience, what other vendors in the
space are doing, and our goal for the audience (for example, qualified leads).

There is also a [competitive landscape page](../competitive_landscape/) with
information on the market and competitors.

## Kinds of marketing

### Outbound

- Account-Based Marketing
- [Social Media](#accounts)
- Targeted ads
- Note that to manage LinkedIn company page, your personal LI account must be
  added as an admin. There's no separate login via 1Password

### Inbound

- Website
- Blog Posts
- Overview Deck
- Traditional Earned Media (eg news articles)

## Accounts

Synura has the following social media accounts:

- [Twitter](https://twitter.com/SynuraHQ)
- [YouTube](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg)
- [LinkedIn](https://www.linkedin.com/company/synura)

They are not official channels, but some employees also have streaming channels
where they work on Synura. Links to these can be found on our
[team page](/handbook/general/about/).

### Twitter

#### Purpose of building a Twitter presence

1. Build a network of people interested in enterprise video.
2. Increase visibility and credibility in the tech scene.
3. Leverage already existing connections and affiliations (by following and
   getting followed by industry leaders).

#### General Twitter guidelines

1. Tweet from our account
2. Keep the tweet concise.
3. Limit hashtags to 1-2 per Tweet.
4. Include a clear call to action where applicable (i.e.” Read the full story
   here”).
5. Have a conversational, informative, and optimistic tone. Avoid snarky or
   cynical tones, and don't cut down others. That's not who we are.
6. Monitor events and trending conversations to tweet appropriately.
7. Use images, GIFs, and/or videos whenever possible.
8. Use emojis to add emotion when it applies.
9. Ask questions and run polls (see poll ideas here
   [https://business.twitter.com/en/blog/engaging-twitter-poll-ideas.html](https://business.twitter.com/en/blog/engaging-twitter-poll-ideas.html)).
10. Do not express political views.

#### Tweet approval process

For now, there is no special tweet approval process but if you want someone to
review it just grab another team member.

#### At which frequency should we tweet?

1 to 2 times a week, in addition to our bi-weekly progress updates.

#### Who should we follow?

1. C-level and executive from successful and interesting startups, potential
   partners, or publishing platforms (i.e., Twitch, YouTube)
2. Companies and people that produce interesting, innovative video and streaming
   content.

#### What should we tweet?

- Product updates and app features.
- Media content from events taking place at (i.e. NAB, etc).
- Original blog posts (note: format links with Bit.ly).
- Blog posts and news articles written by other platforms about making great
  video content.
