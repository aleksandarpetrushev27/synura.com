---
title: Official company tools
description:
  Here’s a list of tools that the company licenses for shared productivity or
  for which we have company accounts.
---

# {{% param "title" %}}

{{% param "description" %}}

Communication is a key linker across all these tools, so also check out our
[communication page](../communication/).

## Official tools

All of these tools require a user account, so if you need it for your job you
can ask in #help.

- [1Password](https://1password.com/): Store shared passwords and account
  information
- [Calendly](https://www.calendly.com): Calendar scheduling
- [Earth Class Mail](https://www.earthclassmail.com/): Manage postal mail online
- [Figma](https://www.figma.com/@synura): Designs and brainstorms
- [GitLab](https://www.gitlab.com): Source control and website hosting
- [Google Analytics](https://analytics.google.com/): Website instrumentation
- [Google Cloud Platform (GCP)](https://console.cloud.google.com): Cloud
  platform
- [Google Workspaces](https://workspace.google.com): Email and office
  applications
- [LinkedIn](https://linkedin.com/): Social media
- [Mailchimp](https://mailchimp.com): Public information signup and investor
  updates
- [Mention.com](https://mention.com/en/): Brand monitoring (see #mentions
  channel in Slack)
- [Postman](https://postman.com/): API documentation, automation, design and
  testing
- [PTO by Roots](https://www.tryroots.io/pto) (Slack integration): PTO tracking
- [Sendgrid](https://sendgrid.com): Mail delivery in app
- [Slack](https://www.slack.com): Team communication & chats
- [Twitter](https://twitter.com/): Social media
- [Userbrain](https://userbrain.com): User research and testing
- [YouTube](https://www.youtube.com/): Uploading company videos

## Free tools

We don't have a company account or subscription for these, but they can still be
helpful.

- [Pexels](https://www.pexels.com/) (free): Free stock video footage
- [Pave](https://www.pave.com/): Salary benchmarking for offers
- [Twitch](https://twitch.com): Live streaming
