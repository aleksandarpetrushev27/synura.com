---
title: Direction
description:
  Our company direction (mission, use cases, and next steps) is something we'll
  iterate on constantly, but always keep a north star.
---

# {{% param "title" %}}

{{% param "description" %}}

You may wonder why our direction is public. In line with our
[transparency value](../values/#transparency), being open and receiving constant
feedback is the best way for us to ensure we’re on the right track. If you see
something here that you love, or something where you think we're missing the
mark please [let us know](mailto:contact@synura.com).

You can see a recent walkthrough of this page here:

{{< rawhtml >}}

<!-- prettier-ignore -->
  <lite-youtube
    class="uk-align-center"
    videoid="hjvG-_FqdsU"
    style="background-image: url('/mockups/mobile_static_video_mockup.jpg');"

>

    <a
      href="https://youtube.com/watch?v=hjvG-_FqdsU"
      class="lty-playbtn"
      title="Play Video"
    >
      <span class="lyt-visually-hidden">Company direction overview</span>
    </a>

  </lite-youtube>

<!-- prettier-ignore -->
{{< /rawhtml >}}

## Mission

The potential impact of video and streaming for brands and creators is huge, but
there is still so much to be done to make it easier to create great content. Our
mission is to make it so anyone, regardless of training, has the resources,
guidance, and support they need to produce amazing content. We want to do for
video what apps like Instagram did in making it easy for anyone to make
great-looking images: no special technical expertise is needed.

## Use cases

Having a mission is important, but perhaps even more important is knowing
exactly how you deliver real value to the people who use your product. We use
use-cases to try to organize the themes, and
[value prop stories](https://medium.com/bloated-mvp/how-to-sanity-check-your-startup-idea-dbb3ad4c9888)
to illustrate specific examples.

### Relative prioritization

We have two use cases,
[coordinating the production process](#coordinate-the-production-process-without-needing-to-be-an-expert)
and
[taking advantage of cloud processing](#take-advantage-of-cloud-storage-and-processing).
Of the two, cloud processing is a higher priority because it has more standalone
value. People can use that capability with any number of other tools they might
already be using, and this is also the only part of the product that we charge
for, and it's a very visible time saver no matter how your process works today.

That being said, providing a simple solution that lets you coordinate your video
production process is also important for people to discover our product, and
also just to help them get started with video. Where we can provide nice,
easy-to-use features that help people structure their work, and also leads them
to the services we charge for, all the better. Along these lines would be other
free adjacent services like a video branding creator or AI-powered motion
graphics generator.

One special case of the latter is supporting
[advisory consultancies](#advisory), who are typically small to medium firms
with under a hundred clients they provide custom guidance to. Where we can
provide features that help them interact and work with their clients, it's a
win-win; they help bring customers onto the platform, and we help them manage
their clients effectively.

A good filter to help make a trade-off decision between two features is to
consider which will lead to better growth.

### Coordinate the production process without needing to be an expert

**What is the problem? Who has it?**

<!-- Identify the business pain you're seeking to solve so your audience can evaluate whether it is relevant.
You're in good shape if you say "Have you encountered this?" and they not only say yes but get into an exciting conversation about it.
A good rule of thumb for who has the problem is who has the budgetary responsibility to spend to fix it. -->

Producing a video or live stream, whether for a company or as an independent
content creator, is a complex task that requires a lot of specific knowledge.
From script writing to storyboarding, reviews, captions, post-processing,
publishing, and everything in between - it's a lot.

At the same time, it's becoming more and more common for the person making the
video to not have any videography experience, just a passion for the topic.
Content creators are often inspired to teach or communicate something they care
about. Marketing teams often feel like producing videos is important, and
someone is given the task to figure it out. They may have cobbled together a
process that sort of works, but they feel like there must be something simpler
and more effective.

Finally, even where there are tools today, none of them are video-native. You
define your workflow rather than having something included that was designed for
video, and because there is no built-in video preview or understanding of
scripts, storyboards, and so on, there's a lot of clicking around and jumping
between different apps (which makes them especially ineffective at using them
when on the go).

**What is the cost of the issue?**

<!-- You may be looking at what it costs to solve a problem, or what it will cost not to solve it.
Aim for hard ROI when you can (specific numbers) rather than "this will generally make things better". -->

Not having a clear, well-understood production process results in a lot of back
and forth. Producing artifacts out of order (such as making a first draft video
before you review your script) can cause a lot of re-work, which is time that
you'll never get back. Not having clear timelines and expectations also ensures
that a lot of time is wasted manually following up with people for reviews and
approvals. A 75th percentile
[digital marketing manager costs on average $135,497/year](https://www.salary.com/research/salary/benchmark/digital-marketing-manager-salary)
in the United States. Do you want to tie a person like that up with managing a
crude, ad-hoc process.. even if it's just half their time?

Going with an agency or hiring a dedicated video production team is an even more
expensive solution. It may seem like a good idea to hire an agency to produce
your content, so you don't have to worry about it and can just direct your
goals, but it's not going to be cheap. And anyway, wouldn't you rather have the
people who are passionate about being the ones in charge of producing it?

A better, more affordable option is hiring an expert to help guide you and give
you advice about creating the best video process, but coordinating that work can
be complicated.

**How is this currently solved? Why don't current solutions work?**

<!-- The thrust of sales conversations will be to convince users that their existing solutions are insufficient.
In cases where no solution is in place, you need to explain why non-solution is costly.
In cases where they have a process, you need to demonstrate why the process is inferior (time cost, fragility)
In cases where they have a service provider, the cost tends to be very high.
In cases where they are using a product, you need to know why your product is better. -->

If they aren't spending a lot of money on agencies to produce their content,
people are using non-integrated suites like Google Drive for this. They might
have scripts in Google Docs, video files in a Google Drive folder, and a list of
ideas in a Google Spreadsheet. Communication is done over email or Slack, or a
combination of both. Because these tools are not integrated well, lots of
details, dependencies, and deadlines fall through the cracks. In some cases,
there's a little bit of overall project management happening in a tool like
Asana, but this isn't especially common, and you will run into issues where they
don't work well with video files.

Alternatively, some products help with reviews or asset management that sort of
help with video, but they either assume you have a lot of professional
knowledge, don't model the end-to-end process, or both.

**What has changed?**

<!-- Typically in product innovation, and associated selling, something has changed that enables a new solution.
This can come in the form of new technology making something possible, but also as a change that demands a new solution. -->

The world has become video-first over the last five years. Accelerated by the
pandemic, consumption of on-demand and streaming video has become a primary way
that people engage with content. Creators know that video is important to reach
their audiences, but don't have any insight into how it's done, day to day. They
try and get stuck, and maybe even get frustrated and quit, which is a shame.

**How does our solution work?**

<!-- If something changed that enables a new means of solving a problem, you need to explain how your solution does it.
A good way to do that can be to compare your product to existing solutions that your customer understands. -->

Imagine if all the documents in your ad-hoc solution knew about each other's
status, and integrated into projects, and you'll have a good sense of how Synura
works. Projects are organized into coherent units, with all assets bundled
together. Everyone involved in a project receives relevant notifications when
they have an action or update that's important for them. The state of the
project is front and center, and anyone can see what's going on at any time.

**How do we know it is better?**

<!-- Take a look at how you answered "What are the costs associated with this problem"
Now it's time to present why your solution does a better job, as measured in the language of existing solutions.
Third-party validation (customer counts, testimonials, case studies, press, etc.) is great to provide here, if available. -->

We provide a better way to plan, produce, review, and publish videos, with lots
of guidance built-in that's meant for regular people. Smart defaults, helpful
templates, and learning resources (such as blog posts or videos) that help teach
important concepts are all built-in. The entire process is on rails, with each
step of the production workflow visible. Everyone understands what's needed,
rework and other inefficiencies are avoided, and collaboration is facilitated
naturally as a result.

Even better, you don't need to send your ideas to a production agency to execute
on your ideas. Because we make the production process easy, everyone can
participate - that means that the most passionate voices can be intimately
involved in content creation. This means your content will be even more
impactful for your audience.

### Take advantage of cloud storage and processing

**What is the problem? Who has it?**

<!-- Identify the business pain you're seeking to solve so your audience can evaluate whether it is relevant.
You're in good shape if you say "Have you encountered this?" and they not only say yes but it proceeds to an exciting conversation about it.
A good rule of thumb for who has the problem is who has the budgetary responsibility to spend to fix it. -->

Processing, storing, and sharing videos after you've recorded them can also be a
technical challenge. If you store them locally, they can be lost forever if
something happens to your computer. If you store them in a cloud file archive
that isn't video-aware, it can be hard to find and preview your files. A
non-video-aware solution also won't be able to automatically process and edit
your videos for you, leaving it up to you to figure out.

**What is the cost of the issue?**

<!-- You may be looking at what it costs to solve a problem, or what it will cost not to solve it.
Aim for hard ROI when you can (specific numbers) rather than "this will generally make things better". -->

Not doing anything will leave your videos looking and sounding unprofessional,
with sagging engagement numbers. The effort you're spending will not match up
with the results you're seeing, and competitors who invest in video production
will look more professional.

A 75th percentile video editor
[will cost on average $64,282 in the United States](https://www.salary.com/research/salary/benchmark/video-editor-salary)
if you were to hire one to organize and process your video files. Once you start
producing a decent amount of video content, you'll either need to keep hiring
more or move to an automated process that scales better. You could hire an
engineering team to do that, but that would be even more expensive.

Videos on different platforms (TikTok, Twitter, YouTube) each come with their
requirements, norms, and best practices. Customizing videos for each one is also
time-consuming, and error-prone - and mistakes will cost you in engagement
numbers.

**How is this currently solved? Why don't current solutions work?**

<!-- The thrust of sales conversations will be to convince users that their existing solutions are insufficient.
In cases where no solution is in place, you need to explain why non-solution is costly.
In cases where they have a process, you need to demonstrate why the process is inferior (time cost, fragility)
In cases where they have a service provider, the cost tends to be very high.
In cases where they are using a product, you need to know why your product is better. -->

A lot of creators are doing their best and publishing at the level of their
capability. They may spend some time learning and trying to improve their
technical post-production skills, but this takes away from time spent planning
great content. Either that, or they are scaling editors, hiring post-production
agencies, or building their video pipelines using engineers. None of these
options are cost-effective in the end.

**What has changed?**

<!-- Typically in product innovation, and associated selling, something has changed that enables a new solution.
This can come in the form of new technology making something possible, but also as a change that demands a new solution. -->

Video processing in the cloud has brought forth several new possibilities to
automatically handle basic post-production tasks:

- Seamlessly remove "umms" from your speech
- Improve light and color balance to make your videos pop
- Remove background noise
- Auto-generate compelling thumbnails
- Apply standardized intros, music, and outros
- Reframing and transcoding for different social networks, device orientations,
  and aspect ratios
- Automatic transcriptions/captioning
- Auto-trimming

These are examples; many more possibilities are out there. The key thing is that
no special expertise is required to take advantage of them. In the future, it
will be possible to run more advanced edits by providing natural language
instruction. For example, "cut to the office footage here" or "add on
screen-text whenever a participant is introduced with their name".

**How does our solution work?**

<!-- If something changed that enables a new means of solving a problem, you need to explain how your solution does it.
A good way to do that can be to compare your product to existing solutions that your customer understands. -->

Once you upload a video to Synura, we can automatically run all kinds of
post-processing on it for you. Imagine having every video you upload
automatically get the above suite of improvements on it, without you having to
do anything.

**How do we know it is better?**

<!-- Take a look at how you answered "What are the costs associated with this problem"
Now it's time to present why your solution does a better job, as measured in the language of existing solutions.
Third-party validation (customer counts, testimonials, case studies, press, etc.) is great to provide here, if available. -->

We're confident that if you process your videos using our automated systems,
they will look and sound better. You won't need to hire your team of editors,
you won't need to build out a video pipeline using an expensive engineering
team.

## Guiding principles

### Get single-player right first

There's plenty of amazing content out there that's being worked on by a single
person. By focusing on them first and ensuring we're adding value to their
production process, we will naturally reach a moment where we have excited users
who want to be able to invite others to collaborate.

Evolving the product in this way will help ensure we stay on track, don't try to
do too much too early, and we'll build something valuable and relevant for
people making videos today. It also helps us avoid making too many
collaboration-only features before we know what our users need, and thereby
avoid accruing too much complexity too early.

All that said, it isn't a rule that we can't make collaboration features early.
Just that they should be strongly validated, rather than prospective features
that customers _might_ be interested in.

### Bring "developer tools"-style innovation to video production

Over the last ten years, innovation in developer tools has accelerated at a
rapid pace. Video production has a lot of analogs with how software is planned,
produced, reviewed (tested), and published, and by building features that bring
these two worlds together, in terms of workflows and experience, we can provide
something unique and valuable.

This is not to say that there aren't major differences between developer tools
and video production, and we must account for them, but they can be as much a
source of inspiration and creativity as they can be a challenge.

## Customer profiles

At the moment our customer profiles are focused but contain a lot of different
kinds of people making videos for a lot of different reasons. It will help us to
continue to further refine these into niches we can focus on at launch where we
can solve the use cases perfectly.

In general, in terms of finding first customers with budgets and problems we can
solve, [companies and brands](#companies-and-brands) and
[advisory agencies](#advisory) are the most likely initial targets. Likely
relatively smaller, growing companies that have not already put a complicated
process in place that they'd have to change from - even if what we offer is
better, change is harder than providing a new solution.

### Companies and brands

Some companies are already working extensively with video: hosting webinars,
publishing to YouTube channels (typically demo-style content), and creating
advertisements and other pre-recorded sales and training materials. They
understand the challenges and value of video and would benefit from better
collaboration.

These kinds of users tend to have the most ad-hoc processes for creating videos
and can benefit a lot from a collaboration tool. They would benefit from a
little training and structure not just to plan and execute their videos and
streams, but to help them wrangle their stakeholders. Because these kinds of
people tend to have a budget, and we can also help them quickly, they are great
first users for us.

### Agencies

There are a few broad categories of agencies/consultancies that we could provide
value for, but the [advisory](#advisory) one is the most obvious and immediate.
A couple of interesting examples of agency collaboration apps are the
[Lemonlight](https://www.lemonlight.com/how-it-works/) and
[Shootsta](https://shootsta.com/the-shootsta-platform/) platforms, which handle
intake and ongoing client collaboration for them. Something like this could be
valuable for either advisory or production-for-hire shops, but these kinds of
shops would benefit less from our automatic post-processing.

#### Advisory

There are a lot of businesses that provide advisory services to content
creators. They have a variety of titles, but it includes examples like video
marketing (or YouTube/TikTok/etc.) coach, advisor, expert, specialist, guru,
strategist, consultant, and so on. They will typically have a stable of clients
they work with whom they may charge on a per-hour basis, but a
subscription-based is becoming more prevalent. Their customers are typically not
experts in video production, so they provide both strategic and technical advice
to their clients.

This is a particularly interesting niche for partnerships and providing
solutions. We both target the same kinds of users, but with different
priorities. Typically these advisors will provide strategic advice, helping them
come up with integrated video strategies and find a brand identity for their
videos. We provide some similar advice, but at scale rather than tailored to a
specific person or company.

You could imagine a workflow for these kinds of advisors where they have all of
their customers in Synura, with access to all of their accounts, and they can
set up resources/templates, and leave comments and advice on videos in progress.
This would be a much more effective and efficient workflow for them, helping
them scale their businesses.

There may also be opportunities here for special pricing, an affiliate or
referral program that pays the agency, or other kinds of group-based incentive
pricing and partnerships.

#### Production for hire

Brand and advertising agencies that produce video content for their clients, who
are typically companies that don't want to grow video expertise in-house. Their
workflow is similar to if it was the company itself doing it but has the
additional requirement of client reviews (both in pre and post-production). This
workflow, with a professional production team and client reviews, is the one
least aligned with our model and most towards products like Frame.io.

#### Post-production/marketing focused

Creator/influencer-focused agencies (i.e.,
[Jellysmack](https://jellysmack.com/),
[Underscore Talent](https://www.underscoretalent.com/), and
[ScreenPlay](http://www.screen-play.org/)) are a subset of these, and provide
services for creators such as remarketing (and reworking content) for different
social networks, operating channels, optimizing strategy, and more - even
including things like financial management, booking, and so on. These agencies
have more collaboration in pre-production, while post-production (modification
and distribution of content to other networks) is primarily a hands-off
factory-type model (at least from the point of view of the creators.) They tend
to work with only the most viral content.

### Independent creators

In situations where an independent creator produces content (good examples are
educational channels that plan and produce a video monthly), they would likely
benefit from the very same features as the brand and company users.

They are sort of a middle ground between the agency and company use cases, in
that they have some unique social media requirements and have more professional
production processes, but they also tend to run on smaller budgets than either
agencies or companies. We should welcome this kind of user by having a pricing
tier that supports them.

## Where we are now

We have not yet launched the product publicly and are working with early
adopters to determine their most important problems to solve and what their
priorities are. This includes talking to support, sales, developer relations,
and marketing teams, as well as independent creators. If you are interested in
sharing your feedback, please sign up for our waiting list on our [home page](/)
and we will reach out.

Our most important immediate task is to
[build an organization that learns and iterates](../product_development/), and
then
[launch our product](https://gitlab.com/groups/synura/-/milestones/1#tab-issues)
so we can start getting real feedback.

### Roadmap

We want our [first iterations](../product_development/#minimum-viability) to
immediately add value, prove the product concept, and serve as a foundation from
which we can build more advanced features. As with everything on this page this
concept is likely to evolve, but for now, it is likely that we will build an MVP
user flow as follows:

1. Come to the Synura website and register an account.
2. Create a project for your videos
3. Upload your video files, storyboards, and other assets.
4. Run pre-configured video processing on your videos.

This workflow is represented by the
[1.0 milestone](https://gitlab.com/groups/synura/-/milestones/1#tab-issues).

The second iteration will likely allow for inviting others and having comment
threads on saved videos, bringing the initial collaboration workflows into the
product.

You can see a full list of
[upcoming milestones](https://gitlab.com/groups/synura/-/milestones) in our
GitLab group. Please upvote and comment on
[issues](https://gitlab.com/groups/synura/-/issues) and
[epics](https://gitlab.com/groups/synura/-/epics?state=opened&page=1&sort=start_date_desc)
you'd like to see and we'll take that into account in our planning.

### What we aren't doing

- There are other products out there that aim more at the professional video
  production market, with clients like Vice, Netflix, and others. We are not
  focusing on this use case for now because we believe there is an underserved
  segment in the enterprise and prosumer segments that we can better and
  uniquely address, and technology like C2C (connectivity on expensive
  filmmaking equipment) is not as relevant for our segment. By going after
  "regular Joes and Janes" we can carve out a niche for ourselves, and do it in
  a rewarding space where we can make a real difference.

## Competitive landscape

An extensive competitive analysis can be found on our
[competitive landscape page](../competitive_landscape/).
