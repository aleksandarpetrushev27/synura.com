---
title: Product development
description:
  We are a product-led company, and how we design and build our product and
  handbook is critical to our success. This page outlines the principles and
  practices that guide us.
---

# {{% param "title" %}}

{{% param "description" %}}

See also our [marketing & brand](../marketing/) guidance for details like logos,
colors, feelings, and other elements associated with our brand that play into
product development.

## Source availability

We are big believers in open source and open core software, and that's why we
[support projects like OBS with donations](https://opencollective.com/synura).

Synura itself is distributed under our own closed-source, proprietary (but
source available) license. What this means is we operate a lot like an open
source project, even for our proprietary code:

- Our source code is available for anyone to see, and we welcome merge requests
  from anyone.
- Our issue tracker is public and anyone can participate.
- Unlike open-source software, we do not grant permission to use the Synura
  software in production without a license that matches the features and seats
  you are using.

## Product workflows

Our workflows within the product (creating an account, logging in, creating
projects, browsing them, and so on) can be found in our
[initial mapping](https://www.figma.com/file/avKfzfTTbU5jZ41vBY16Ga/Initial-Mapping?node-id=0%3A1)
Figma document. This is an important resource for onboarding new team members as
well as ensuring we're on the same page when we talk about product concepts, so
we try to keep it up to date.

## Release and roadmap management

We release on a monthly cadence, with the release date being the last non-Friday
working day of the month and the start of the milestone being on the first day
of the same month. The release year starts in August with an `x.0` release, and
one increment is added per month (i.e., `x.1`, `x.2`) until `x.11` is reached,
at which point we've reached August again and we bump the first number (i.e.,
`1.11` to `2.0`).

### Roadmap

Our list of
[upcoming milestones can be found in GitLab](https://gitlab.com/groups/synura/-/milestones).
We maintain three specific release milestones, covering the next three months of
what we'll be building. However, beyond this rolling three-month period we use
[nearsighted roadmap](https://medium.com/geekculture/4-excellent-examples-of-agile-nearsighted-roadmaps-ffec9cf6de5e)
buckets:

- [Next 3-6 months](https://gitlab.com/groups/synura/-/milestones/4#tab-issues)
- [Next 6-12 months](https://gitlab.com/groups/synura/-/milestones/5#tab-issues)

Issues and epics in the upcoming three releases will be quite detailed and
broken down, while the further out you get into the 3-6 or 6-12 month buckets
will be more prospective. Over time, issues and epics will be refined and move
up from the 6-12 month bucket, to the 3-6 month bucket, and then into specific
releases.

In any case, our roadmap is subject to change at any time as we listen to our
customers. The best way to advocate for something to be prioritized is to
participate in the issue or epic, including upvoting with a thumbs up.

### Feature and marketing launches are separate

Note that we separate our feature launch from our marketing launch, and the
activity that happens at the end of each month is the marketing activity. It's
the point where we'll summarize and communicate about all the new improvements
we've deployed, but changes are deployed to production continuously as they are
finished. This means you may see features in an upcoming release live before the
official release date.

## Tracking work in progress (Kanban)

We use the [current milestone](https://gitlab.com/groups/synura/-/milestones) to
track in-progress work. The columns are automatically applied to all issues that
have the milestone label, based on the assignment and status:

- Completed column: all closed issues
- Ongoing column: all open and assigned issues
- Unstarted column: all open and unassigned issues

To start working on an issue, assign it to yourself (and optionally everyone who
will be working on it.) This will move it to the ongoing issues column.

Each morning we take a look at this board to see what's in progress. To remain
focused on completing things, we limit the number of issues in the "ongoing
issues" column to 5. Before starting something else, please pitch in to help
finish something else if at all possible.

The milestone page also has burndown (and burnup) charts which can be used to
assess progress in the milestone in general.

## Iteration

In line with our [iteration value, we iteratively build features to reduce the
risk of going down a path where our customers don't get meaningful value from
what we build.

This [iteration office hours](https://www.youtube.com/watch?v=zwoFDSb__yM)
recording from GitLab highlights some of the ways you can think about decreasing
iteration size.

### Minimum viability

What it means for features, products, and changes to be minimally viable is
important to understand. It's important to think of it not as delivering a broad
base of capabilities at a minimal level, but rather as building up a single use
case in a meaningful, differentiated way; nobody is inspired to adopt a creative
tool that does the basics and nothing more. It should be inspiring; it should
delight our users. It should include elements of
[emotional design](https://en.wikipedia.org/wiki/Emotional_Design).

![MVP Pyramid](/images/handbook/mvp_pyramid.png)

Similarly, each iteration must be valuable. First, build a skateboard, then a
scooter, then a bike, and so on. Make sure that the users are getting value at
each step, otherwise, we aren't learning anything until, to use the example from
the image, the car is already complete.

![MVP Steps](/images/handbook/mvp_usefulness.png)

### Minimize merge request size

Just as with product design and requirements, iteration is important for how we
code as well. The GitLab handbook contains a fantastic guide on
[how to keep code iterations small](https://about.gitlab.com/handbook/engineering/workflow/iteration/#overview),
including when to vertically or horizontally slice. See also our
[general guidance on merge requests](../communication/#everything-starts-with-a-merge-request-mr)

### Commit early, commit often

Getting continuous feedback is especially important when
[working async](../../general/communication/#communication-norms). Because of
this, apart from keeping every merge request small and focused, we try to commit
frequently to branches so that progress can be seen by anyone and they can give
feedback. It's easy to leave a conversation thinking something is clear, and if
you only share the branch when it's done, you might not realize there was a
misunderstanding until a lot of work has gone into it.

Instead, commit early and commit often and we'll stay on the same page with each
other better.

### Iteration reviews

Another way we stay on the same page with each other is by setting and reviewing
iteration goals on Monday every two weeks. We set 2-3 goals each week on the
most important things to focus on (if we are having difficulty understanding or
selecting a metric to track, check out the video lecture
[How to Set KPIs and Goals](https://www.ycombinator.com/library/6j-how-to-set-kpis-and-goals-sus-2019)).
These goals are likely to be primary user statistics, aiming for
[aggressive growth targets](http://paulgraham.com/growth.html) like
[10% week over week](https://about.gitlab.com/blog/2020/05/05/wow-rule/) (20%
over a two-week iteration), or other customer development goals (for example,
how many users we talked to in the last week). Goals may also be drawn from our
[monthly release milestones](https://gitlab.com/groups/synura/-/milestones).
Iterations and results are tracked publicly in our
[iteration review log](https://docs.google.com/document/d/1YSVJms-Jy3rlEC8_MN0EkSSzzDVW1r1xRSni1-LTu8Q/edit#).

As long as we haven't yet publicly launched, the number of weeks left to launch
is always included as a goal.

At the end of the iteration, we publish a
[post-review update](#post-review-updates), and a day or two after this
(depending on when the investor call lines up), we discuss the results there
also.

We do not have daily standups, planning, reviews, retrospectives, backlog
refinement, or other meetings to enable this process. The goal is something
simple that triggers interesting and important conversations and keeps everyone
focused.

### Post-review updates

At the end of each iteration review, the CEO posts an update to a variety of
social media channels.

Here is the process:

1. The CEO takes the output of the iteration review and prepares a blog post
   progress update covering the highlights. Apart from goals, status, and any
   [necessary graphs](https://docs.google.com/spreadsheets/d/1iJTeBPDSuQ-23QIZcK9rZn1i_uWRzXeZz5RmVTzRfv0/edit#gid=0)
   (internal only SVG template). It can include things like:
   - What we learned from talking to our users
   - What most improved our primary metric
   - The biggest obstacle we faced
2. The team reviews the progress report blog post, adding any additional
   important information/updates. In the future, this will also be the time we
   update a central changelog with new features that were delivered in the
   iteration.
3. CEO records a video presenting the same progress update and adds it to the
   blog post.
4. CEO adds the blog post link with video to the "Progress" playlist on our
   YouTube channel.
5. CEO posts the video and 1-2 highlights to Twitter from our account.
6. CEO retweets from his account and schedules the same post on our other media
   channels, including LinkedIn.
7. If this was the last iteration of a given month, then we also send an
   [investor update](../../corporate/investors/#monthly-investor-update).

### Avoid rubber stamp merge request reviews

This [article](https://alisterbscott.com/2018/07/11/avoiding-lgtm-pr-cultures)
talks about why it's important not to have a habit of rubber stamping pull
requests.

1. Make pull requests as small as possible to avoid them being too big for the
   reviewer to understand, as noted above.
2. Review your pull request by coming back at it a little later, with a fresh
   mind, and leaving questions/comments on anything you're unsure about.
3. Include clear instructions for reviewing and testing your pull request.
4. Test your peer review process now and then to make sure it's working.

## User research and testing

We use [Userbrain](https://userbrain.com) for user research (problem space
exploration) and user testing (validation of functionality/prototypes).
Userbrain can help you find people to talk to (they have a panel of users you
can recruit from) and can facilitate tests with our users/contacts. The
[Userbrain blog](https://www.userbrain.com/blog) also has various helpful
resources on how to create good tests.

As a company we take talking to customers as part of ideation and building
seriously, so don't hesitate to run tests to validate your assumptions. We are
[exceptionally transparent](../../general/values/#transparency) as a company,
and the best way to take advantage of that is by having as many open
conversations as we can to refine our ideas.

Results of your user testing efforts should be uploaded to the
[user interviews folder in Google Drive](https://drive.google.com/drive/folders/1MUKVS9QuUOOSNkpJrKl1A2VM7aE-sNjf)
(internal only to protect the privacy of interviewees). Create a dated subfolder
for each group of interviews you do, and include a summary Google Doc or
Spreadsheet in the folder to lay out your findings. When your summary is
complete you should share a link to it in the #marketintel channel so everyone
can check it out.

If more credits are needed for testing, contact Jason to add them.

## Documenting technology decisions

We use [Any Decision Records (ADRs)](https://adr.github.io/madr/) to make and
document technical decisions. These are just markdown files with a pre-specified
format.

If you need diagrams for your decisions, you can use our company Figma account,
or a free Miro account
([for UML, for example](https://miro.com/uml-diagram-tool/)).

ADRs should be created as a merge request to our
[Console repository](https://gitlab.com/synura/console), and the discussion can
be had in the MR prior to merging. The files should go in the
[`docs/decisions` folder](https://gitlab.com/synura/console/-/tree/main/docs/decisions).

## Operations

Documentation is in Google drive on
[how to find/manage our staging and production environments](https://docs.google.com/document/d/1eObsPkkiNN0f1r9TMoHIKZl92W4HHQJKBZm9K3gD4ZU/edit#)
(internal only).
