---
title: Recruiting
description:
  We aim to build a world-class team. This page goes through processes for
  internal teams and candidates to help support that goal.
---

# {{% param "title" %}}

{{% param "description" %}}

## General factors we're looking for

At a high level, we value teammates with intelligence, integrity, and a strong
work ethic. These traits should be looked for during the interview process. In
addition, there are specific factors relevant to all candidates applying to
Synura:

1. **Alignment** - clear what the candidate is looking for and that it aligns
   with what we need.
2. **Domain experience** - SaaS software, new product launches, startup
   companies. Note: focus on whichever is stronger between the resume and
   LinkedIn Profile, recognizing that different people put different emphases on
   different mediums.
3. **Tech expertise** - web technologies, agile stack.
4. **Leadership** - have a player/coach mentality. Want to contribute by doing
   and by helping others.
5. **Communication** - concise, clear, and able to cite specifics. They write
   and communicate well, and avoid glaring errors. Note that we make affordances
   for challenges commonly faced by non-native speakers. For example, issues
   that spell check can catch should be a red flag, but more subtle grammatical
   or phrasing that may seem awkward to a native speaker should not.

## Hiring process steps

We have a series of steps we take everyone through in the hiring process.
Proceeding to the next step is contingent upon success in the previous one.

The first step is always to apply, we have job listings (when available) on our
[about page](/about/#careers).

### 0. Application

The first step in any recruitment process is to apply. When we review
applications we look at several factors, beyond just the specific day to day job
requirements. While none of the below are strict requirements (any individual
person on the team today surely misses one or more of these), the following tend
to be good signals we watch out for:

1. Experience at companies that have been successful in their markets.
2. A pattern of growing and being assigned more responsibility throughout your
   career.
3. Education that's applicable to the role or to video in general.
4. Having been a video content creator, or at least consumer.
5. Participation in continued education, meetups, or other communities related
   to your area of expertise.
6. A general curiosity and openness to trying new things and facing new
   challenges

It's fine if you miss some or even all of these, but where you do match please
be sure to highlight that in your application and/or CV.

#### Designer portfolio evaluations

When reviewing a portfolio for a designer, we look at them through two primary
lenses:

1. Demonstration of your thinking process
   - One of the first things to look at is if the designer describes the
     thinking process of how they got to the solution. Typically these will be
     in the form of case studies.
   - This tells you if the designer only focused on visual elements, or really
     spends time understanding the problem.
   - The more coherent and detailed, while remaining to the point, the better.
     This also evaluates their written communication ability.
2. Demonstration of your visual skills
   - It’s also important to look for good visual skills because, in the end,
     this is a designer role.
   - This is extremely personal but look for generally attractive, well balanced
     designs that are easy to understand.
   - Including style guide and design system work can also be a positive sign.

### 1. Initial introductions

If we decide to move forward based on your application, the first step is a
two-way introduction. You'll receive an invitation from us for a real-time
introduction video call where we will cover topics like:

1. What is our top-level [mission and direction](../../general/direction/)?
2. What it means to be an all-remote, async company
3. About us (who we are and what we do, where we're at as a company, our
   funding, how our vision evolved, and where we see ourselves in five years)
4. What we're looking for (the passion and technical/domain/communication skills
   we're looking for generally and specifically to your role)
5. Confirm we have a shared understanding of what the role entails (i.e., for
   designers we might confirm you have experience with video apps, design
   systems, and detailed application UX/UI; we might also talk about other
   applications that inspire our own design.)
6. A chance to ask any questions you might have about any of these topics

After this introduction is done, you'll hear back from us shortly (whether or
not we decide to move forward to step two.)

### 2. Role-based interview(s)

This interview is all about the role you're doing. For engineers, for example,
this will be a technical interview. For other roles, you can expect a detailed
conversation on specific projects you've worked on as well as an exploration of
your talents. See below for more details on different kinds of role interviews.

#### Engineering technical interview process

The technical interview is 50 minutes with an engineering leader at the company.
Some example questions are here:

- Have you worked on a technical project related to video before? What was it
  and what was your contribution?
- Have you worked on an open-source project before? What was it and what was
  your contribution?
- What technical project are you most proud of, why, and what was your
  contribution?
- Describe your experience working remotely on an engineering team; what went
  well, what didn’t, and how you tried to ensure the team worked well together.
- Tell about a time you improved a process or system.
- What is the role you tend to play within an engineering team dynamic? Why do
  you gravitate towards that role?
- What are your professional goals over the next three to five years that you'd
  hope to achieve here?

We will also cover domain-specific questions for your area of expertise (for
example, if you are applying for a React developer role, we will ask you
technical questions about React.) We keep a
[list of standard questions](https://docs.google.com/document/d/13dJYYF1o5JEgR0TCimxVAIvVuqP1pGdqa6IVrDkw828/edit#heading=h.6fqjc3ptr2e5)
for consistency across interviews, but these are not public because they are
intended to be a representative sample of the kinds of knowledge we'd expect
someone in an engineering role to have. Making them available ahead of time
would in that case make them less effective.

In some cases, we will also do a coding challenge: a 2-3 hour exercise to
demonstrate your strengths.

#### Designer interview process

The designer interview is 50m. First, we will ask you to take 25m to walk
through one of the projects in your portfolio, highlighting how it exemplifies
your approach to understanding and solving a product design problem.

After this, we will cover additional topics that didn't come up in the portfolio
review such as:

- Explain how you approach understanding a product design problem.
- Once you have collected enough data for problem analysis, what kinds of tools
  and methods do you use to examine it and find a solution?
- What do you do when, even after analysis, you aren’t sure how to solve a
  design problem?
- For Synura specifically, we’re a video collaboration app. What do you expect
  some of the most important things to get right will be? What design topics are
  going to be especially difficult for us?
- Talk about a project you worked on where the design wasn't effective. How did
  you know there was a problem? What do you think went wrong? What did you do
  about it?
- How do you handle constructive criticism regarding the products you design?
- What are some of the best practices you’ve found for collaborating with
  developers?
- What are some of the best practices you’ve found for collaborating with key
  stakeholders?
- How do you think about managing a product holistically as a designer? I.e,
  what is important to understand about your product, your users, and your
  market? How do you keep on the pulse of all of this?
- What kind of written documents do you produce, how do you know if it was worth
  your time?
- How do you keep up with the latest trends and practices when it comes to good
  product design?
- If you were to become known as the world’s best design expert regarding X,
  what would X be?

#### Leadership interview process

If you are joining us in a leadership role the role-based interview process will
be highly tailored to the kind of leadership you bring to the table. You can
check with your recruiting coordinator to find out the plan.

### 3. CEO interview

Before your CEO interview, please be sure to read through our
[direction page](../../general/direction/). The CEO interview tends to be
fast-paced and values succinct answers. There's a lot to get through in a short
amount of time, and the best interviews allow for follow-up questions on your
initial responses. Here are some of the questions that will likely be asked;
you're also welcome to ask me similar questions:

1. How would you introduce Synura to a new prospective customer, colleague, or
   friend?
2. Tell me about your experience with video and why you are passionate about the
   topic.
3. Let's discuss our company [direction](../../general/direction/).
   1. What aspects of it excite you most?
   2. Are there areas that you see as potential concerns?
   3. What do you think will be some challenges we will face in the next 6-12
      months and what are your thoughts on how to address them?
   4. Please share some thoughts about challenges pertinent both to our company
      as a whole and to your role in particular.
4. Tell me about a complex, early-stage project that you worked on. What made it
   challenging? How did you approach it? How does it exemplify how you approach
   your work?
5. What's your experience with remote and async work? What are some of the
   practices you've developed to be effective? What practices from the company
   have you experienced that were helpful or detrimental?
6. Tell about a colleague with whom you've worked in the past who you thought
   was truly exceptional. What qualities or practices did they demonstrate? In
   what ways are you similar to them, and in what ways are you different?
7. Please consider the best and next best teams you've worked with. How would
   you characterize them? What did they have in common, and what distinguished
   the best from its next counterpart?
8. What are you looking for in a CEO?
9. What would you like to do in your first month of working here?
10. Tell me about a time you took initiative.
11. Tell me about a time you improved a process or system.
12. How do you take responsibility for your mistakes?
13. How do you stay motivated?
14. What would you do if you noticed a coworker was doing something incorrectly?
15. What is your ideal work environment?
16. What questions do you have for me?

Note that this is not necessarily exhaustive. We may also explore specific
questions that arose from my review of your experience, your fit with Synura, or
follow-ups from previous rounds of interviews.

#### Advice on how to interview well with the CEO

Here are some tips on how to have a good interview with the CEO:

- Be concise. Answer directly and specifically; let him ask follow-up questions
  as necessary.
- Be willing to ask for clarification if needed.
- Be honest and straightforward. Don't try to spin or obfuscate, even if he
  probes in areas you're not proud of.

### 4. References

We request 5 references before making an offer, of which we will speak with at
least 3. This helps us learn more about you from those who have worked with you
closely. In addition to the obvious benefit of helping us assess what you can
bring to the team and gain context on who you are as a person, it also helps set
the stage for a productive working relationship with you because we can ask for
advice on how best to support your success once onboard.

We would like to talk with a current or former colleague from each of these
categories. An individual from each of the first three categories is required,
and the fourth is required if you're interviewing for a supervisory role.

1. Your direct (or formerly direct) supervisor - someone responsible for or who
   oversaw your work
2. A peer in the same role as you - someone who did similar work to you
3. A cross-functional partner or customer - someone who was a consumer of your
   work
4. Someone you've supervised, if you're applying for a management position
5. Someone else of your choosing

We encourage you to provide people who have worked with you closely and know
your capabilities well. We also encourage you to ask them to be candid so we can
get to understand who you are and what you can bring to the team. We have a
[reference script](https://docs.google.com/document/d/1O06rwGv0nMgrDcRWqMJklUlXWRQ9cH7q/edit#heading=h.gjdgxs)
that we keep internal only because we want candid and spontaneous responses.

### 5. Board interview (only for leadership roles)

Some candidates will be required or interested to meet with our board (you will
know ahead of time if this is the case). Before sending interview invitations,
it's important to prepare a summary doc with relevant information. The interview
doc is internal and should **not** be shared with the candidate.

**Note for Candidates** We recommend you prepare for your conversation with our
investor. Our investor is [Open Core Ventures](https://opencoreventures.com/).
You can browse the
[GitLab CEO Handbook page](https://about.gitlab.com/handbook/ceo/) and in
particular review the
[pointers from his direct reports](https://about.gitlab.com/handbook/ceo/#pointers-from-ceo-direct-reports)
and context on
[how he conducts interviews](https://about.gitlab.com/handbook/ceo/#interviewing-and-conducting-meetings).

The advice on how to interview well with our CEO also applies here.

**Notes for Synura team** Use the procedure below when scheduling on your
calendar directly or on behalf of someone else:

- Create a separate Google doc for each interviewee
- Add a link to the doc from your calendar invite

Include in the doc:

- Candidate's name and current title
- Candidate's email address
- Link to candidate's LinkedIn profile
- Link to Synura's reference check
- Summary of any particular points to address or known flags to probe on.

### 6. Offer

_IMPORTANT: For direct hires, 3-4 weeks lead time is needed between offer and
start date. For contractors, it's just a couple business days._

We use [Pave](https://www.pave.com/) to benchmark compensation generally, but as
per our job listings the offer will depend on several factors:

> The actual offer, reflecting the total compensation package and benefits, will
> be at the company’s sole discretion and determined by a myriad of factors,
> including, but not limited to, years of experience, depth of experience, and
> other relevant business considerations. The company also reserves the right to
> amend or modify employee perks and benefits.

Offers should first be delivered verbally. When communicating equity we
recommend including share count, vesting, and current % on a fully diluted
basis. For people who are unfamiliar with startup equity grants, it's helpful to
explain that the % will decrease later when the company raises new funding, but
all current shareholders (including the founder) are diluted in the same way at
fundraising, and hopefully, a smaller % of a bigger pie means more pie at the
end of the day.

After the verbal offer we send an email attaching a signed offer letter, and
then a legal contract. Some team members join us through an agency, in which
case the details are worked out in the conversation with their agency. For
independent, full-time team members, use the appropriate offer template and
script from our
[standard forms library](https://drive.google.com/drive/folders/1xElcxvOs23eAR07FsLIM6F_fAN62-U86)
(internal only).

To trigger an offer process, use the following forms:

- [New Employee](https://forms.gle/7sm8BbD5eFPah9CG6)
- [New Contractor](https://forms.gle/S9EXafcXmnhnARMX9)

Be sure to check that their country is
[eligible for direct hire](https://help.pilot.co/en/articles/3672752-hire-list-of-supported-countries)
before choosing this option. If not, we can hire them as a contractor in
[most other countries](https://help.pilot.co/en/articles/3672748-pay-list-of-supported-countries)
(essentially any non-sanctioned country).

Make sure you have the following info:

1. Legal name:
1. Address:
1. Phone number:
1. Agreed compensation rate:
1. Preferred currency (only for B2B contractors, direct employees are paid in
   local currency):
1. Direct employee or B2B contract:
1. Planned start date:
1. Equity offer:

After compensation has been determined, create two documents for the candidate:

- "Exit scenarios" spreadsheet (possible outcomes if the company has an exit)
- "Informal offer email" template.

If you need these templates, reach out to People Ops on Slack, and they will
provide you links that automatically create copies. Change the name of the
documents accordingly (e.g., "[candidate's name]'s exit scenarios,") and link to
the exit scenarios spreadsheet from the offer email.

1. Prepare the informal offer email. You'll need to add the following
   information to the template:
   - Candidate's name and email address
   - Candidate's start date
   - Candidate's compensation
   - Candidate's reporting manager
   - Equity offered to the candidate (make this information a link to the
     candidate's exit scenarios spreadsheet)
   - Benefits (determined by the candidate's location)
2. Prepare the exit scenarios spreadsheet. Enter the percentage of equity
   offered to the candidate, and the spreadsheet will update to reflect this.
   Note: Don't play with numbers in the exit scenarios spreadsheet. The revision
   history is visible to the candidate, and they might misunderstand.
3. Once both documents are complete, share the offer email draft, exit scenarios
   copy, and a link to the compensation decision, with People Ops for
   confirmation.
4. After People Ops confirms that everything is correct, the CEO or CTO will
   send the offer email. The offer email is copied directly from Google drive
   and sent to the candidate. When they send the offer, the CEO or CTO will edit
   the permissions of the exit scenarios sheet and share it with the candidate.
   Note: When hiring an international employee,
   [our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only) recommends starting the hiring process a month before the new
   employee's start date.

Note that the position should only be closed and other candidates notified after
the acceptance of the written offer.

## Candidate FAQ

Here are frequently asked questions and our answers:

1. What is your funding situation?
   1. We’re funded by a single investor through a SAFE and not expecting revenue
      immediately. Our goal is to demonstrate product-market fit through growth
      and paying customers, and then start raising again around the beginning of
      summer 2023.
   2. Our investor is [Open Core Ventures](https://opencoreventures.com/), led
      by Sid Sijbrandij, the founder and CEO of GitLab. He has built a
      successful all-remote company that provides an end-to-end platform in a
      market full of fragmented solutions. In addition to funding, he provides
      advice and relationships to help us learn from and build upon GitLab’s
      success.
2. What’s your stance on remote work?
   1. We’re committed to being an all-remote company with no offices. We think
      this yields better results: it will build a culturally diverse workforce
      necessary to operate in cities around the world, offers great advantages
      to employees like control over personal time and no commute, and is much
      more productive than semi-remote environments that disadvantage remote
      employees over those at headquarters.
   2. This does take an investment in the process to make work. For example,
      good hygiene around documented communications, a handbook that’s
      operationally useful and gets contribution from the whole company, and
      team social chats -- agendaless meetings just for socializing. These are
      good practices anyway, but many companies don’t invest sufficiently
      because so much of their process evolves informally.
3. How many teammates are you?
   1. Our current team is listed on our
      [about page](../../../handbook/general/about/).
4. What do you do and where are you headed?
   1. Check out our [direction page](../../general/direction/) for answers to
      this question.
5. What’s the compensation range?
   1. We aim to pay at the average 50th percentile of the job markets in which
      we're targeting hiring. We factor in the role, seniority, and location
      (broadly). Your exact compensation amount will be based on your experience
      level and market conditions. Some references we use to calibrate market
      rates are reports from [Pave](https://www.pave.com/),
      [Birches Group](https://birchesgroup.com/),
      [Payscale.com](https://www.payscale.com/),
      [Glass Door](https://www.glassdoor.com/),
      [Salary.com](https://www.salary.com/), and
      [Blind](https://www.teamblind.com/).
   2. We post compensation ranges in our job postings, so if in doubt check
      there.
6. Have you specified growth targets and timelines? How well are you doing
   against those goals?
   1. Please see details on our [direction page](../../general/direction/)
   2. Please see progress against top-line goals from our latest "Progress
      Update" from our
      [YouTube Playlists](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg).

## Tips for a successful interview

**Arrive on time** For all of our interviews we try to be respectful of your
time by keeping them as short and focused as possible. Arriving late means we
won't have enough time to complete the interview. Either arrive on time or, if
you know you'll be late, reach out ahead of time to reschedule.

**Test your tech in advance** There’s nothing worse than having technical
difficulties during an important call. That’s why it is important to get
familiar with [Zoom](https://zoom.us/) before your interview.

- To begin, download the app to your phone or desktop ahead of time. If you’ve
  never used Zoom before, familiarize yourself with the quick start-up guide for
  new users. See
  [bandwidth requirements](https://support.zoom.us/hc/en-us/articles/201362023-System-requirements-for-Windows-macOS-and-Linux)
- Once you have downloaded the Zoom app, take a moment to start your private
  meeting to get familiar with the interface and features.
- You can take your test one step further by recording yourself and watching it
  to find areas for improvement.
- Ensure that you have a stable internet connection. Use an ethernet cable if
  necessary. You can check your bandwidth through
  [SpeedTest](https://speedtest.net) or
  [Bandwidth Place](https://www.bandwidthplace.com/)
- Test out your sound. Use headphones to hear clearly and to block outside
  noise.

## Internal procedures

### Jobs and boards

We have an
[applicant tracking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.2ac6fr9azryw)
(internal only) to host all jobs and manage our recruiting and interview
process. All team members are welcome to participate and will be asked to play
different roles for different positions.

#### Posting a job ad

Each job may have other specific requirements that will be kept in the job
description or scorecards within
[our applicant tracking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.2ac6fr9azryw)
(internal only).

Here's a template that you can reuse for the new job ads. It only requires
minimal edits to the requirements and duties section of
[our job ad template](https://docs.google.com/document/d/1enNrEONtdaBFyJPsXoRHeqn_t9k3-lb42hxfF9QBCAY)
(internal only).

Regarding the salary range we specify - we have a strict guideline for the upper
band being at most 1.3x of the low band:

- ✅ good: $10k - $13k
- ❌ bad: $10k - $14k

The reasoning behind this is simple - wider bands are not well perceived by most
of the developers we know. They just seem slightly suspicious. There are cases
when we don't know exactly who we're looking for - eg. someone between mid and
senior developer. In that case, we should just post two ads adhering to the 1.3x
rule.

#### Inbound vs outbound recruitment

Where possible we will strive to recruit via inbound channels, which in our case
means:

- Posting the ads to job boards (LinkedIn, NoFluffJobs, JustJoin.it,
  StackOverflow, etc.)
- Newsletters (eg. ruby weekly)
- Forums (eg. forum.rubyonrails.pl)
- Facebook groups
- Any other spots that we consider relevant

For harder-to-fill roles, we will supplement our inbound efforts with outbound
activities. This may include but is not limited to the LinkedIn recruiter,
LinkedIn InMail, cold email, and outreach to personal networks of active
employees (only when we get approval or intro from the particular employee).

All outbound messages should be:

- Concise: no more than 4 paragraphs
- Relevant: mention the particular skills that piqued our interest
- Connected: include a link to a job posting created in the inbound step
- Action-oriented: finish your message with a call to schedule an intro chat
  (link to Calendly or Google Calendar appointments)

All follow-up efforts should be limited to 2 subsequent bumps from our end. WE
ARE NOT NOT SPAMMERS. In the second ping, it can be nice to include something
like "If you're not interested we understand, but if you know anyone who might
be please let us know." Often people know others who have similar skillsets to
themselves that they can recommend.

### Referral incentive program

Synura is offering a referral bonus to team members who refer a software
engineer that we decide to hire. If you know someone who you think would be a
good fit for a position at our company, feel free to refer them. If we end up
hiring your referred candidate, you are eligible for a $500 referral bonus.
Additional rules for rewards:

1. There is no cap on the number of referrals an employee can make. All rewards
   will be paid accordingly.
2. If two or more employees refer the same candidate, only the first referrer
   will receive their referral rewards.
3. Referrers are still eligible for rewards even if a candidate is hired at a
   later time or gets hired for another position.
4. The reward will be paid 6 weeks after the candidate starts at the company and
   is in good standing

### Process for recording candidate scores

Each team member should enter their feedback into
[our applicant tracking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.2ac6fr9azryw)
(internal only), without discussing it first with others. Then we will use 10-15
minutes at the next Strategy call to discuss the feedback synchronously. In
general:

- All team members who participate in the interview process should have
  [Applicant tracking system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.2ac6fr9azryw)
  (internal only) accounts and be added as Interviewer to the role for which
  they are evaluating candidates.
- All feedback must be added as a Scorecard at the end. The reviewer should note
  observed strengths, any surfaced concerns, and (if appropriate) any open
  questions. Cite specifics to help with your memory and help the manager and
  other team members interpret your feedback.
- All Scorecards are summarized with a 4-point rubric, interpreted as follows:
  - Definitely Not: Based on this interaction there were serious cultural or
    behavioral red flags. The scorecard should articulate what those were. This
    could also be used to indicate that there was no evidence that the candidate
    had the necessary skillset; however, hopefully, these situations are
    screened out before the candidate is interviewed.
  - No: Based on this interaction, the reviewer would recommend not proceeding.
    Usually, this indicates that the reviewer did not end with a sense of
    confidence that the candidate demonstrated the necessary skill. If the
    candidate did not convince you, this is the option to choose.
  - Yes: The reviewer felt confident that the candidate can do the job and will
    be a good addition to the team. Candidate actively demonstrated skill and
    will to perform. The reviewer must cite specifics to use this; however,
    there may still be open questions or red flags that are communicated and can
    be followed up on by others.
  - Strong Yes - The reviewer was very impressed and has a specific reason to
    believe that this candidate had the necessary skill and will to perform more
    than what's being asked in the role, and seemed exceptional relative to
    other candidates. The reviewer is excited about adding this candidate to the
    team.
- It is recommended that after each round and all scorecards are complete, all
  reviewers meet synchronously for a few minutes with the manager to provide
  color and context and Q&A about their responses.
