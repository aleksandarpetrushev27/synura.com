---
title: PeopleOps
description:
  This page describes our people-oriented processes and important concepts such
  as DRIs and how to get employment verification letters.
---

# {{% param "title" %}}

{{% param "description" %}}

## Letters of employment & full-time contractor

Please submit your Letter of Employment / Full-Time Contractor to
peopleops@synura.com with the following information:

- Example Letter from the entity to be presented to (Financial Establishment,
  School, etc.) or complete the template here (document TBD).

Your request will be reviewed and returned to you within 72hs of submission.
