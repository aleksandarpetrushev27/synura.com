---
title: Offboarding
description:
  This page describes our offboarding procedures for when someone has left the
  company, either voluntarily or not.
---

# {{% param "title" %}}

{{% param "description" %}}

## Voluntary separation

A voluntary separation occurs when a team member informs his or her manager of a
resignation. The choice to leave Synura was their decision.

If you are a current team member and you are thinking about resigning from
Synura, we encourage you to speak with your manager, or another trusted team
member to discuss your reasons for wanting to leave. At Synura we want to ensure
that all issues team members are facing are discussed and resolved before a
resignation decision has been made. This will allow Synura the ability to
address concerns and in return foster a great work environment.

If a resignation is the only solution after you have discussed your concerns,
please communicate your intention to resign to your manager. We would advise you
to review your employment contract for the statutory notice period and determine
your last working day. Then, you can discuss with your manager the time needed
to work on a handover/transition. If there’s no notice period included in your
employment contract we would advise that you provide Synura 2 weeks of notice.
Hereafter please review the below process which will be followed for voluntary
resignation.

### Voluntary process

1. **Team Member**: Team members are requested to provide an agreed-upon notice
   of their intention to separate from the company to allow a reasonable amount
   of time to transfer ongoing workloads.
2. **Team Member**: The team member should provide a written resignation letter
   or email notification to their manager.
3. **Manager**: Create an Offboarding ticket

## Involuntary separation

Involuntary offboarding of any team member is never easy; however, it is a
reality that we will encounter from time to time. Not every team member hired
will be the right fit for what the company needs; as unpleasant as involuntary
separation may be, it is better than allowing a person to languish in a position
where they are not succeeding. What’s important is to treat the situation with
dignity and respect for the people involved. Below are our guidelines on how to
approach this.

An involuntary separation may be the appropriate course of action if the team
member is unwilling or unable to perform at the level required of their role. In
most cases, this will be a culmination of the process of managing
underperformance, and a series of remediations should be considered before
taking this approach. Team members should not be surprised by an involuntary
separation for underperformance – their manager should have provided a written
warning or actionable performance improvement plan well in advance.

It may also be appropriate if the company needs change and a team member is
unable to adapt. Before embarking on separation, in most cases we will look for
alternative ways for the team member to contribute. Shifts and changes can be
great opportunities to learn new skills or explore other interests, or even take
on new roles. This kind of adaptation can be more difficult when someone holds a
unique role within the company (e.g. “CEO” or “Head of…”), but even in these
situations, we want to try to retain a great contributor if possible.

In either of the above situations, if the manager believes the team member has
been acting in good faith, Synura policy is to offer a severance payment of six
weeks for team members who have served for nine months or more.

Other situations where involuntary separation is appropriate include unethical,
dishonest, or illegal behavior, making other team members or customers feel
unsafe in their work environments, or major strategic shifts in company strategy
or financial position. These will be evaluated on a case-by-case basis.

### Process for involuntary separation

Should a manager decide that involuntary separation is the appropriate course of
action, they must follow the process below. There are a variety of goals to
achieve in the process, including treating the affected team member(s) with
dignity and respect, minimizing disruption to company operations, and fulfilling
legal and ethical obligations.

If the need for Involuntary offboarding arises, the manager must follow this
process:

1. Obtain CEO approval, and notify peopleops@synura.com
2. Share the news with the team member over videoconference, with a PeopleOps
   representative present
3. Disconnect accounts, with support from PeopleOps
4. Share the news with the affected member’s team
5. Share the news with the rest of the company
6. Follow procedures from the standard offboarding process

A few notes on the process:

1. It’s highly recommended by nearly all HR specialists that company accounts
   are disconnected immediately after notifying a team member of involuntary
   separation. This stems from cybersecurity concerns, and Synura will abide by
   this practice by default. However, on a case-by-case basis, a manager may
   offer to pass along a note from the team member with new contact information.
2. Given the
   [asynchronous nature of work](../../general/communication/#communication-norms)
   at Synura, it’s inevitable that many team members will hear about the news
   first through email. We will embrace this – it’s better to communicate the
   news right away and schedule a follow-up video call for Q&A than to try to
   “manage” its rollout at the expense of creating rumors for hours before a
   team may have the opportunity to convene.
3. In addition to these company policies, Synura will adhere to any applicable
   labor laws for the country in which the team member resides.

## Company assets and laptop

If a team member has completed 2 calendar years or more at Synura at the time of
offboarding, they can opt to keep their laptop at no cost. If the team member
hasn't completed 2 calendar years at the time of offboarding or has received a
laptop refresh within the past year, they have the option to purchase their
laptop for current market value from Synura.

Otherwise, the laptop should be returned to the company and the
[fixed assets tracker](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0)
(internal only) updated.

## Offboarding checklist

1. [ ] Inform Synura employees, OCV, accounting, and legal that they are no
       longer an employee.
2. [ ] Update the
       [former employees routing rule](https://admin.google.com/ac/apps/gmail/routing)
       to redirect future work emails to someone appropriate.
3. [ ] Delete their Google Workspace account, reassigning owned files to someone
       appropriate.
4. [ ] Review [tools page](../../general/tools/) and remove access/licenses in
       all systems, reassigning ownership of files as needed.
5. [ ] Verify return of laptop and any other company property
