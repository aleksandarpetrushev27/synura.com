---
title: Performance metrics & evaluation
description:
  This page describes how our people performance management process works. Our
  goal is always to help people be their best with us.
---

# {{% param "title" %}}

{{% param "description" %}}

## Performance feedback

At Synura, performance feedback is a continuous process. We give feedback
(especially negative) as soon as possible. Most feedback will happen during 1:1
meetings, if not sooner. We evaluate and update compensation decisions yearly,
shortly after the anniversary of a team member's start date.

### Managing underperformance

Managing and identifying underperformance can be one of the most difficult
responsibilities of a manager, but it's also one of the most important. As a
people leader, to be fair and transparent with the team member, you want to
address underperformance in a timely and structured way.

Synura will leverage GitLab’s
[skill-based and will-based performance model](https://about.gitlab.com/handbook/leadership/underperformance/#skills-based-performance)
to identify the root cause of the team members' underperformance and with that
determine a suitable remediation plan. The remediation plan will differ based on
the type of performance issue.

### Remediation for underperformance

Managers should address performance concerns early and not wait until the
performance problems have become unrecoverable. Feedback should be given early,
it should be specific and documented. Remediation options will vary based on the
circumstances, but as a best practice managers should leverage one or more of
these approaches:

1. Coaching - engage with the team member to help diagnose the problem(s) and
   solution(s), perhaps with some outside guidance such as articles or courses.
   Requires written follow-up but not cc to PeopleOps.
2. Written warning - an explicit notice that performance is not meeting the
   company’s needs, citing examples of problems and clarity on expectations
   moving forward. (manager must cc [insert group email])
3. Performance Improvement Plan (PIP) - a formal plan documenting problems,
   expectations, and a timeline for a resolution to avoid termination. (manager
   must cc [insert group email]).

With either a Written Warning or Performance Improvement Plan the team member
will meet jointly with their manager and the CEO to review the concerns
outlined, discuss remediation steps, share additional context, or request
additional help or support.

### Coaching

Coaching is the preferred option to deal with underperformance and is the first
step in addressing performance issues.

Managers are expected to address performance concerns promptly. Managers should
address concerns verbally during one-on-one meetings or in impromptu private
coaching sessions with the team member. These conversations should be documented
by the manager and shared with the team member to ensure the context is clearly
“coaching related to underperformance” and not simply “feedback” that may be
provided more regularly and informally. The written feedback also should ensure
alignment on where improvements need to be made and by when, and allow the team
member to request clarification or a conversation with PeopleOps or the CEO. The
note should be brief (a few key bullet points or a paragraph) and should be sent
via email to the team member within 24 hours of the verbal discussion.

The GitLab handbook
[coaching section](https://about.gitlab.com/handbook/leadership/underperformance/#coaching)
has other useful tips, including an email template.

### Written warning

A Written Warning is used to bring attention to new or ongoing deficiencies in
conduct and/or performance issues. The intent is to define the seriousness of
the situation to encourage immediate corrective action. During a zoom meeting,
the manager will inform the team member that they will be receiving a written
warning. The Warning will identify development areas/gaps, outline what success
looks like, and specific action/deliverables and due dates. This letter must
then be sent to the team member and to [insert group email].

As noted above, after receipt of a Warning, the team member will have an
opportunity to discuss it jointly with their manager and CEO.

### Performance Improvement Plan (PIP)

Many companies use a PIP for most involuntary offboarding, as documented support
for releasing a team member. At Synura we think that giving someone a plan while
you intend to let them go is a bad experience for the team member, their
manager, and the rest of the team.

A PIP at Synura is not used to "check the box;" a PIP is a genuine last chance
to resolve underperformance. A manager should only offer a PIP if they believe
that the team member can complete it. The team member should also be committed
to completing the PIP and maintaining the level of performance arrived at
through the PIP. A PIP will not be successful unless both the team member and
the manager believe the team member can succeed.

For team members who truly lack the skill for the role or for those team members
who lack the will to be successful, a PIP should not be offered. PIPs, as
mentioned above, are for team members who with additional guidance, coaching,
resources, or tools can improve and sustain long-term performance.

As noted above, after receipt of a PIP, the team member will have an opportunity
to discuss with their manager and CEO together.

## Performance management framework

To achieve our goals, we must think about how we break them down, and manage
performance to them. At Synura, managing performance has three primary
components:

1. Team-level Objectives and Key Results (OKRs) that we use to drive company
   growth and development of new capabilities
2. Team-level Key Performance Indicators (KPIs) that we use to measure the
   health of existing organizational capabilities
3. Individual performance feedback, that we use to grow as individuals and
   develop our careers

Our process is modeled after GitLab's, but simplified because we're so much
smaller. You can learn more about their process through their pages on
[OKRs](https://about.gitlab.com/company/okrs/),
[KPIs](https://about.gitlab.com/handbook/ceo/kpis/), and
[360 feedback](https://about.gitlab.com/handbook/people-group/360-feedback/).

### Key aspects of Synura performance management

The most important aspects of our performance framework are:

1. OKRs are written and evaluated each quarter and are intended primarily for
   organization functions or teams rather than individuals. They articulate
   measurable goals we aim to achieve to materially advance our capabilities.
   They should be focused and ambitious -- i.e. we should expect between 1 and 3
   OKRs per function and set them to stretch us beyond what we are sure we can
   achieve.
2. KPIs are also at a function or team level and measure the health of existing
   capabilities. Each function has responsibilities that they must contribute
   for the company to run. Finance pays invoices and closes the books;
   engineering rapidly builds high-quality features; our product serves users.
   Each function will keep a variety of Performance Indicators (PIs) to evaluate
   whether it's delivering what's needed. Those that are most important will be
   considered "Key" (a "KPI") and raised to the awareness of other departments.
3. Individual performance feedback focuses on creating a productive dialog
   between a team member and their manager. It aims to build self-awareness and
   foster professional development. We do not have a formal process that ties
   personal goals with team OKRs or PIs. Instead, the process will collect input
   from the team member and others familiar with their work around strengths,
   areas for improvement, and general. We also facilitate feedback from team
   members to their managers.

### Why we don't link OKRs with personal goals

Our performance management framework aims to emphasize that results are achieved
through teamwork. Managers and team members may choose in certain cases to
articulate personal goals, but we don't have a company-wide policy to set goals
at an individual level or to give scores to the attainment of them. Individuals
are accountable for helping the team achieve its goals, and for approaching
their work in ways consistent with our values. We think these outcomes will be
best achieved through promoting a healthy dialog between a team member and their
manager, informed by the input received from peers and with consideration to
overall team performance.

Some more of the rationale behind this approach:

1. We want to prioritize team results over individual outcomes
2. We don't want to create disincentives for taking on ambitious goals, or for
   playing a supporting role
3. Effective teams often realign their work based on the challenges they're
   facing, and individual goals set quarterly can make it difficult for people
   to reprioritize quickly
4. It takes discretion and context to properly understand someone's
   contribution, and emphasizing evaluating individual goals can distract from
   the most important things
5. For clarity, managers do have accountability for the achievement of team
   results

## Performance management process

### How we implement OKRs and KPIs

Notes on our implementation of OKRs and KPIs:

1. An objective and key result should be able to be read in this phrase: "We aim
   to achieve _objective_ , as measured by _key result_ "
2. Each Objective or KR will be prefixed with its year and quarter in the format
   yyyy-qq
3. Each Objective is captured in an Epic that articulates what we aim to
   achieve. If the objective has a top-line result against which it will be
   measured, we will add OKR to its prefix and include that KR in its summary.
   Some objectives may have several key results and not one primary; in this
   case, we add Objective to its prefix.
4. A Key Result or KR indicates how we evaluate the progress of an objective.
   Each is represented as an issue and should have a clear date and way to
   measure or observe that it's been met. Each KR issue is nested under its
   parent Objective epic.
5. KPIs (and PIs) are defined and listed on a function's handbook page. KPIs are
   shared in a function's progress update (either from the handbook or on a
   slide deck)
6. The top-level OKR epic and its children are used to promote visibility on
   what we aim to achieve, and we use the Health status of its issues to monitor
   progress toward our goal. To keep the OKRs easy to understand, we do not link
   the operational issues and epics that track our work into this hierarchy.
   (i.e. the work to accomplish the KR should be tracked separately.)

### Process when we miss goals

Our north star metric has monthly milestones. We aim to hit this
month-over-month. However, growth is not always continuous and there are times
when we won't hit the goal. When this happens, it's a good opportunity for us to
understand what went wrong (if applicable) and refocus our energy and creative
spirit to get back on track. Our process when this happens is as follows:

1. Each teammate takes 10-15 minutes to
   [brainstorm 5-10 ideas](https://forms.gle/4Tpd8PedNHYZXxtH8) to improve.
2. Flag ideas that you particularly like with
3. CEO will review all ideas and choose a few to focus on
4. In the next Sprint Retrospective, we will brainstorm what would be some
   iterative approaches to those ideas, focusing on simple and actionable
   near-term steps. For example: can we get 50% of the value of the idea at 10%
   of the cost? What's the smallest iteration that can make some progress toward
   the goal?
5. We'll write a few stories to add to the iteration about to kick off
