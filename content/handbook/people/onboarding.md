---
title: Onboarding
description:
  This page describes our onboarding procedures, both internal and for the new
  employee, and details on how to get your work computer.
---

# {{% param "title" %}}

{{% param "description" %}}

## Steps before an offer is made

The general process for making an offer is documented on the
[recruiting page](../recruiting/#7-offer). Apart from what's there:

1. Reach out to CEO and CTO and allow them to meet the candidate.
2. Complete a
   [new contractor (or freelancer) onboarding form](https://forms.gle/S9EXafcXmnhnARMX9),
   if applicable.

### Steps after an offer is accepted

1. Once an offer is accepted in writing, reply to the candidate, CCing People
   Ops via his Synura email address to introduce the candidate to him.
2. If the new team member is in the United States, People Ops will reach out to
   them and get any information needed (typically the new team member's home
   address and phone number) and send them a consulting or employment agreement
   through [Docusign](https://www.docusign.com/). If the new team member is an
   international employee or contractor, People Ops will start the hiring
   process in
   [our non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only). Note: International contractor and employment agreements are
   handled by the
   [non-US payroll and expense system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.r2fy9rrzg6i6)
   (internal only) when you start the hiring process.
3. For US employees/contractors After an agreement is signed and stored in the
   correct Google Drive folder, People Ops will start onboarding the new team
   member in
   [our US payroll system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.o8wzj9p4hse2)
   (internal only). If the new team member is a W-2 employee, People Ops will
   reach out to them and schedule an I-9 verification meeting. **Note**: If
   we're hiring in a new state, we'll have to register for state taxes and
   unemployment. This process can be handled by
   [our US payroll system](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.o8wzj9p4hse2)
   (internal only) usually.
4. Before their first day at Synura, People Ops will create a
   [Google Workspace account](https://admin.google.com/ac/users) for the new
   team member, add them to the Synura Gitlab project, create an onboarding
   issue in the [onboarding repo](https://gitlab.com/synura/onboarding), and
   invite them to join Slack. If the new team member needs to purchase a work
   computer, People Ops will set them up with a company credit card with
   [our credit card provider](https://docs.google.com/document/d/1LybPEFdnZHIODLXsbEG9uW49zZIZDflMJNoord6OEYo/edit#bookmark=id.ipdcjld2g29p)
   (internal only).

### US-based consultants

When retaining the services of a U.S.-based consultant, please use the Open Core
Ventures consulting agreement. Follow these steps to fill it in:

1. Change the '**Effective Date**' located on PAGE 1 under the Title.
2. On PAGE 8
   1. Add the name and title of the Employee executing the Agreement in the
      '**CLIENT:**' section.
   2. Add the name of the consultant in the '**CONSULTANT:**' section.
3. On Exhibit A (PAGE 9): 3. Fill in the '**Project Assignment: #**' Section
   with the appropriate Project NUMBER below the tile. For Engineering, project
   numbers start with E followed by a NUMBER. 4. Fill in the '**Dated:**'
   Section with the appropriate date below the tile 5. Fill in the date the work
   will start and the date it will end under '**Schedule Of Work:**' 6. Set the
   hourly fee in Section A of '**Fees And Reimbursement:**' 7. Set the maximum
   amount we may pay the consultant for the project in Section C of '**Fees And
   Reimbursement:**' 8. Add the Scope of Work in the '**Project:**' Section of
   the Exhibit A section (PAGE 9)
4. On PAGE 10 9. Add the name and title of the Employee executing the Agreement
   in the '**CLIENT:**' section 10. Add the name of the consultant in the
   '**CONSULTANT:**' section on PAGE 10 11. Fill in the '**Dated:**' Section
   with the appropriate date
5. DO NOT CHANGE EXHIBIT B AND C.
6. Sending the full contract via Docusign or helloSign.
7. Add signature lines for CEO and Contractor on the main Agreement (Page 8)
8. Add signature lines for CEO and Contractor on Agreement and Exhibit A
   (Page 10)
9. DO NOT TOUCH Exhibit B and C — these are for reference only
10. Send to CEO and Contractor for signature
11. Email peopleops@synura.com to inform them to set up the consultant with a
    1099 form.
12. Save the executed copy of the Agreement to our Shared Drive for contracts.
    (Ask CEO for the link.)

### Onboarding a new advisor

Advisor agreements are sent through [DocuSign](https://www.docusign.com/), using
the "Advisor Agreement" template. To send a new advisor agreement, you'll need
the new advisor's name, and the number of shares they are offered. Once the
agreement is sent, add a new row to the advisory board spreadsheet (this
document, containing contact info and how they help, is not yet created) and
enter the new advisor's information. Use this spreadsheet to track the advisor's
progress through the onboarding process.

**Note**: Be sure to mark any columns that haven't been completed yet as "TODO"

When the agreement is completed, make sure it is in the correct Google Drive
folder, and ask the new advisor to add us on
[Linkedin](https://www.linkedin.com/company/synura),
[Crunchbase](https://www.crunchbase.com/organization/synura), and AngelList
(link TBD).

## All-remote

We're an all-remote company with employees everywhere, and we don't have a home
office. This can be quite different from an in-person company if you haven't
seen it before. GitLab, one of the all-remote pioneers, has a great
[all-remote guide](https://about.gitlab.com/company/culture/all-remote/guide/)
for any company that can help you get going.

The first month at a remote company can be hard. Particularly if you're new to
working in a 100% remote environment, read over the GitLab
[Guide for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/)
and
[how to thrive in an all-remote environment](https://about.gitlab.com/company/culture/all-remote/onboarding/#the-importance-of-onboarding).

Being a manager at an all-remote company comes with its own challenges as well.
Check out GitLab's
[manager guide for building trust](https://about.gitlab.com/handbook/people-group/learning-and-development/building-trust/)
to learn more about how to be effective at this.

## Workstation setup

### Spending company money

As we continue to expand our own company policies, we use
[GitLab's open expense policy](https://about.gitlab.com/handbook/spending-company-money/)
as a guide for company spending.

In brief, this means that as a Synura team member, you may:

1. Spend company money like it is your own money.
2. Be responsible for what you need to purchase or expense in order to do your
   job effectively.
3. Feel free to make purchases in the interest of the company without asking for
   permission beforehand (when in doubt, do inform your manager prior to
   purchase, or as soon as possible after the purchase).

For more developed thoughts about spending guidelines and limits, please read
[GitLab's open expense policy](https://about.gitlab.com/handbook/spending-company-money/).

### Purchasing a company-issued device

Synura provides laptops for full-time team members to use while working at
Synura. As soon as an offer is accepted, someone will reach out to the new
teammate to start this process and get it shipped.

Team members are free to choose any laptop or operating system that works for
them, as long as the price [is within reason](#spending-company-money).

When selecting your new laptop, we ask that you optimize your configuration to
have a large hard drive (video files are large) and be available for delivery or
pickup quickly, without having to wait for customization. We recommend
[Apple MacBook Pro 14", 512 GB](https://www.apple.com/shop/buy-mac/macbook-pro/14-inch-space-gray-8-core-cpu-14-core-gpu-512gb#)
for non-engineers, and
[Apple MacBook Pro 16"](https://www.apple.com/shop/buy-mac/macbook-pro/16-inch-space-gray-10-core-cpu-32-core-gpu-1tb#)
for engineers.

When a device has been purchased, it's added to the
[spreadsheet of company equipment](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0)
(internal only) where we keep track of devices and equipment purchased by
Synura. When the team member receives their computer, they will complete the
entry by adding a description, model, and serial number to the spreadsheet.

You need to have a good workstation to work from home to be a productive
contributor at Synura. Full-time teammates may use company money to purchase
[office equipment and supplies](https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies),
as they do at GitLab. (Part-time teammates or contractors should ask their
manager before making purchases.) We mostly follow the guidelines described in
their policies, though one important distinction is that within Synura,
purchases exceeding USD 500 are company property and are required to be tracked
in the
[Employee Equipment - Fixed Asset Tracking sheet](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0)
(internal only).

There's a link below that provides details on reasonable prices for items on the
list. Look at our [finance page](../../corporate/finance/) for instructions on
how to file expenses.

### Internet

As an all-remote company, you must have high-speed, reliable internet to be
effective.

### Audio/video

A nice webcam and microphone (Apple devices have great ones built-in) are very
useful. Earbuds can be a nice, unintrusive way to improve your audio quality.
You don't need anything professional tier, but you want something that will be
reliable and help you communicate clearly.

### Security

In case of lost or stolen property, the employee should immediately notify
security@synura.com.

## Onboarding process

Welcome to the team! We take onboarding seriously and try to plan out something
that is detailed and works for everyone. There's a week-by-week process built in
where we cover a lot of important topics. Note that onboarding applies to
full-time team members as well as part-time and contract team members.

Before you join, your manager should create an onboarding ticket in GitLab. The
process for them to do this is:

1. Go to
   [our onboarding project](https://gitlab.com/synura/onboarding/-/issues/new)
2. Create a Title of "Onboarding -- employee name"
3. Select Choose a template next to "Description"
4. Select the New Teammate Onboarding template. This template has todos for the
   new teammate, for their manager, and to help set up the proper administrative
   needs.
5. If a manager is not able to adequately commit to onboarding efforts, they
   should appoint an "onboarding buddy", who will be able to fix the sticking
   points in the process as well as answer most questions of the new hire.
   Assign the buddy to the GitLab issue.
6. Schedule a daily check-in for each day of the first two weeks. Add this
   onboarding 1:1 issue as the notes for the agenda

### Improving how we onboard

With each teammate we welcome, we learn a bit more about how to bring people
onboard; as we learn, we update the template. Please be sure to share your
thoughts and suggestions on how we can do better.

### Relocation expenses

If you’re moving for work, Synura will pay relocation costs subject to
pre-approval on the amount by your manager. We rarely if ever do this, though,
as we are an all-remote company.

### Welcoming new teammates

When a new teammate joins, we take the opportunity for the whole company to come
together and welcome them to the team during the bi-weekly team call. We do this
ritual to celebrate the growth of our team, to reflect on how we've grown over
time and how every single new person has changed the trajectory of our company,
to form new relationships among ourselves, and to help everyone know the newest
member of the Synura team.

- Each person introduces themselves, explains their role, and shares some
  tidbits about themselves.
- Each person then calls on the next teammate on the agenda.

## Fast Boots

A Fast Boot is an event that gathers the members of a team or group in one
physical location to work together and bond to accelerate the formation of the
team or group so that they reach maximum productivity as early as possible.

### Why should you have a Fast Boot?

Right now, the fast boot is intended for new teams or for teams with a majority
of new members who need to build their culture of shipping work. If your team
fits this description, you can propose holding a Fast Boot to reduce ramp-up
time and establish and strengthen relationships between team members.

### How should you get approval?

First, raise the idea with your manager to determine if there’s a good case for
a Fast Boot. Ultimately, the executive team will approve these on a case-by-case
basis.

### How should you measure success?

When building the case for your Fast Boot, you should determine what metrics you
will use to measure success. For example, after the Fast Boot, you may expect
your engineering team's throughput to increase or you may expect your support
team's mean time to ticket resolution to decrease. You may also want to measure
the sentiment of the team members after the Fast Boot with follow-up surveys.

### What artifacts should you produce?

During the Fast Boot, the priority is building a culture of shipping work.
Ideally, the group can work together to ship one large, high-impact item to
production. Failing that it could be a backlog of smaller items. Secondarily you
should record and live stream videos that give people insight into what your
team is working on. Kickoff meetings, working sessions, and demos are all great
candidates for being live-streamed.

### Pros & Cons

#### Pros

- Team members enjoyed meeting each other in person and got to spend a lot more
  time together than at the Summit/Contribute.
- Team members now feel more comfortable asking for help, especially across
  teams (FE, BE, UX).
- Team members understood each other's workflows better, allowing them to
  organize better and work more efficiently in the future.
- Remote work can feel very transactional. Fast boots are a way to connect at a
  slower pace.

#### Cons

- It takes time to create a proposal and get approval.
- It takes a fair amount of planning: hotels, flights, meals, content, and
  follow-up.
- It can be expensive.
- It is antithetical to the way we usually work at GitLab, so it's important not
  to use it to establish an in-person way of working.
- It can be mentally and physically exhausting to spend a lot of time with team
  members outside of personal comforts.

### Tips & Tricks

- Hold the Fast Boot in a location where at least one of your team members
  resides. In addition to reducing the cost of flights and hotels, your team
  member can act as a guide to the city and help choose and book locations.
- Set clear expectations about how your time will be spent. Will there be
  downtime? Will the team eat dinner together every night?
- If at all possible, select one day for non-work-related activities on the tail
  end of the Fast Boot. Doing a fun activity will allow team members to get to
  know different aspects of one's personality.
- Keep the event as short as you can (while still getting the results you need)
  to prevent everyone from feeling too burned out by the end.
- Agree and plan what events should be recorded and stick to the plan for the
  rest of the event.
- Prepare any equipment for recording (or streaming) the events while getting
  approvals for Fast boot. Basic cameras might not be sufficient and additional
  equipment might need to be rented.
- When booking accommodation, ensure that everyone gets enough personal space.
  Consider that everyone will spend most of the day working together, so
  personal downtime might be needed.
