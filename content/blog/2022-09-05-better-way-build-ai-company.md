---
title: A better way to build an AI business
date: "2022-09-05"
tags:
  - ai
image: /images/blog/2022-09-05-better-way-build-ai-company.jpg
image_credit_name: Dall-E 2
image_credit_url: https://openai.com/dall-e-2/
author: jyavorska
description:
  "Products in the AI space have quickly gone from pure research to competent
  technology demos, and are now moving into a new phase: synthesis of multiple
  capabilities along a single use case."
---

New AI capabilities tend to evolve through three main phases: from pure research
to usable technology demos, and finally into productized businesses. Once they
reach this last phase, they seem to diverge along one of two paths:

- Large, often very well-funded products that try to solve many problems for
  many people using a single, powerful model.
- Vertical-oriented solutions that connect multiple models, but focus on a
  narrow user, problem, or market segment.

This article makes a case that the latter will prove to be a better, more
profitable model for a significant number of emerging AI startups.

## Research and demo phase

At first, a new AI technology is something that we read research papers on and
see the results of, but aren't able to play with ourselves - especially if we
don't have the technical know-how or resources to set it up ourselves.
[Dall-E](https://openai.com/blog/dall-e/) is a well-known example of a project
that was recently in this phase when they were sharing images that they were
generating but not providing access for anyone else to do so.

Before long, you start to see public demos (often an official one from the
researchers) and single-purpose products that are wrapped in more user-friendly
interfaces, making it possible for anyone to play with them.

- Dall-E now gives anyone (at least after being on a waiting list) easy access
  to providing prompts and generating images through their
  [web portal](https://beta.openai.com/login/).
- [DreamStudio](https://beta.dreamstudio.ai/home) for
  [Stable Diffusion](https://stability.ai/blog/stable-diffusion-public-release)
  provides a similar interface, but Stable Diffusion goes further in that you
  are also able to
  [download and run the model yourself](https://github.com/lstein/stable-diffusion) -
  as long as you are fairly technical.

## Going wide

After the research and demo phases, many projects then form a commercial entity
that tries to productize their tech. Sometimes this evolves directly from the
team that created the demo in the previous phase, but in the case of public/open
source models, it can be anyone. One big example of a company that is
productizing a single model is Microsoft with
[GitHub Copilot](https://github.com/features/copilot), which can generate code
based on its training set.

What's common with these sorts of companies is that they are trying to solve a
broad swathe of problems with a single model, and although they can do amazing
things, it can sometimes feel like they are a hammer in search of a nail - they
have built something powerful, but applying it consistently and generally is
quite hard. It can seem like they do a lot of different things, but none of them
deeply enough to solve any single use case.

## Going deep

An alternative approach to going wide with your model/business is to integrate
multiple AI technologies and research areas into a single product, focused not
on general applicability but on solving problems associated with some quite
specific niche.

This approach is being made possible thanks to the emergence of powerful models
with quite permissive licenses, such as
[Stable Diffusion](https://stability.ai/blog/stable-diffusion-public-release);
you get an almost open source development experience, where you can integrate
various components (models in this case) to build a complete, novel application.

Companies like [DhiWise](https://www.dhiwise.com/) and
[Locofy](https://www.locofy.ai/) are applying similar models as GitHub, but are
doing it within more specific use case verticals (React and Flutter apps for the
former, and converting designs into code for the latter). [Synura](/) is also
taking this approach to integrating multiple research areas into a single
application, focused squarely on making it easier to plan and create videos with
AI assistance.

## A better way

Focusing on a specific kind of user or a specific class of problem allows these
companies to create deeper, more compelling solutions that paper over some of
the rougher edges that you run into when applying similar AI tech to more
general problems. The model doesn't need to do everything anymore, it just needs
to be part of a suite of well-integrated tools.

This combination of tailored, problem-specific solutions with AI assistance is a
much more compelling product, solves real problems for real people today, and
avoids being blocked by difficult edge cases in applying a model more generally.
We should expect to see a lot of these kinds of companies appear and thrive
soon, especially as we see more and more open models and innovation happening.
