---
title: Lessons learned from video game streams
date: "2022-04-06"
tags:
  - video
image: /images/blog/lessons-learned-from-video-game-streaming.jpg
image_credit_name: Alena Darmel
image_credit_url: https://www.pexels.com/photo/a-man-and-woman-sitting-on-a-gaming-chair-while-playing-video-games-7862622/
author: jyavorska
description:
  In comparison with typical webinars, video game streams have led the way in
  user engagement and audience growth over time. What can we learn from them?
---

Is there any video format that gets people more excited than gaming streams on
Twitch? An engaged chat room, interactive participation, and great content -
it's a winning combination. How can we take these lessons and apply them to
streams meant for other kinds of audiences?

## Be well prepared

One of the best things about gaming streams is how spontaneous they feel, but
there's a lot of thought that goes into preparation and priming the audience to
be ready. First of all, you want to make sure your audience knows ahead of time
when you're going to go live and what's on the docket so they arrive in the
right frame of mind. This also gives them a chance to share the schedule with
others who might be interested, which you should encourage them to do. Consider
having an agenda somewhere of upcoming streams and topics that people can find
at any time.

It's also important that you've prepared the content for the stream. You want to
make room for improvisation and spontaneity, but you also want to provide
something that excites, entertains, and educates your users, and that is
difficult to do without a little bit of planning. It can be difficult to strike
that balance - a good rule of thumb is to already have bullet points prepared
that you want to cover, but not an entire detailed script that forces you onto
rails the whole time.

A great way to add variety to your streams without too much additional
preparation is to add guest hosts who bring their related topics, streams with a
special guest who you interview, or just "ask me anything" (AMA) type events
where an interesting person from your team comes with no agenda and answers
questions live.

## Make the experience interactive

Perhaps even more than the feeling of spontaneity, interactivity is a key
difference between great gaming streams and corporate ones. There isn't a
separate question and answer section at the end, but rather an open chat room
during the entire stream where the host is reacting live to the commenters; it's
more like a discussion than a presentation. The biggest thing you can do to
improve the quality of your streams is to embrace this and ensure there is a
chat room and the host is actively participating in it.

You don't want to rely too much on helpers in the chat who answer questions;
that can be useful to augment a busy chat room, but having chat be a separate
stream of thought compared to what is being discussed in the video never feels
quite right. Polls, voting on topics, or even just asking chat for their
thoughts on the subject and reading interesting questions/replies can also be
great ways to build interactive moments into your stream.

This format may feel like you're going off on tangents, but it's more engaging
for the viewers to have a role in the discussion than observe passively. If you
find you ran out of time because the audience was asking so many questions, try
to see that as a good thing and schedule a follow-up event right away!

All that said, if having a chat where all participants engage with each other
isn't your cup of tea, you can opt for a Q&A section only. Many webinar-hosting
platforms offer Q&A buttons that work independently from the chat function and
this can be a good choice, especially if you're just running your first few
streams.

## Build a long-term connection

It's common to do surveys after their streams, but the best bit of instant
feedback comes immediately at the end of the stream. If it was a great one,
people will be excited to like and share the content. The interactivity of the
stream will help you maintain that high level of engagement, but a sincere ask
to your audience to like and share the video with others while you're still top
of mind can ensure that your next one reaches an even bigger audience.

Beyond that, be sure to make an on-demand recording available. Consider making a
highlight reel or standalone snippets of some of the best moments from the
stream, especially if it was a particularly engaging one. These bite-sized bits
of content can be useful to get important sub-topics out there, as well as
attract new viewers. You can share these out to other social networks to test
the waters in places you might not usually have a presence (yes, TikTok, I'm
looking at you!). A social media outlet you don't normally work with can do
wonders since you reach people who might not have known to search for you in the
first place.

The final ingredient for growing your audience is the same as it is for
streamers (or any other kind of creative content): be consistent. Engage your
audience regularly and be predictable. You don't need to stream daily, but
whatever schedule you have, make sure it's like clockwork and you'll see people
coming back for more.

Keep these tips in mind, and you'll be producing awesome content and growing
your audience in no time. Do you have more ideas? We'd love to hear about them
on our [Twitter account](https://twitter.com/SynuraHQ) so we can add them to
this article.
