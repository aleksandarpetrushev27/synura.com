---
title: Async means attendance is optional
date: "2022-08-31"
tags:
  - async
image: /images/blog/async-means-sync-optional.jpg
image_credit_name: Andrea Piacquadio
image_credit_url: https://www.pexels.com/photo/photo-of-man-holding-a-book-927022/
author: jyavorska
description:
  Synura is an all-remote async company, which means that although we have many
  people in different time zones (and no office) we still get things done. How
  do we do it?
---

Async work is a hot topic these days, with more and more companies trying it
out. One thing that there still is a lot of confusion about, though, is what
exactly it means to be async. In this article, we explore a little bit about
what that means for us here at [Synura](/) to be an all-remote, async-first
company.

## Real-time collaboration

One thing you might be surprised to learn is that we have a lot of real-time
collaboration. Although we rarely have meetings, we do have them sometimes. We
also have lively chat threads, conversations on issues or merge requests, pass
videos back and forth to each other, and much more. It's not uncommon for these
conversations to be happening rapid-fire with many people contributing over a
short time.

The way we make this work without compromising async-first is one simple belief
and rule: **all real-time collaboration is optional**. This has a few surprising
consequences:

### It makes it easy to cancel meetings

First of all, to schedule a meeting that's going to be optional for everyone,
you need to create a clear agenda that the attendees will agree is important.
That on its own is a big win. Even more powerful is that, since everyone is
invited to decline the meeting, it triggers everyone to make an active decision
on whether or not want they want to be part of that conversation or not.

If everyone decides they are fine only hearing the outcome, then you (as the
scheduler) have just been fully empowered to go ahead and make your decision, or
if your meeting was a presentation, to just share the slides or video. Quickly
you'll find that the habit of status meetings and presentation readouts on
autopilot fade away, and the real-time conversations you do have are impactful.

### It ensures that there are notes to be reviewed later

Since you can't guarantee that everyone (or anyone) will be there, it becomes
mandatory (and also quite natural) to take notes, record the meeting, and
summarize everything at the end. This ensures that there's always a record that
can be referred to later, once memories fade a bit.

A surprising additional benefit is that this creates a built-in reflection
period to any real-time conversation, where people - even those who attended the
meeting - can (re)read the notes, sleep on them, and come back with something
new and important that still adds to the conversation. This helps ensure
different voices are part of the discussion, even those who don't necessarily
feel confident speaking up during the meeting.

### It ensures that things are shared widely

You've finished and summarized your meeting, but now you need to get the word
out. One of the ways we use Slack is as a topic-based information feed, almost
like a subscription to anything related to the channel topic. That means it's
easy for us to share a link to notes for anyone who is interested - just drop a
link to the meeting notes/recording in the appropriate channel.

As a side effect, this lets anyone easily (and with intentionality) choose when
and where to engage in any conversation. The post-conversation updates make it
so everyone can see what's going on, confident that they don't _have_ to engage
unless they want to. What we avoid are situations where someone would have liked
to have been included, but never even heard about it until it was too late.

## One and two-way door decisions

You may wonder if all this sharing and allowing time for reflection slows us
down. In general, it does not, because we're able to push forward more
activities simultaneously than an organization that is single threading through
meetings all day. That said, there's an important distinction here between
[one and two-way door decisions](https://notes.serverlessfirst.com/public/One-way+vs+two-way+door+decisions).

- **One-way door decisions** are the kind where, once you go through the door,
  there's no way to undo the decision. For these, we intentionally slow down and
  over-communicate that the conversations are happening, to ensure we don't walk
  into a mistake, and ensure everyone has a chance to engage with the decision
  and reflect on it.

- **Two-way door decisions** are ones where you can unmake or change a decision
  a few days (or even weeks) later. These lend themselves quite naturally to an
  iterative, async approach, and so often the notes come in the form of
  something like "we've made a decision, here's what it is and why; we're moving
  forward, but please let us know if you have any feedback." This avoids
  unnecessarily blocking achieving consensus in situations where it isn't
  important to do so.

## An important exception

One-on-one meetings are the only formal exception we have to the "all real-time
collaboration is optional" rule, and that's for one simple reason: one on ones
are where we do performance management, and that kind of feedback tends to come
across more clearly when delivered face to face. In the end, though, while this
is recommended it's still up to each manager/employee pair to decide what works
best for them.

## Give it a try

Hopefully, no matter your situation, these guidelines inspire you and your team
to try setting a similar rule. They can work from small companies like ours up
to
[quite large ones](https://about.gitlab.com/company/culture/all-remote/meetings/#1-make-meeting-attendance-optional)
(the Synura process was adapted heavily from what some of our employees did
while working at
[GitLab, an async work pioneer](https://about.gitlab.com/company/culture/all-remote/asynchronous/).)
You can also read more about our practices in detail in the
[communication section of our public handbook](/handbook/general/communication/).

You may be surprised at how well it plays out, and anyway it's pretty safe to
experiment.
