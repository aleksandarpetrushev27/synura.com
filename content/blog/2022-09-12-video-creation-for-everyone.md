---
title: How we will make video creation easy for everyone
date: "2022-09-12"
tags:
  - video
image: /images/blog/2022-09-12-video-creation-for-everyone.jpg
image_credit_name: Mikhail Nilov
image_credit_url: https://www.pexels.com/photo/happy-woman-holding-a-pencil-and-notebook-while-filming-7594319/
author: jyavorska
description:
  Our vision at Synura is "video creation for everyone", but what exactly does
  that mean and where are we headed?
---

Our vision at Synura is "video creation for everyone", but what exactly does
that mean? Our home page describes our product as follows:

> Synura is a collaborative video planning and editing app. It helps streamline
> your creation flow and offers automatic, AI-powered edits. You don't need to
> be an expert to make amazing videos!

In this article, I'll be sharing a birds-eye view of what our vision means and
how we are bringing it to reality.

## Our users

There are very specific kinds of users we want to help, who we like to think of
as members of the “video-native” generation. They have a passion for videos, and
they might want to become professional or hobbyist creators themselves, but they
don’t know where to start because there is a ton of knowledge required.

They may be creating videos for themselves for their own YouTube or TikTok
channel. They may also be doing it as part of a company - for example, on a
marketing team that is stepping into video production. Either way, there are two
main problems that they run into:

- Knowing _how_ the video production process works and how to collaborate. For
  example, there are different stages, and different things happen for a reason
  during each phase. Without this knowledge, things tend to be inefficient.

- Being able to _apply_ their vision without having technical editing ability.
  There isn't any product for videos or streams yet that makes powerful editing
  techniques as simple to use as image filters on Instagram.

We want to remove these roadblocks to help them bring their creative visions
into the world.

## Our technology

Removing these roadblocks is where our technology comes in. There are many basic
editing practices that normal people don't know about. For example, having your
video be loud (without clipping) or adding intros, outros, and music. We want to
make everything you need to make your videos look great be "one-click simple".

In addition to the above, we are building a framework for planning and
collaborating on videos. We're putting the entire production process on rails
with built-in templates and learning resources. We'll guide you through the
creation process, from ideation to publishing.

Most exciting of all, we are taking the
[innovation](/blog/2022-09-02-stable-diffusion-open-source-ai/) happening in AI
and video and
[bundling it to make it accessible to everyone](/blog/2022-09-05-better-way-build-ai-company/).
We'll be starting with features like background noise reduction, transcriptions,
and "umm" removal. The limitations here are

## Our business

As a company, we're designed to iterate on applying these technologies to these
problems. We're funded by [Open Core Ventures](https://opencoreventures.com) and
have runway ahead of us to explore. We are also proud to be
[supporters of open source](https://opencollective.com/synura), with a focus for
now on OBS (Open Broadcaster Software).

We also want to build a sustainable business that aligns with the success of our
customers. One way we do that is with our [pricing](/pricing/). We will be
giving away planning and collaboration features for free, and charging only for
storage and processing.

## When will it be available?

Next on the agenda is launching our initial version into early access in the
next few weeks. It will be a minimal version, but already solve real problems
for real people. It will be the springboard for continued learning and
iterations of our product.

If you're interested in checking it out as soon as it becomes available, there
is a [signup link](#signup) just below this article.

## Get involved

There are a few ways to get involved apart from signing up. You can check out
our [careers page](/handbook/general/about/#careers) if you'd like to join the
team. We also frequently update our [blog](/blog/) with detailed
[progress updates](/tags/progress/) and articles on video, collaboration, AI,
and more.

We're a [source-available](https://gitlab.com/synura) company, so merge
requests, feature requests, or comments on issues are all welcome. You can also
contribute suggestions to [this website](https://gitlab.com/synura/synura.com)
as well as
[our public handbook](https://gitlab.com/synura/synura.com/-/tree/main/content/handbook).

Finally, if you're a prospective user, I'd love to hear from you. I will reach
out to you if you sign up for our waiting list but feel free to
[email me any time](mailto:jason@synura.com) to talk about anything at all. My
vision for Synura is to build something together, so I'm always happy to hear
from you.
