---
title: August 28th progress update
date: "2022-08-28"
tags:
  - progress
image: /images/blog/2022-08-28-progress-update.jpg
image_credit_name: Terje Sollie
image_credit_url: https://www.pexels.com/photo/person-holding-canon-dslr-camera-close-up-photo-320617/
author: jyavorska
description:
  Every two weeks we complete an iteration and share our progress. Check out
  this article to see what's new and where we're headed next.
---

Hello everyone, and welcome to the first Synura progress update! In this update,
we share what we worked on during the last two weeks so that you can give us
feedback. If you'd like to chat about anything Synura-related you can always
reach us at [contact@synura.com](mailto:contact@synura.com).

### Goals

Our biggest focus for these two weeks was on making progress towards
[launching](https://www.synura.com/handbook/general/direction/#roadmap), so we
can learn how real people actually use the product. We also set three specific
goals for ourselves:

- Complete authentication flow (can create an account and log into the
  production app)
- API structure for basic project operations (create, replace, update, delete)
  is defined and documented
- 20% increase in early access signups

The most important was the 20% growth metric, which draws inspiration from the
[week-over-week rule](https://about.gitlab.com/blog/2020/05/05/wow-rule/). Once
we've launched, we'll be changing this to an active user metric, and then
revenue.

### Results

We achieved a **29% two-week increase in early access signups**, along with a
**17% increase in unique visitors to the site**, which is super exciting. Part
of how we achieved them was through a redesign of our [home page](/), which is
now more focused on explaining the product more clearly and making the signup
the most obvious call to action. We are also seeing success from tailored cold
outreach to prospective customers on LinkedIn.

![Visitor and signups graph](/images/blog/2022-08-28-visitors-signups.svg)

At the start of the iteration, we estimated we were five weeks away, and now we
are a little more than three weeks away. Progress was good, but unfortunately,
we are still a small team - an urgent personal issue with a team member came up
and this meant we didn't get as far as we would like. The authentication flow
was completed for the log-in use case, but not registration. The API structure
was also defined, but not completely implemented. In the future, this kind of
delay will be mitigated as we grow the team; we are opening two more engineering
positions on Monday.

We also saw great responses to our latest
[walkthrough video](https://www.youtube.com/watch?v=58yl0FUcIho). One use case
that has been appearing frequently in customer interviews is
[helping video coaches/advisors manage their clients](https://www.synura.com/handbook/general/direction/#advisory),
so we produced a
[short video explaining that use case](https://www.youtube.com/watch?v=40ggpCesSRY)
as well.

On Monday we will be setting our goals for our next iteration and will share our
progress again two weeks from now. If you have any thoughts on the above update
or where we should head next, you can
[reach out at any time](mailto:contact@synura.com).
