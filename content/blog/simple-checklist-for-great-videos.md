---
title: A simple checklist for making great videos
date: "2022-04-07"
tags:
  - video
image: /images/blog/simple-tips-to-improve-video-impact.jpg
image_credit_name: Gustavo Fring
image_credit_url: https://www.pexels.com/photo/dressmaker-filming-her-self-happily-3984877/
author: jyavorska
description:
  Producing videos is a complex technical and coordination task, but there are a
  few simple tips and tricks that can help you reach your audience better.
---

Creating a video can feel overwhelming because there are a lot of little details
you need to keep in mind all at once. This checklist can help you ensure you're
ticking all the right boxes to make something great.

## Pre-production

1. [ ] Make sure you know what you want to accomplish before you start. This
       includes having an outcome in mind (preferably a goal you can measure)
       and knowing exactly who your audience is and why they care. This
       information will set the stage for every step that follows.

2. [ ] Depending on how creative a video you're making, a mood board can help
       you capture some of the non-tangibles for your video. What are the
       colors, themes, and feelings that you want to communicate? It can be hard
       to put this into words, but collecting some images or sounds and putting
       them into a folder can help inspire you. Just watching a few videos from
       other producers that you like can bring a fresh perspective.

3. [ ] Write a script ahead of time, but leave it loose enough that you aren't
       just reading. Your viewers will find improvisation (and even small
       mistakes!) more natural and engaging, but if you over-rehearse or just
       read a detailed script it will feel stilted.

4. [ ] This is the best time to get wide feedback on your ideas. Share the
       topics and high-level points you're thinking of covering and collect a
       lot of thoughts and ideas, but don't forget to edit them down - the best
       videos are quite focused, and so once you have all the ideas on the table
       it's important to take the best and leave the rest. Some can be ideas for
       future videos, but there's no reason to get too attached: if they are
       good ideas, they will come up again.

## Recording

1. [ ] Get comfortable with your recording software. Do a few dry runs with
       scene changes before you start reading through your script. Watch the
       video to make sure you're happy with the lighting and audio. Monitoring
       your audio in headphones can be awkward at first, but helps. Comfort with
       your tools will help you relax and focus on your content.

2. [ ] Speak a little slower than you normally would, but comfortably so. You
       want to speak naturally, but as if you're speaking to a diverse audience
       who may not hear well or may not speak the same native language as you.
       You don't want to be so slow as to bore them, but keep in mind the
       materials are new to them.

3. [ ] You don't need expensive equipment to make a great video. Often the
       hardware built into your laptop or phone can be good enough, but you do
       need to make sure you're in a quiet environment, without echoes, that is
       light enough. A naturally lit space during the day with the windows
       closed is enough for most purposes.

## Post-production

1. [ ] Perfectionism is the enemy. As you review your video after recording,
       don't get too hung up on making things perfect. If you're producing a
       video for an audience you've identified, with content you know they are
       interested in, they will appreciate it. It's better to get something out,
       get feedback, and iterate on your production process for next time than
       it is to wait until everything is just so. Sincerity and relevance win
       out over perfection any day of the week.

2. [ ] The exception to the perfectionism rule is audio quality. A lot of other
       things can be imperfect, but if the audio is bad nobody is going to
       understand what you're saying and they will stop listening. Check a few
       different devices and make sure everything is crystal clear.

3. [ ] Final feedback at this point is important, but it's key to keep it to
       just a few important reviewers. Preferably, ones who are a lot like the
       kinds of people the video is targeted at. If you ask too many people for
       feedback, you'll start to see conflicting requests (speed up, and also
       slow down) and you can get bogged down in confusing feedback.

## Publishing

1. [ ] You should pick an attractive thumbnail (human faces tend to be
       interesting to other humans, so are nice to include) and you want to
       select a clear title that sparks curiosity. You don't want to go off the
       deep end (if it starts to sound like clickbait you may have gone too
       far), but it's important to give your video a chance to stand out.

2. [ ] Different platforms support different bookmarks, tags, and other
       metadata, and it's important to take advantage of it to improve ease of
       use and discoverability. For example, on YouTube, you can add timestamps
       to the video description, which will help viewers jump around to key
       moments they are interested in. Tags can be a great way to show up in the
       right search results.

Of course, there's a lot more you can do than this to make even better videos,
but if you get these fundamentals right you'll already be off to a great start.
Do you have more ideas? We'd love to hear about them on our
[Twitter account](https://twitter.com/SynuraHQ) so we can add them to this
article.
