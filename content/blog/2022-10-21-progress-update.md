---
title: October progress update
date: "2022-10-21"
tags:
  - progress
image: /images/blog/2022-10-21-progress-update.jpg
image_credit_name: Zhang Kaiyv
image_credit_url: https://www.pexels.com/photo/camera-on-the-ground-3244384/
author: jyavorska
description:
  Every month we complete share our progress. Check out this article to see
  what's new and where we're headed next.
---

It's time again for another Synura progress update. As with every pre-launch
update, our goals were around wrapping up our launch features and growing our
user community:

1. Deliver
   [1.0 milestone](https://gitlab.com/groups/synura/-/milestones/1#tab-issues)
1. Increase early access signups by 20% every two weeks

### Results from this month

We've improved our rate of delivery for MVP launch features in a few ways.
First, we welcomed a new team member who is an expert on video processing and
will help us build features like automatic transcription, "umms"/background
noise removal, and more.

We are also wrapping up our 1.0 design system which will inform our product look
and feel going forward. As a company that helps non-experts make videos, user
experience is especially important and this was a critical step in accelerating
front-end development.

Although we haven't completed the full milestone yet, we have now finished
several key features:

1. User account creation and email validation
1. New project creation/deletion
1. Uploading video files to projects
1. Video processing backend (we used transcription as the proof of concept)
1. Administration interface

Across the two iterations that ended this month, we also increased early access
signups from 254 to 314. This missed our goal of 20% (we hit 10.2% and 12.1%,
respectively). This was primarily due to less focus on content marketing as we
needed to temporarily turn our attention to hiring. We made two offers, both
accepted, and one person has now already started. The other will start on Nov 1.
We also opened two additional positions (Product Designer and Rails Developer)
which are still in the interview process.

![Visitor and signups graph](/images/blog/2022-10-21-visitors-signups.svg)

### Expectations for the next month

We expect to launch the MVP product in the next month since we're already close.
The remaining elements are:

1. Wrap up various basic user features (reset password, email validation)
1. Additional file operations (delete, rename)
1. Wire in frontend to video processing ("generate captions" button)

From there we can let users on the platform to organize and add files into their
video projects. We'll also look to add more project and asset metadata (due
date, stage), templates, learning resources, more of the basic features you see
in the [mockups](/mockups/), and finally the ability to invite other users to
collaborate.

We'll also be attending [Web Summit](https://websummit.com/) at the beginning of
November, and [Slush](https://www.slush.org/events/helsinki/) in the middle of
the month. If you'll be at either event please reach out and say hello, we'd
love to talk to you about the product.

Finally, we are resuming our push on content marketing to re-accelerate new user
signups and visitors and get back to hitting our 20% growth goal. We're doing
this by working with a writer and video producer to help us generate even
better-performing content that can help us drive people to learn more about
Synura and sign up.

If you have any thoughts on this update, where we should head next, or if you'd
like to chat about anything Synura related you can always reach me at
[jason@synura.com](mailto:jason@synura.com).
