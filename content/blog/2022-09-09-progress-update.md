---
title: September 9th progress update
date: "2022-09-09"
tags:
  - progress
image: /images/blog/2022-09-09-progress-update.jpg
image_credit_name: David Bartus
image_credit_url: https://www.pexels.com/photo/woman-leaning-back-on-tree-trunk-using-black-dslr-camera-during-day-610293/
author: jyavorska
description:
  Every two weeks we complete an iteration and share our progress. Check out
  this article to see what's new and where we're headed next.
---

Hey everyone! Fall is arriving here in the Netherlands which means the leaves
have started turning and it has gotten a lot rainier, but it also means there is
a lot of time for drinking coffee and working on Synura. :)

With that, I'm very excited to bring you the latest progress update. During the
last two weeks, our focus was on making continued progress toward
[launch](https://www.synura.com/handbook/general/direction/#roadmap). To get
there, there were a few key things we wanted to do:

- ✅ Go from ~3.5 weeks remaining until
  [MVP launch](https://gitlab.com/groups/synura/-/milestones/1#tab-issues) to
  ~1.5 weeks
- ✅ Wrap up the last iteration's objectives (document API structure; wire in
  user registration)
- ✅ Get our build, test, and deployment pipeline in place
- ✅ Grow active waiting list members by 20% (from 125 to 150)

## Results

Not only did we deliver all the green checks above, but we reached a total of
207 waiting list members - this represents **an increase of 66%, rather than
just the 20% we were aiming for**. Over the same period, **unique website
visitors increased by 76% (from 153 to 270 over the previous two weeks)**.

![Visitor and signups graph](/images/blog/2022-09-09-visitors-signups.svg)

A few things contributed to this:

- OBS released a new major version, and we
  [sponsor the OBS project](https://opencollective.com/synura). We had a lot of
  visitors check out our site after visiting the OBS home page.
- We started a quite modest (<$10/day) Reddit ad campaign to begin experimenting
  with advertising-based lead gen. Although this didn't generate a lot of
  referral traffic, it may have contributed to general product awareness.
- We wrote two blog posts that captured some of the world's attention on AI and
  Stable Diffusion. One was on how
  [Stable Diffusion represents a new open approach to AI](/blog/2022-09-02-stable-diffusion-open-source-ai/),
  and the other was a follow-up going into how
  [businesses will be able to leverage these open models much like they do open source](/blog/2022-09-05-better-way-build-ai-company/).
  Both ended up being significant sources of organic web search traffic.

I'm excited to see how many of these sign-ups we'll be able to convert into
users once we launch.

## Other news

We've opened up [two new positions](/handbook/general/about/#careers) and have
had an amazing number of applications from great candidates. We are looking
forward to growing the team very soon since this will help us accelerate
front-end development as well as increase our ability to do research and
development around AI and video.

On Monday we will be setting our goals for our next iteration. If you have any
thoughts on the above update, where we should head next, or if you'd like to
chat about anything Synura related you can reach me at
[contact@synura.com](mailto:contact@synura.com).
