---
title: Speaking without words
date: "2022-06-20"
tags:
  - video
image: /images/blog/speaking-without-words.jpg
image_credit_name: Oleksandr Pidvalnyi
image_credit_url: https://www.pexels.com/photo/woman-in-gray-crew-neck-t-shirt-3791983/
author: tpanaritis
description:
  Can you tell a story on video without a single word? Let’s see how to use body
  language best practices to get your message across.
---

Can you tell a story on video without a single word? Not since the first talkies
premiered in the 30s, but the impact of your words is determined from the moment
you appear on camera. Let’s see how to transform traditional non-verbal cues
into digital body language best practices that will help you get your message
across.

## Make a statement with your frame

Speaking in person at a conference, a training session, or a keynote on
location, people get all of you right away. They see your body from top to
bottom and receive cues during your presence on stage. Remotely, though, you
can't read the room and adjust your mannerisms to suit your audience.

When it's time to go live, the first thing to do is get comfortable, tidy up the
background behind you and make sure the camera on your screen or tripod is
pointing at you. The ideal distance is an arm’s length from the lens. What’s now
left is to adjust the framing of the camera by applying the rule of thirds:
divide the screen into thirds using vertical and horizontal lines and position
your eyes at the top line of the grid. You can either stay centered, or move
toward the left side of the screen.

![Rule of threes diagram](/images/blog/speaking-without-words-threes.jpg)

Framing yourself following the golden rule of broadcasting ensures that not only
your head is standing at the right height, but also your body is in the picture
offering your shoulders, chest, and hands to the audience.

## Sync your body with your script

Having [prepared your script](../simple-checklist-for-great-videos/) ahead of
time and having rehearsed it to the point that you only need a glimpse of those
bullet points, it’s time to connect with the audience while delivering your
message. Practice looking at the camera when you welcome the attendees or
explain a concept and never miss the lens when you invite them to take action,
such as answering a poll or asking questions in the chat. Just remember,
blinking is allowed - you’re only human after all!

Another way to appear warm and approachable is by allowing the audience to see
your hands. Although in webinars this is quite a task as you often juggle slides
and potentially screen sharing alongside a live chat, this is where your hands
are needed the most. If you stay still and just move your lips, people will get
tired, disconnect from you and find you less likable. By allowing your hands to
move naturally as you speak, you elevate your credibility and connect with the
audience in a way that they feel comfortable accepting you as a presenter and as
a messenger bringing them valuable content.

## Bring out the props

We’re not talking special effects here, but what about little details around you
that make impressions that help to convey your message? They can be household
items that you have around you and only need the right placement to frame you
positively on video.

For example, plants are an indication of tranquillity and peace, books can make
you look more professional. Including items related to your hobbies help create
a memory that helps people to connect with you on a different level. A crowded
bookshelf might seem overwhelming, but a potted plant next to a couple of books
and maybe a dog toy on a side table behind you can create a calming and
welcoming environment.

Lastly, don’t forget about clothes! No need to visit the costume department, but
make sure you take a trip to your closet before going live. No matter if it’s a
suit and tie show or a jeans and t-shirt gig, the key here is to choose colors
that compliment you and help you pop out of your background. A white shirt in
front of a white wall doesn’t do you or the audience any favors. But the same
shirt in front of a dark blue wall will create enough contrast in the video to
capture the eye and the attention of your participants.

In any audience interaction, live or virtual, onstage or on video, considering
digital body language is not a matter of vanity. Investing in non-verbal
communication cues connects you with the attendees and enables you to use that
established rapport as the first step towards a relationship that is meaningful.
