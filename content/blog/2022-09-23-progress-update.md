---
title: September 23rd progress update
date: "2022-09-23"
tags:
  - progress
image: /images/blog/2022-09-23-progress-update.jpg
image_credit_name: Pixabay
image_credit_url: https://www.pexels.com/photo/close-up-photo-of-camera-shutter-414781/
author: jyavorska
description:
  Every two weeks we complete an iteration and share our progress. Check out
  this article to see what's new and where we're headed next.
---

It's time again for another Synura progress update! As with every pre-launch
iteration, our goals this time were around wrapping everything up that we need
to reach that point:

- ❌ Go from ~1.5 weeks remaining until
  [MVP launch](https://gitlab.com/groups/synura/-/milestones/1#tab-issues) to
  done
- ✅ Grow active waiting list members by 20% (from 207 to 248)

### Results from this iteration

Unfortunately, missed our goal of getting to MVP launch. As a team of three, we
are impacted more heavily by personal emergencies that impact availability than
a larger team would. After this happened last time we opened up two more
positions, which are now nearing the offer stage, so we expect to be able to
mitigate this better going forward.

On a more positive note, we exceeded our 20% goal of growing waiting list
members by 3%. This was done without as heavy a push on content marketing
compared to last time, indicating that even though that reduction resulted in
fewer total new visitors, there appears to be sustained momentum working to our
advantage when it comes to signups.

![Visitor and signups graph](/images/blog/2022-09-23-visitors-signups.svg)

During this iteration, the one new blog post we made was about
[how we will make video creation easy for everyone](/blog/2022-09-12-video-creation-for-everyone/)
which is likely of interest to progress update readers like you. It contains a
birds-eye view of our direction as well as information on how to get involved.

### Expectations for next iteration

I hope to close both [open positions](/handbook/general/about/#careers) within
the next iteration, with start dates shortly thereafter. We're also continuing
to follow along with developments in AI generation and assistance for video;
with more open models like Stable Diffusion and Whisper, the pace of innovation
is extremely exciting.

Our team goals for the next iteration will remain focused on MVP launch and
continued growth of early access signups.

If you have any thoughts on the above update, where we should head next, or if
you'd like to chat about anything Synura related you can always reach me at
[jason@synura.com](mailto:jason@synura.com).
