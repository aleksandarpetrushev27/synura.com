function addToIndex(person, peopleIndex) {
  newPerson = document.createElement("li");
  newPerson.innerHTML =
    "<a href='#" + person.id + "'>" + person.innerText + "</a>";
  peopleIndex.appendChild(newPerson);
}

document.addEventListener("DOMContentLoaded", function () {
  var tableOfContents = document
    .evaluate(
      "//a[contains(., 'The team')]",
      document,
      null,
      XPathResult.ANY_TYPE,
      null
    )
    .iterateNext();
  const peopleIndex = document.createElement("ul");
  tableOfContents.parentElement.appendChild(peopleIndex);
  var peopleHeaders = document.querySelectorAll(".biolink");
  if (peopleHeaders) {
    peopleHeaders.forEach((el) => addToIndex(el, peopleIndex));
  }
});
