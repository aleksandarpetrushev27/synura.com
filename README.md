# synura.com

This repository contains our main website as well as our handbook.

## Deployment

The `main` branch is automatically deployed to synura.com using Netlify.

## Git LFS

This repository contains large files, like images and videos. You should install
[git-lfs](https://git-lfs.github.com/) to use it.

Instructions on how to use LFS with GitLab can be found
[here](https://docs.gitlab.com/ee/topics/git/lfs/).

If you add new file types to LFS, you must also update the GIT_LFS_FETCH_INCLUDE
value in the Netlify deployment config.

## Static site framework

We use [Hugo](https://gohugo.io/) as the static site framework.

After installing hugo, use `hugo serve -DF` to run the site locally.

## CSS framework

The website uses [UIKit](https://getuikit.com/docs/introduction) as our CSS
framework because it is easy to use and looks nice with minimal fuss. We also
support the icons found in
[UIkit icons extended](https://hajnyon.gitlab.io/uikit-icons-extended/).

## Cookie consent

We use this lightweight
[cookie consent script](https://github.com/orestbida/cookieconsent) to ensure we
honor our users cookie and tracking preferences.

## Tools

There are various tools in this repo you can install with `npm install`. Note
that you must first install [node.js](https://nodejs.org/en/download/) to use
them.

### Prettier

You can use [prettier](https://prettier.io/) to make the markdown look nice.
There is a nice VS Code extension for prettier which you can set up to
automatically format on save.

```bash
npx prettier . --write
```

### Markdownlint-cli

[`markdownlint-cli`](https://github.com/igorshubovych/markdownlint-cli) will
identify other errors and style problems in the markdown files.

```bash
npx markdownlint .
```
