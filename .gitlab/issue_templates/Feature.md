### Problem to solve

<!-- What problem do we solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->

### What is the competitive advantage or differentiation for this feature?

<!-- How does our version of this feature differentiate from what other products have? -->

### Intended users

<!-- Who will use this feature? -->

### User experience goal

<!-- What is the single user experience workflow this problem addresses?
For example, "The user should be able to use the UI/API to <perform a specific task>" -->

### Proposal

<!-- How are we going to solve the problem? Try to include the user journey! -->

### Further details

<!-- Include use cases, benefits, goals, or any other details that will help us understand the problem better. -->

### Permissions and Security

<!-- What permissions are required to perform the described actions? Are they consistent with the existing permissions as documented for users, groups, and projects as appropriate? Is the proposed behavior consistent between the UI, API, and other access methods (e.g. email replies)? -->

### Documentation

<!-- What documentation is required for this change? -->

### Availability & Testing

<!-- What risks does this change pose to our availability? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? Will it require cross-browser testing? -->

### Feature Usage Metrics

<!-- How are you going to track usage of this feature? Think about user behavior and their interaction with the product. What indicates someone is getting value from it? -->

### What does success look like, and how can we measure that?

<!-- Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this. -->

### What is the type of buyer?

<!-- What is the buyer persona for this feature? Individual, team lead, group director, or executive? -->

### Available Tier

<!-- Is this a premium feature, or is it associated with add-on usage pricing? -->

### Links / references

<!-- Add links to any other important references for this feature -->

/label ~"Type::Feature"
