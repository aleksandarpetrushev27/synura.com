<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "type::bug" label:

- https://gitlab.com/synura/console/issues?label_name%5B%5D=Type::bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### What is the current _bug_ behavior?

<!-- Describe what actually happens. -->

### What is the expected _correct_ behavior?

<!-- Describe what you should see instead. -->

### Relevant screenshots

<!-- Paste any relevant screenshots here. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~"Type::Bug"
